/*
	берем файл *.pct и компилим его в *.pcp
*/

#include <stdio.h>
#include "pcp/pcp.h"
#include <malloc.h>
#include <string.h>

static void do_optimize(pcp_t * p);
static void do_compress(pcp_t * pcp);

int main(int argc, char * argv[] ) {
	if ( argc == 1 ) {
		printf ("pcpcomp [options] <pctfile>\n");
		printf ("\n");
		printf ("options:\n");
		printf ("\t-o - optimizes content (removes all spaces and empty data)\n");
		printf ("\t-z - deflates all pieces in pcp\n");
		printf ("\t-e - execute main piece after parsing and converting\n");
        printf ("\t-d - increase log level\n");
        printf ("\t-x - don't explain\n");
		return 1;
	}
	// argc>0
	init_varstorage();
	pcp_t t;
	pcp_init(&t);
	int n = 1, opts = 1, do_optimize_contents=0, do_exec = 0, do_compress_pieces = 0;
    int log_level = 0;
	int do_explain = 1;

	while (opts) {
		opts = 0;
		if ( strcmp("-o", argv[n])==0) {
			do_optimize_contents=1;
			n++;
			opts++;
		}
		if ( strcmp("-z", argv[n])==0) {
			do_compress_pieces=1;
			n++;
			opts++;
		}
		if ( strcmp("-e", argv[n])==0) {
			do_exec=1;
			n++;
			opts++;
		}
        if ( strcmp("-d", argv[n])==0) {
            log_level++;
            n++;
            opts++;
        }
        if ( strcmp("-x", argv[n])==0) {
            do_explain = 0;
            n++;
            opts++;
        }
	}
	
	pcp_load(&t, argv[n]);
    pcp_set_log_level(&t, log_level);
	pcp_pars(&t);
	if ( do_optimize_contents )
		do_optimize(&t);
	if ( do_compress_pieces )
		do_compress(&t);
	char * argvsave = (char *) malloc (strlen(argv[n])+1+4);
	memcpy(argvsave, argv[n], strlen(argv[n])+1);
	char * p = argvsave + strlen(argv[n]);
	int modified = 0;
	while (p>argvsave) {
		if (*p=='.') {
			modified = 1;
			++p;
			*p = 'p'; ++p;
			*p = 'c'; ++p;
			*p = 'p'; ++p; *p = 0;
			break;
		}
		if (*p=='/')
			break;
		--p;
	}
	if (!modified) {
		p = argvsave + strlen(argv[n]);
		*p = '.'; ++p;
			*p = 'p'; ++p;
			*p = 'c'; ++p;
			*p = 'p'; ++p; *p = 0;
	}
	pcp_save(&t, argvsave);
    if ( do_explain )
        pcp_xplain(&t, stdout);
    if ( do_exec ) {
        vecstring * ret = pcp_exec(&t, argv[n], "main");
        if ( ret ) {
            fprintf(stderr, "%V", ret);
            delete_vecstring(ret);
        }
    }
	pcp_fini(&t);
	fini_varstorage();
	return 0;
}

static void do_optimize(pcp_t * pcp) {
	// оптимизируем все куски
}

static void do_compress(pcp_t * pcp) {
	int idx=0;
	for (;idx<PCP_HT_SIZE;idx++) {
		pcp_piece * it = pcp->pieces[idx];
		while(it) {
			pcp_z(it);
			it = it->next;
		}
	}
}
