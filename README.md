pcp-project
===========

Parse Cut and Paste Project
---------------------------

### Basics:

This project consists of parser - interpretator and executer. Parser parses files (PCT file) with formatted data and creates
PCP container of pieces. Executer starts working on pcp container (PCP file) by taking piece called `main` at first.

### How to format data for parser

You need to define pieces in this file like this:

    <!--{<piece name>}-- ... <some data> .. --{<piece name>}-->

Example of test.pct:

    <!--{main}-- Here comes some data --{main}-->
    <!--{other-piece}--Some other data--{other-piece}-->

Now data is ready to be parsed. Use `pcpcomp` utility to create binary container of pieces (PCP file).

### Using variables in pieces

Pieces in PCT files are actually templates. So you can add some variables to this templates.

Example of test.pct:

    <!--{main}-- Here comes soma data: $(data)$ --{main}-->
    <!--{other-piece}-- Some other data: $(other-data)$ --{main}-->

Use `pcpcomp` utility to create PCP file.

### PCP Extensions

PCP Engine also supports extensions.

 * [Standart extensions](/vyacheslavs/parse-paste-and-cut/src/3b587020e05a91a2eb80bc8b991a3d7456547612/pcp/so-extensions/std?at=master)
 * [Curl](/vyacheslavs/parse-paste-and-cut/src/a56fbbfa5854b057b85a0a79cbbad68f23244609/pcp/so-extensions/curl?at=master)
 * [Dump/Load](/vyacheslavs/parse-paste-and-cut/src/pcp/so-extensions/curl?at=master)
 * [Json](/vyacheslavs/parse-paste-and-cut/src/a56fbbfa5854b057b85a0a79cbbad68f23244609/pcp/so-extensions/json?at=master)
 * Math
 * Mysql
 * Python
 * Random
 * System
 * Xml
