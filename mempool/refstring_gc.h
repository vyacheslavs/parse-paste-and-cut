#ifndef __REFSTR_GC_H
#define __REFSTR_GC_H

#include "vecstring/refstring.h"

// смысл этого модуля в том, что
// все строки refstring типа при удалении будут попадать в список (также как это делает mempool)
// А при конструировании новой строки vecstring они будут задействованы

void refstr_gc_init();
void refstr_gc_fini();

void refstr_utilyze( refstring * refstr );
refstring * refstr_fetch ();

#ifdef CONFIG_GC_PERFORMANCE_COUNTERS
uint64_t refstr_reuse_count();
#endif

#endif
