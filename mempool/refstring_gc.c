#include "refstring_gc.h"
#include <malloc.h>

static refstring * head = NULL;
static refstring * tail = NULL;

#ifdef CONFIG_GC_PERFORMANCE_COUNTERS
static uint64_t reusing_count = 0L;
#endif

void refstr_gc_init() {
}

void refstr_gc_fini() {
	refstring * it = head, *n;
	while (it) {
		n = it->next;
		free(it);
		it = n;
	}
}

void refstr_utilyze( refstring * refstr ) {
	refstr->next = NULL;
	if (!head) {
		head = refstr;
		tail = refstr;
	} else {
		tail->next = refstr;
		tail = refstr;
	}
}

refstring * refstr_fetch () {
	if (!head)
		return NULL;
	refstring * ret = head;
	refstring_refc(ret) = 1;
	head = head->next;
	if (!head)
		tail = NULL;
#	ifdef CONFIG_GC_PERFORMANCE_COUNTERS
	reusing_count++;
#	endif
	return ret;
}

#ifdef CONFIG_GC_PERFORMANCE_COUNTERS
uint64_t refstr_reuse_count() {
	return reusing_count;
}
#endif
