#include "mempool.h"
#include <assert.h>
#include <malloc.h>

#ifdef CONFIG_GC_PERFORMANCE_COUNTERS
static uint64_t reuse_count = 0L;
#endif

#define container_of(ptr, type, member) ({ \
                const typeof( ((type *)0)->member ) *__mptr = (ptr); \
                (type *)( (char *)__mptr - offsetof(type,member) );})

void mp_init( mempool * pool, size_t size ) {
	pool->reuse = NULL;
	pool->reuse_count = 0;
	pool->item_size = size;
}

void * mp_alloc ( mempool * pool ) {
#	ifdef CONFIG_TURNOFF_MEMPOOL
	void * retz = malloc(pool->item_size);
	return retz;
#	endif
	if (!pool->reuse) {
		mempool_chain * chain = malloc(sizeof(mempool_chain)+pool->item_size);
		assert(chain);
		chain->next = NULL;
		return &chain->data;
	}
	void * ret = &pool->reuse->data;
	pool->reuse = pool->reuse->next;
#	ifdef CONFIG_GC_PERFORMANCE_COUNTERS
	reuse_count++;
#	endif
	return ret;
}

void mp_free( mempool * pool, void * ptr ) {
#	ifdef CONFIG_TURNOFF_MEMPOOL
	free(ptr);
	return;
#	endif
	mempool_chain * chain = container_of(ptr, mempool_chain, data);
	chain->next = pool->reuse;
	pool->reuse = chain;
	pool->reuse_count++;
}

void mp_fini( mempool * pool ) {
	mempool_chain * n;
	while (pool->reuse) {
		n = pool->reuse->next;
		free(pool->reuse);
		pool->reuse = n;
	}
}

#ifdef CONFIG_GC_PERFORMANCE_COUNTERS
uint64_t mempool_reuse_count() {
	return reuse_count;
}
#endif
