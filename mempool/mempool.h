#ifndef __MEMPOOL_H
#define __MEMPOOL_H

#include <stddef.h>
#include <stdint.h>

typedef struct __mempool_chain {
	struct __mempool_chain * next;
	uint8_t data;
} mempool_chain;

typedef struct __mempool_ctx {
	
	mempool_chain * reuse;
	size_t reuse_count;
	size_t item_size;
} mempool;

void mp_init( mempool * pool, size_t size );
void * mp_alloc ( mempool * pool );
void mp_free( mempool * pool, void * ptr );
void mp_fini( mempool * pool );

#ifdef CONFIG_GC_PERFORMANCE_COUNTERS
uint64_t mempool_reuse_count();
#endif

#endif
