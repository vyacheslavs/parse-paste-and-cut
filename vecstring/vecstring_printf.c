#include "vecstring.h"
#include "gccver.h"
#include <printf.h>
#include <stdio.h>

static int print_vecstring (FILE *stream,
                   const struct printf_info *info,
                   const void *const *args) {
	const vecstring * ref = *((const vecstring **) (args[0]));
	vecstring_chain * it = ref->head;
	int len = 0;
	while (it) {
		len+=fwrite(refstring_ptr(it->refstr)+it->payload_offset, it->payload_size, 1, stream);
		it = it->next;
	}
	return len;
}

static int print_vecstring_detailed (FILE *stream,
                   const struct printf_info *info,
                   const void *const *args) {
	const vecstring * ref = *((const vecstring **) (args[0]));
	vecstring_chain * it = ref->head;
	int len = 0;
	int chunkNo = 0;
	len+=fprintf(stream, "head=%p,tail=%p\n", ref->head, ref->tail);
	while (it) {
		len+=fprintf(stream, " chunk %d: ptr=%p\n", chunkNo, it);
		len+=fprintf(stream, "  payload_offset: %d\n",it->payload_offset);
		len+=fprintf(stream, "  payload_size  : %d\n",it->payload_size);
		len+=fprintf(stream, "          next  : %p\n",it->next);
		len+=fprintf(stream, "          data  : [");
		len+=fwrite(refstring_ptr(it->refstr)+it->payload_offset, it->payload_size, 1, stream);
		len+=fprintf(stream, "]\n");
		it = it->next;
		chunkNo++;
	}
	return len;
}

#	if GCC_VERSION > 40100
static int print_vecstring_arginfo (const struct printf_info *info, size_t n, int * some,
#	else
static int print_vecstring_arginfo (const struct printf_info *info, size_t n,
#	endif
                           int *argtypes) {
	if (n > 0)
		argtypes[0] = PA_POINTER;
	return 1;
}

void init_vecstring_printfs() {
#	if GCC_VERSION > 40100
	register_printf_specifier
#	else
	register_printf_function
#	endif
		('V', print_vecstring, print_vecstring_arginfo);
#	if GCC_VERSION > 40100
	register_printf_specifier
#	else
	register_printf_function
#	endif
	('B', print_vecstring_detailed, print_vecstring_arginfo);
};
