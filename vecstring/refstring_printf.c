#include "refstring.h"
#include <stdio.h>
#include "gccver.h"
#include <printf.h>

static int print_refstring (FILE *stream,
                   const struct printf_info *info,
                   const void *const *args) {
	const refstring * ref = *((const refstring **) (args[0]));
	return fwrite(refstring_ptr(ref), refstring_size(ref),1, stream);
}

#	if GCC_VERSION > 40100
static int print_refstring_arginfo (const struct printf_info *info, size_t n, int * some,
#	else
static int print_refstring_arginfo (const struct printf_info *info, size_t n,
#	endif
                           int *argtypes) {
	if (n > 0)
		argtypes[0] = PA_POINTER;
	return 1;
}

void init_refstring_printfs() {
#	if GCC_VERSION > 40100
	register_printf_specifier
#	else
	register_printf_function
#	endif
		('R', print_refstring, print_refstring_arginfo);
};
