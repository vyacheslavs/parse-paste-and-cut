#ifndef __REFSTRING_H
#define __REFSTRING_H

#include <stddef.h>
#include <stdint.h>

typedef struct __refstring {
	uint8_t rmark;
	size_t refCount;
	size_t size;
#	ifdef CONFIG_REFSTR_GC
	struct __refstring * next;
#	endif
	uint8_t cdata;
} refstring;

refstring * new_refstring(size_t sz);
void delete_refstring(refstring * rs);
refstring * clone_refstring(refstring *rs); // делает независимую копию
refstring * share_refstring(refstring *rs); // делает зависимую копию

void pchar_to_refstring(refstring * dst, const char * src );
refstring * new_refstring_from_pchar(const char * src);
int refstring_cmp( refstring * r1, refstring * r2 );
char * rtopchar(refstring * r);

#define __RMARK	254

#define refstring_ptr(x) ((uint8_t *)&x->cdata)
#define refstring_size(x) (x->size)
#define refstring_refc(x) x->refCount
#define refstring_mark(x) x->rmark

#endif
