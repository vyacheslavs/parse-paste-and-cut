#ifndef __GCC_VER_H
#define __GCC_VER_H

#define GCC_VERSION (__GNUC__ * 10000 \
                               + __GNUC_MINOR__ * 100 )

#endif
