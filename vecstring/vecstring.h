#ifndef __VEC_STRING
#define __VEC_STRING

#include "refstring.h"
#include <stdio.h>

typedef struct __vecstring_chain {
	struct __vecstring_chain * next; // next chain
	
	refstring * refstr;
	
	size_t payload_offset; // начало хороших данных в refstr
	size_t payload_size; // размер этих хороших данных
	
} vecstring_chain;

#define __VMARK 255

typedef struct __vecstring {
	uint8_t vmark;
	vecstring_chain * head; // head chain
	vecstring_chain * tail;
	size_t length;
} vecstring;

vecstring * new_vecstring (size_t sz);
void vappend(vecstring * dst, vecstring * src); // добавить к dst данные из src
void vmove(vecstring * dst, vecstring * src); // добавляет к dst данные из src, причем src уничтожается
void vappend_refstring( vecstring * dst, refstring * src, size_t offset, size_t size );
vecstring * subvstr(vecstring * src, size_t offset, size_t size); // выделает подстроку из src
void vinsert(vecstring * dst, size_t offset, vecstring * src );
void verase(vecstring * dst, size_t offset, size_t size);
vecstring * vsplit(vecstring * src, size_t offset); // распиливает строку src так, что в src остается одна половина, а другая возвращается через return
vecstring * vecstring_from_refstring( refstring * str, size_t offset, size_t size );
vecstring * vecstring_from_pchar( const char * pc );
vecstring * vecstring_from_bchar( const char * bc, size_t sz );
void vdump_to_ptr( vecstring * src, char * dst );
size_t vread ( vecstring * src, void * dst, size_t offset, size_t bytes );
size_t vread_to_stream( vecstring * src, FILE * dst, size_t offset, size_t bytes );
int vstrcmp( vecstring * src, const char * pattern); // сравнить с pattern и возвратить 1 в случае ок
int vstrcmp2( vecstring * src, const char * pattern, size_t offset); // сравнить src начиная с offset с pattern и возвратить 1 в случае ок
int vstr_compare( const vecstring * src, const vecstring * dst); // сравнить 2 vecstr
char * vtopchar( vecstring * src);

void vwrite(vecstring * dst, const void * ptr, size_t sz);

void vunique(vecstring * dst); // после выполнения этой функции гарантируется, что данные этой строки будут уникальными

typedef struct __viteration_t {
	vecstring_chain * it;
	size_t payload_index;
	size_t offset;
} viteration_t;

#define VITERATION_INIT { NULL, 0, 0 }

uint8_t vstring_at ( const vecstring * src, size_t offset, viteration_t * v );

vecstring * vshare(const vecstring * src);

typedef int (*vfind_callback) ( vecstring * src, int position, void * priv );
 // вызывает callback каждый раз, когда находится pattern, если cb возвращает 0, то поиск превращается
void vfind_pchar( vecstring * src, const char * pattern, size_t offset, vfind_callback cb, void * priv );

typedef int (*vforeach_callback) ( vecstring * src, int position, char c, void * priv );
void vforeach( vecstring * src, vforeach_callback cb, void * priv );

void pchar_to_vecstring(vecstring * dst, const char * src); // копирует из src в dst pchar
void delete_vecstring(vecstring * dst);
uint64_t vecstring_keyval(const vecstring * src);

#define foreach_chain(vec,it) \
	vecstring_chain * it = vec->head; \
	for (;it!=NULL;it=it->next)


#endif
