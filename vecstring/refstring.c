#include "refstring.h"
#include <malloc.h>
#include <assert.h>
#include <string.h>
#include "mempool/refstring_gc.h"

refstring * new_refstring(size_t sz) {
	uint8_t * news = malloc(sizeof(refstring)+sz-1);
	assert(news);
	refstring * ret = (refstring *)news;
	refstring_refc(ret) = 1;
	refstring_size(ret) = sz;
	refstring_mark(ret) = __RMARK;
	return ret;
};

void delete_refstring(refstring * rs) {
	refstring_refc(rs) = refstring_refc(rs) -1;
	if (refstring_refc(rs)==0) {
#		ifdef CONFIG_REFSTR_GC
		refstr_utilyze(rs);
#		else
		free(rs);
#		endif
	}
}

refstring * clone_refstring(refstring * rs) {
	uint8_t * news = malloc(sizeof(refstring)+refstring_size(rs)-1);
	assert(news);
	refstring * ret = (refstring *)news;
	refstring_refc(ret) = 1;
	refstring_size(ret) = refstring_size(rs);
	refstring_mark(ret) = __RMARK;
	memcpy(refstring_ptr(ret), refstring_ptr(rs), refstring_size(rs));
	return ret;
}

refstring * share_refstring(refstring * rs) {
	refstring_refc(rs) = refstring_refc(rs) + 1;
	return rs;
}

void pchar_to_refstring(refstring * dst, const char * src ) {
	size_t idx = 0;
	uint8_t c;
	while (idx<refstring_size(dst)) {
		c = *( (const uint8_t *)src + idx);
		if (!c) break;
		*(refstring_ptr(dst)+idx) = c;
		idx++;
	}
}

refstring * new_refstring_from_pchar(const char * src) {
	refstring * newo = new_refstring(strlen(src));
	pchar_to_refstring(newo, src);
	return newo;
}

int refstring_cmp( refstring * r1, refstring * r2 ) {
	if (refstring_size(r1)!=refstring_size(r2))
		return 0;
	return memcmp(refstring_ptr(r1), refstring_ptr(r2), refstring_size(r1))==0;
}

char * rtopchar(refstring * r) {
	char * ret = (char *) malloc ( refstring_size(r)+1 );
	memcpy(ret, refstring_ptr(r), refstring_size(r));
	ret[refstring_size(r)]=0;
	return ret;
}
