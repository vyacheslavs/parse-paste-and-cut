#include "vecstring.h"
#include <assert.h>
#include <stdio.h>
#include "mempool/mempool.h"
#include <string.h>
#include "mempool/refstring_gc.h"
#include <malloc.h>

static mempool m; // для chain
static mempool m1; // для vecstring

static void __attribute__((constructor)) vecstring_mempool() {
	mp_init(&m1, sizeof(vecstring));
	mp_init(&m, sizeof(vecstring_chain));
#	ifdef CONFIG_REFSTR_GC
	refstr_gc_init();
#	endif
}

static void __attribute__((destructor)) vecstring_mempool_free() {
	mp_fini(&m);
	mp_fini(&m1);
#	ifdef CONFIG_REFSTR_GC
	refstr_gc_fini();
#	endif
}

#	ifdef CONFIG_REFSTR_GC

static void enqueue_vec_chain( vecstring * vec, vecstring_chain * chunk ) {
	chunk->next = NULL;
	if (!vec->head) {
		vec->head = chunk;
		vec->tail = chunk;
	} else {
		vec->tail->next = chunk;
		vec->tail = chunk;
	}
}

vecstring * new_vecstring (size_t sz) {
	vecstring * newone = mp_alloc(&m1);
	newone->vmark = __VMARK;
	newone->head = NULL;
	newone->tail = NULL;
	if (!sz) {
		newone->length = 0;
		return newone;
	}
	size_t provided = 0;
	while (provided < sz) {
		vecstring_chain * newc = mp_alloc(&m);
		refstring * recycled = refstr_fetch();
		if (!recycled) {
			// самый простой вариант - кончились refstr, тогда просто берем и зафигачиваем колбаску
			newc->refstr = new_refstring(sz-provided);
			newc->payload_offset = 0;
			newc->payload_size = sz-provided;
			provided = sz;
		} else {
			// refstr существует, присоединяем его
			newc->refstr = recycled;
			newc->payload_offset = 0;
			if ( refstring_size(recycled) + provided < sz ) {
				newc->payload_size = refstring_size(recycled);
				provided += refstring_size(recycled);
			} else {
				newc->payload_size = sz-provided;
				provided += newc->payload_size;
			}
		}
		enqueue_vec_chain(newone, newc);
	}
	newone->length = sz;
	return newone;
}
#	else
vecstring * new_vecstring (size_t sz) {
	vecstring * newone = mp_alloc(&m1);
	vecstring_chain * newc = NULL;
	if (sz>0) {
		newc = mp_alloc(&m);
		newc->next = NULL;
		newc->refstr = new_refstring(sz);
		newc->payload_offset = 0;
		newc->payload_size = sz;
	}
	newone->vmark = __VMARK;
	newone->head = newc;
	newone->tail = newc;
	newone->length = sz;
	return newone;
}
#	endif

void pchar_to_vecstring(vecstring * dst, const char * src) {
	vecstring_chain * it = dst->head;
	int idx, srcidx=0;
	uint8_t c;
	while (it) {
		idx = 0;
		while (idx<refstring_size(it->refstr)) {
			c = *((uint8_t *)src+srcidx);
			if (!c) {
				it->payload_size = idx;
				return;
			}
			*(refstring_ptr(it->refstr)+idx) = c;
			idx++;
			srcidx++;
		}
		it = it->next;
	}
}

static vecstring_chain * new_vecstring_chain( vecstring_chain * share ) {
	vecstring_chain * chain = mp_alloc(&m);
	chain->refstr = share_refstring(share->refstr);
	chain->payload_offset = share->payload_offset;
	chain->payload_size = share->payload_size;
	chain->next = NULL;
	return chain;
}

void vappend(vecstring * dst, vecstring * src) {
	vecstring_chain * it = src->head, * head = NULL, * tail = NULL;
	
	while (it) {
		
		vecstring_chain * chain = new_vecstring_chain(it);
		
		if (!head) {
			head = chain;
			tail = chain;
		} else {
			tail->next = chain;
			tail = chain;
		}
		it = it->next;
	}
	if (!dst->head) {
		dst->head = head;
		dst->tail = tail;
	} else {
		dst->tail->next = head;
		dst->tail = tail;
	}
	dst->length += src->length;
}

void delete_vecstring(vecstring * dst) {
	vecstring_chain * it = dst->head, *n;
	while (it) {
		n = it->next;
		delete_refstring(it->refstr);
		mp_free(&m, it);
		it = n;
	}
	mp_free(&m1, dst);
}

static int vecstring_is_end( vecstring_chain * it, vecstring_chain * newc, size_t offset, size_t payload_index, size_t size ) {
	
	if ( offset+size >= payload_index && offset+size< payload_index+it->payload_size ) {
		newc->payload_size -= (payload_index+it->payload_size) - (offset+size);
		return 1;
	}
	return 0;
}

vecstring * subvstr(vecstring * src, size_t offset, size_t size) {
	vecstring * ret = new_vecstring(0);
	ret->vmark = __VMARK;
	ret->head = NULL;
	
	if ( src->length - offset < size ) size = src->length - offset;
	size_t payload_index = 0;
	vecstring_chain * it = src->head;
	int do_share = 0;
	while (it) {
		
		if ( offset >= payload_index && offset < payload_index + it->payload_size ) {
			// начало данных для выгрузки
			vecstring_chain * newchain = new_vecstring_chain(it);
			// теперь начинаем рассчет оффсета
			newchain->payload_offset += (offset-payload_index);
			newchain->payload_size -= (offset-payload_index);
			
			ret->head = newchain;
			ret->tail = newchain;
			ret->length = newchain->payload_size;
			
			if ( vecstring_is_end( it, newchain, offset, payload_index, size )) {
				ret->length = newchain->payload_size;
				break;
			}
			do_share = 1;
			payload_index += it->payload_size;
			it = it->next;
			continue;
		}
		
		if (do_share) {
			vecstring_chain * newchain = new_vecstring_chain(it);
			ret->tail->next = newchain;
			ret->tail = newchain;
			
			if ( vecstring_is_end( it, newchain, offset, payload_index, size )) {
				ret->length += newchain->payload_size;
				break;
			}
			ret->length += newchain->payload_size;
		}
		
		payload_index += it->payload_size;
		it = it->next;
	}
	return ret;
}

void vinsert(vecstring * dst, size_t offset, vecstring * src ) {
	
	vecstring_chain * it = dst->head, *srcit = src->head;
	size_t payload_index = 0;
	while (it) {
		if ( offset >= payload_index && offset < payload_index + it->payload_size ) {
			// мы нашли куда надо вставить src
			// здесь мы либо будем использовать 2-а чанка, либо 1 чанк
			// все зависит от разности offset-payload_index
			
			vecstring_chain * repeat_it = new_vecstring_chain(it);
			repeat_it->next = it->next;
			// обрезаем it
			
			repeat_it->payload_offset += (offset-payload_index);
			repeat_it->payload_size -= (offset-payload_index);
			// в результате it -> [src] -> repeat_it
			
			vecstring_chain * head = NULL, * tail = NULL;
			while ( srcit ) {
				vecstring_chain * newchain = new_vecstring_chain(srcit);
				if (!head) {
					head = newchain;
					tail = newchain;
				} else {
					tail->next = newchain;
					tail = newchain;
				}
				dst->length += srcit->payload_size;
				srcit = srcit->next;
			}
			it->payload_size = (offset-payload_index);
			it->next = head;
			// после того как цепочка была скопирована надо восстановить дорогу на разрыв
			tail->next = repeat_it;
			
			return;
		}
		payload_index += it->payload_size;
		it = it->next;
	}
}

void verase(vecstring * dst, size_t offset, size_t size) {
	if ( dst->length - offset < size ) size = dst->length - offset;
	vecstring * dst_half2 = vsplit(dst, offset);
	if ( size>=dst_half2->length ) {
		delete_vecstring(dst_half2);
		return;
	}
	vecstring * dst_sub = subvstr(dst_half2, size, SIZE_MAX);
	vappend ( dst, dst_sub );
	delete_vecstring(dst_half2);
	delete_vecstring(dst_sub);
}

vecstring * vsplit(vecstring * src, size_t offset) {
	assert ( src->length > offset );
	vecstring * half2 = NULL;
	vecstring_chain * it = src->head;
	size_t payload_index = 0;
	while (it) {
		if ( offset >= payload_index && offset < payload_index + it->payload_size ) {
			vecstring_chain * newit = new_vecstring_chain(it);
			it->payload_size = (offset-payload_index);
			newit->payload_offset+=(offset-payload_index);
			newit->payload_size -= (offset-payload_index);
			newit->next = it->next;
			it->next = NULL;
			half2 = mp_alloc(&m1);
			half2->vmark == __VMARK;
			half2->head = newit;
			half2->tail = src->tail;
			if ( src->tail == it )
				half2->tail = newit;
			src->tail = it;
			half2->length = src->length - offset;
			src->length = offset;
			return half2;
		}
		payload_index += it->payload_size;
		it = it->next;
	}
	assert(0);
	return NULL;
}

vecstring * vecstring_from_refstring( refstring * str, size_t offset, size_t size ) {
	vecstring * newone = mp_alloc(&m1);
	vecstring_chain * newc = mp_alloc(&m);
	
	newc->next = NULL;
	newc->refstr = share_refstring(str);
	newc->payload_offset = offset;
	newc->payload_size = size;
	
	newone->vmark = __VMARK;
	newone->head = newc;
	newone->tail = newc;
	newone->length = size;
	return newone;
}

vecstring * vecstring_from_pchar( const char * pc ) {
	refstring * rn = new_refstring_from_pchar(pc);
	vecstring * ret = vecstring_from_refstring( rn, 0, refstring_size(rn) );
	delete_refstring(rn);
	return ret;
}

vecstring * vecstring_from_bchar( const char * bc, size_t sz ) {
	refstring * rn = new_refstring(sz);
	memcpy(refstring_ptr(rn), bc, sz);
	vecstring * ret = vecstring_from_refstring( rn, 0, refstring_size(rn) );
	delete_refstring(rn);
	return ret;
}

void vappend_refstring( vecstring * dst, refstring * src, size_t offset, size_t size ) {
	vecstring_chain * chain = mp_alloc(&m);
	chain->refstr = share_refstring(src);
	chain->payload_offset = offset;
	chain->payload_size = size;
	chain->next = NULL;

	if (!dst->head) {
		dst->head = chain;
		dst->tail = chain;
	} else {
		assert(!dst->tail->next);
		dst->tail->next = chain;
		dst->tail = chain;
	}
	dst->length += size;
}

uint8_t vstring_at ( const vecstring * src, size_t offset, viteration_t * v ) {
	
	vecstring_chain * it = src->head;
	size_t payload_index = 0;
	
	if ( v && v->it && offset>v->offset ) {
		it = v->it;
		payload_index = v->payload_index;
	}
	
	while (it) {
		if ( offset >= payload_index && offset < payload_index + it->payload_size ) {
			if (v) {
				v->it = it;
				v->payload_index = payload_index;
				v->offset = offset;
			}
			return *(refstring_ptr(it->refstr)+it->payload_offset+(offset-payload_index));
		}
		payload_index += it->payload_size;
		it = it->next;
	}
	assert(0);
}

void vfind_pchar( vecstring * src, const char * pattern, size_t offset, vfind_callback cb, void * priv ) {
	viteration_t v, pre;
	v.it = NULL;
	
	if ( strlen(pattern) > src->length ) return;
	
	
	size_t patsz = strlen(pattern);
	size_t lensz = src->length - patsz+1;
	
	//printf("look [%s] in [%V]: patsz: %d lensz: %d\n", pattern, src, patsz, lensz);
	
	size_t idx = offset, idx_inner = 0;
	for (;idx<lensz;idx++) { // идем по всей длине
		for (idx_inner = 0;idx_inner<patsz;idx_inner++) {
			uint8_t c = vstring_at( src, idx+idx_inner,&v );
			if ( idx_inner == 0 ) pre = v;
			if (c!=*(pattern+idx_inner)) break;
		}
		if ( idx_inner == patsz ) {// equal ? 
			if (!cb ( src, idx, priv )) return;
			idx+=patsz-1;
		}
		v = pre;
	}
}

vecstring * vshare(const vecstring * src) {
	vecstring * newone = mp_alloc(&m1);
	vecstring_chain * it = src->head;
	newone->length = src->length;
	newone->head = NULL;
	newone->tail = NULL;
	newone->vmark = __VMARK;
	while (it) {
		vecstring_chain * newit = new_vecstring_chain(it);

		if (!newone->head) {
			newone->head = newit;
			newone->tail = newit;
		} else {
			newone->tail->next = newit;
			newone->tail = newit;
		}
		it = it->next;
	}
	return newone;
}

void vunique(vecstring * dst) {
	
	foreach_chain(dst,it) {
		if ( refstring_refc(it->refstr)>1 ) {
			refstring * unique = clone_refstring(it->refstr);
			delete_refstring(it->refstr);
			it->refstr = unique;
		}
	}
}

void vwrite(vecstring * dst, const void * ptr, size_t sz) {
#	ifdef CONFIG_VWRITE_USES_GC
	// это работает следующим образом:
	// сначала берем свободный refstring
	size_t ptr_oft = 0;
	size_t szfill = 0;

	for (;;) {
		refstring * fetched = refstr_fetch();
		if (!fetched) {// если нет свободных refstr, то просто аллокируем
			szfill = sz-ptr_oft;
			fetched = new_refstring(szfill);
		} else {
			szfill = sz-ptr_oft>refstring_size(fetched) ? refstring_size(fetched) : sz-ptr_oft;
		}

		memcpy(refstring_ptr(fetched), ptr+ptr_oft, szfill); // записываем в составной refstr данные
		vappend_refstring(dst, fetched, 0, szfill);
		delete_refstring(fetched);

		ptr_oft+=szfill;
		if (ptr_oft>=sz) break;
	}

#	else
	refstring * nr = new_refstring(sz);
	memcpy(refstring_ptr(nr), ptr, sz);
	vappend_refstring(dst, nr, 0, sz);
	delete_refstring(nr);
#	endif
}

void vmove(vecstring * dst, vecstring * src) {
	if (!src->head) {
		mp_free(&m1,src);
		return;
	}
	if (!dst->head) {
		dst->head = src->head;
		dst->tail = src->tail;
	} else {
		dst->tail->next = src->head;
		dst->tail = src->tail;
	}
	dst->length += src->length;
	src->head = NULL;
	src->tail = NULL;
	src->length = 0;
	mp_free(&m1,src);
}

void vdump_to_ptr( vecstring * src, char * dst ) {
	foreach_chain(src,it) {
		memcpy(dst, refstring_ptr(it->refstr)+it->payload_offset, it->payload_size);
		dst+=it->payload_size;
	}
}

int vstrcmp( vecstring * src, const char * pattern ) {
	size_t size = strlen(pattern);
	if (src->length < size) return 0;
	viteration_t v = VITERATION_INIT;
	size_t idx=0;
	
	for ( ;idx<size;idx++) {
		uint8_t c = vstring_at(src, idx, &v);
		if ( pattern[idx]!=c ) return 0;
	}
	return 1;
}

int vstrcmp2( vecstring * src, const char * pattern, size_t offset) {
	size_t size = strlen(pattern);
	if (src->length-offset < size) return 0;
	viteration_t v = VITERATION_INIT;
	size_t idx=offset;
	
	for ( ;idx<size+offset;idx++) {
		uint8_t c = vstring_at(src, idx, &v);
		if ( pattern[idx-offset]!=c ) return 0;
	}
	return 1;
}

size_t vread ( vecstring * src, void * dst, size_t offset, size_t bytes ) {
	size_t idx=offset, rd = 0;
	viteration_t v = VITERATION_INIT;
	for (;idx<offset+bytes;idx++) {
		if (idx>=src->length) return rd;
		*((uint8_t *)dst+rd) = vstring_at(src, idx, &v);
		rd++;
	}
	return rd;
}

size_t vread_to_stream( vecstring * src, FILE * dst, size_t offset, size_t bytes ) {
	size_t idx=offset, rd = 0;
	uint8_t c;
	viteration_t v = VITERATION_INIT;
	for (;idx<offset+bytes;idx++) {
		if (idx>=src->length) return rd;
		c = vstring_at(src, idx, &v);
		fwrite(&c,1,1,dst);
		rd++;
	}
	return rd;
}

int vstr_compare( const vecstring * src, const vecstring * dst) {
	if (src->length != dst->length)
		return 0;
	viteration_t srci = VITERATION_INIT, dsti = VITERATION_INIT;
	int idx = 0;
	for(;idx<src->length;idx++) {
		if ( vstring_at(src, idx, &srci)!=vstring_at(dst,idx,&dsti))
			return 0;
	}
	return 1;
}

char * vtopchar( vecstring * src) {
	char * ret = malloc(src->length+1);
	assert(ret);
	vecstring_chain * it = src->head;
	size_t oft = 0;
	while (it) {
		memcpy( ret+oft, refstring_ptr(it->refstr)+it->payload_offset, it->payload_size );
		oft+=it->payload_size;
		it = it->next;
	}
	*(ret+oft)=0;
	return ret;
}

uint64_t vecstring_keyval(const vecstring * src) {
	uint64_t ret = 0;
	viteration_t v = VITERATION_INIT;
	size_t idx;
	for (idx=0;idx<src->length; idx++) {
		uint8_t c = vstring_at(src, idx, &v);
		ret = ret * 10 + c;
	}
	return ret;
}

void vforeach( vecstring * src, vforeach_callback cb, void * priv ) {
	vecstring_chain * it = src->head;
	size_t payload_index = 0;
	size_t offset = 0;
	while (it) {
        for (offset=payload_index;offset<payload_index+it->payload_size;++offset) {
            if (!cb( src, offset, *(refstring_ptr(it->refstr)+it->payload_offset+(offset-payload_index)), priv))
                break;
        }
		payload_index += it->payload_size;
		it = it->next;
	}
}
