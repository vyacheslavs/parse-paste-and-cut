#include "pcp.h"
#include <stdio.h>

void pcp_xplain(pcp_t * pcp, FILE * stream) {
	int idx=0;
	for (;idx<PCP_HT_SIZE;idx++) {
		pcp_piece * it = pcp->pieces[idx];
		while(it) {
			fprintf(stream, "piece [%R::%V]\n", it->ns, it->name);
			if (it->parsed)
				pcp_do_xplain(pcp, stream, it->parsed, 1);
			it = it->next;
		}
	}
}

size_t pcp_do_xplain ( pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	size_t from_offset = 0;
	uint32_t cm, sz;
	while ( from_offset < parsed->length ) {
		// читаем комманду
		if (pcp_read_command ( &cm, &sz, parsed, from_offset)) {
			if ( cm == CMD_DATA ) {
				pcp_intend( lvl, stream );
				fprintf(stream, "CMD_DATA: [");
				vread_to_stream ( parsed, stream, from_offset+8, sz );
				fprintf(stream, "]\n");
			} else {
				// пробуем "объяснить" через хэндлеры
				vecstring * cmdata = subvstr(parsed, from_offset, sz+8);
				pcp_handler * hit = pcp_get_handler(pcp,cm);
				size_t explained_cb = 0;
				if (hit && hit->cb_explain)
						explained_cb = hit->cb_explain( pcp, stream, cmdata, lvl );
				delete_vecstring(cmdata);
				if (!explained_cb)
					fprintf(stream, "Unknown command %d, size:%d\n", cm, sz);
			}
		}
		from_offset+=8+sz;
	}
}

int pcp_read_command ( uint32_t * cm, uint32_t * sz, vecstring * src, size_t oft ) {
	if (vread(src, cm,oft,4)==4) {
		if ( vread(src, sz, oft+4,4)==4 )
			return 1;
	}
	return 0;
}

void pcp_intend( int lvl, FILE * out ) {
	int idx=0;
	for (;idx<lvl;idx++)
		fprintf(out, "  ");
}
