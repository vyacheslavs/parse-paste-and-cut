#ifndef __MACROS_H__
#define __MACROS_H__

#define PARSE_START(command) \
	*parsed = 1; \
	vecstring * dst = new_vecstring(0); \
	uint32_t cm = command; \
	vwrite(dst, &cm, sizeof(uint32_t)); \
	int parsing_offset = sizeof(key)-1; \
    int __log_code = command;

#define PARSE(var,condition) \
	size_t var ## _end=-1; \
	vecstring * var = NULL; \
	{ \
		vecstring * argdata = subvstr(src, parsing_offset, SIZE_MAX); \
		var = pcp_do_parse( argdata, pcp, condition, &var ## _end ); \
		delete_vecstring(argdata); \
		if (var ## _end==-1) { \
			pcp_log(pcp, PCP_LOG_ERROR, __log_code, "syntax error while parsing extension\n" ); \
			delete_vecstring(var); \
			return NULL; \
		} \
		parsing_offset+=var ## _end+1; \
	}

#define PARSE_WITH_CUSTOM_FAIL_HANDLER_START(var,condition) \
	size_t var ## _end=-1; \
	vecstring * var = NULL; \
	{ \
		vecstring * argdata = subvstr(src, parsing_offset, SIZE_MAX); \
		var = pcp_do_parse( argdata, pcp, condition, &var ## _end ); \
		delete_vecstring(argdata); \
		if (var ## _end==-1) {

#define PARSE_WITH_CUSTOM_FAIL_HANDLER_END(var) \
		} \
		parsing_offset+=var ## _end+1; \
	}

#define XPLAIN_START \
	size_t ret = 8;

#define EXPECT_NEXT_COMMAND(command) \
	pcp_read_command( &cm, &sz, parsed, ret) && cm == command

#define XPLAIN(text) \
{ \
	pcp_intend(lvl, stream); \
	fprintf(stream, text "\n"); \
	vecstring * cmdata = subvstr( parsed, ret+8, sz ); \
	pcp_do_xplain( pcp, stream, cmdata, lvl+1 ); \
	ret+=8+sz; \
	delete_vecstring(cmdata); \
}

#define EXEC_START(command) \
	*parsed = 1; \
	vecstring * dst = new_vecstring(0); \
	size_t oft = 8; \
    unsigned int __log_code = command;

#define EXEC_GET_ARG(cmd,arg) \
	vecstring * arg = NULL; \
	if ( pcp_read_command (&cm, &sz, src, oft ) && cm == cmd ) { \
		oft+=8; \
		vecstring * cmdata = subvstr( src, oft, sz ); \
		oft += sz; \
		arg = pcp_do_exec (cmdata, pcp); \
		if (!arg) { \
            pcp_log(pcp, PCP_LOG_ERROR, __log_code, "error while evaluating command argument\n" ); \
			return NULL; \
		} \
		delete_vecstring(cmdata); \
	} else { \
        pcp_log(pcp, PCP_LOG_ERROR, __log_code, "expected command id %d\n", cmd ); \
		return NULL; \
	}

#define EXEC_GET_ARG_NOEXEC(cmd,arg) \
	vecstring * arg = NULL; \
	if ( pcp_read_command (&cm, &sz, src, oft ) && cm == cmd ) { \
		oft+=8; \
		arg = subvstr( src, oft, sz ); \
		oft += sz; \
	} else { \
        pcp_log(pcp, PCP_LOG_ERROR, __log_code, "expected command id %d\n", cmd ); \
		return NULL; \
	}

#define SERIALIZE(command, var) \
        cm = command; \
        vwrite(dst, &cm, sizeof(uint32_t)); \
        cm = var->length; \
        vwrite(dst, &cm, sizeof(uint32_t)); \
        vmove(dst, var);

#define GET_REST(var) \
        vecstring * var; { \
        vecstring * argdata = subvstr(src, parsing_offset, SIZE_MAX); \
        var = pcp_do_parse(argdata, pcp, NULL, NULL); \
        delete_vecstring(argdata); }

#endif
