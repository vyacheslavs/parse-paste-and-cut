#include "pcp.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

/*
   Структура pcp файла следующая:
   <4 байта=размер заголовка><PCP_> заголовок

   далее идут "куски" в след виде:
   <1 байт=флаги куска><4байта=размер><название куска><4байта=размер><данные куска>

   флаги могут быть следующими:
     - PCP_ENCRYPTED 1
     - PCP_GZIPED    2

*/

void pcp_save(pcp_t * pcp, const char * file) {
	int fd = open(file, O_CREAT | O_RDWR | O_TRUNC , 0644);
	if (fd<0) {
		pcp_error(pcp, "can not open file to save pcp: %s\n", file);
		return;
	}
	// пишем заголовок
	uint32_t tmp = 4;
	write(fd, &tmp, 4);
	tmp = 0x5F504350;
	write(fd, &tmp, 4);
	
	int idx=0;
	uint8_t flags = 0;
	for (;idx<PCP_HT_SIZE;idx++) {
		pcp_piece * it = pcp->pieces[idx];
		while(it) {
			flags = 0;
			if ( it->parsed || it->compressed ) {
				if ( it->compressed )
					flags = flags | 2;
				write(fd, &flags, 1);
				// записываем name
				tmp = it->name->length;
				write(fd, &tmp, 4);
				foreach_chain(it->name,nit) {
					write(fd, refstring_ptr(nit->refstr)+nit->payload_offset, nit->payload_size);
				}
				vecstring * to_store = it->parsed;
				if ( it->compressed )
					to_store = it->compressed;
				tmp = to_store->length;
				write(fd, &tmp, 4);
				foreach_chain(to_store,vit) {
					write(fd, refstring_ptr(vit->refstr)+vit->payload_offset, vit->payload_size);
				}
			}
			it = it->next;
		}
	}
	close(fd);
}
