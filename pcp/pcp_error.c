#include "pcp.h"
#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>

static void count_line_offset( vecstring * vec, size_t offset, size_t * line, size_t * lineoft ) {
	viteration_t v = VITERATION_INIT;
	size_t idx=0;
	while (idx<offset) {
		uint8_t c = vstring_at(vec, idx, &v);
		if ( c=='\n' ) {
			(*line)++;
			(*lineoft)=0;
		}
		(*lineoft)++;
		idx++;
	}
}

void pcp_default_error_callback ( const char * what, uint32_t what_len ) {
	fwrite( what, what_len, 1, stderr);
}

void pcp_default_log_callback ( unsigned int level, unsigned int code, const char * what, uint32_t what_len ) {
    char * output = (char *) malloc(0xffff);
    char * message = (char * ) malloc ( what_len+1 );
    memcpy(message, what, what_len);
    message[what_len] = 0;
    
    switch ( level ) {
        case PCP_LOG_CRITICAL: snprintf(output, 0xffff-1, "code %04d CRITICAL %s", code, message); break;
        case PCP_LOG_ERROR   : snprintf(output, 0xffff-1, "code %04d ERROR    %s", code, message); break;
        case PCP_LOG_INFO    : snprintf(output, 0xffff-1, "code %04d INFO     %s", code, message); break;
        case PCP_LOG_WARNING : snprintf(output, 0xffff-1, "code %04d WARNING  %s", code, message); break;
        case PCP_LOG_DEBUG   : snprintf(output, 0xffff-1, "code %04d DEBUG    %s", code, message); break;
        default:               snprintf(output, 0xffff-1, "code %04d          %s", code, message); break;
    }
    fprintf(stderr, output);
    free(message);
    free(output);
}

void pcp_error(struct __pcp_t * pcp, const char * fmt, ... ) {
	va_list ap;
	va_start(ap, fmt);
	char err[0xfff];
	int len = vsnprintf( err, 0xfff-1, fmt, ap );
	va_end (ap);
	if (pcp->errcb)
		pcp->errcb(err, len);
}

void pcp_log(struct __pcp_t * pcp, unsigned int log_level, unsigned int log_code, const char * fmt, ...) {
    if ( !pcp_could_be_logged ( pcp, log_level, log_code ) )
        return;
	va_list ap;
	va_start(ap, fmt);
	char err[0xfff];
	int len = vsnprintf( err, 0xfff-1, fmt, ap );
	va_end (ap);
	if (pcp->logcb)
		pcp->logcb(log_level, log_code, err, len);
}

void pcp_set_log_level ( pcp_t * pcp, unsigned int new_level ) {
    pcp->log_level = new_level;
}
unsigned int pcp_get_log_level (pcp_t * pcp) {
    return pcp->log_level;
}

void pcp_log_code ( struct __pcp_t * pcp, unsigned int code ) {
    if ( !pcp->log_codes ) {
        // инициируем
        pcp->log_codes = (uint8_t *) malloc ( code+1 );
        pcp->log_codes_size = code+1;
        memset( pcp->log_codes, 0, code+1 );
    }
    if ( code >= pcp->log_codes_size ) {
        uint8_t * new_log_codes = (uint8_t *) malloc ( code + 1 );
        memset( new_log_codes, 0, code+1 );
        memcpy( new_log_codes, pcp->log_codes, pcp->log_codes_size);
        free(pcp->log_codes);
        pcp->log_codes = new_log_codes;
        pcp->log_codes_size = code+1;
    }
    pcp->log_codes[code] = 1;
}

void pcp_dont_log ( struct __pcp_t * pcp, unsigned int code ) {
    if ( !pcp->log_codes_except ) {
        // инициируем
        pcp->log_codes_except = (uint8_t *) malloc ( code+1 );
        pcp->log_codes_except_size = code+1;
        memset( pcp->log_codes_except, 0, code+1 );
    }
    if ( code >= pcp->log_codes_except_size ) {
        uint8_t * new_log_codes_except = (uint8_t *) malloc ( code + 1 );
        memset( new_log_codes_except, 0, code+1 );
        memcpy( new_log_codes_except, pcp->log_codes_except, pcp->log_codes_except_size);
        free(pcp->log_codes_except);
        pcp->log_codes_except = new_log_codes_except;
        pcp->log_codes_except_size = code+1;
    }
    pcp->log_codes_except[code] = 1;
}

static int __do_log_system = 0;

void pcp_log_system (int doit) {
    __do_log_system = doit;
}

int pcp_could_be_logged (struct __pcp_t * pcp, unsigned int log_level, unsigned int log_code ) {
    if ( log_code == 0 && !__do_log_system )
        return 0;

    if ( log_level > pcp->log_level )
        return 0;

    if ( pcp->log_codes_except && log_code < pcp->log_codes_except_size) {
        if (pcp->log_codes_except[log_code])
            return 0;
    }

    if ( pcp->log_codes ) {
        if ( log_code >= pcp->log_codes_size )
            return 0;
        if ( !pcp->log_codes[log_code] )
            return 0;
    }

    return 1;
}
