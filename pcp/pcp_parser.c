#include "pcp.h"
#include <stdio.h>
#include <assert.h>
#include <malloc.h>

static char stoponchar = 0;

int stop_on_question( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && c == '?' ) {
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

int stop_on_comma( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && c == ',' ) {
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

char stop_on_char() {
	return stoponchar;
}

int stop_on_comma_or_closingbr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && ( c == ',' || c == ']' ) ) {
		stoponchar = c;
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

int stop_on_eq( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && c == '=' ) {
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

int stop_on_closingcirclebr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && c == ')' ) {
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

int stop_on_closingbr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && c == ']' ) {
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

int stop_on_openbr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && c == '[' ) {
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

int stop_on_dot( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv ) {
	if ( brlvl == 0 && c == '.' ) {
		size_t * stopped_offset = priv;
		*stopped_offset = idx;
		return DPC_STOP;
	}
	return DPC_CONTINUE;
}

void pcp_pars(pcp_t * pcp) {
	int idx = 0;
	for (;idx<PCP_HT_SIZE;idx++) {
		pcp_piece * it = pcp->pieces[idx];
		while (it) {
			if ( it->unparsed ) {
				pcp->piece_being_parsed = it;
				it->parsed = pcp_do_parse(it->unparsed, pcp, NULL, NULL);
				if (!it->parsed)
					return;
			}
			it = it->next;
		}
	}
}

char * piece_name(pcp_piece * p) {
	char * v_name = vtopchar(p->name);
	return v_name;
}

char * piece_ns(pcp_piece * p) {
	char * v_ns = rtopchar(p->ns);
	return v_ns;
}

static void pcp_write_data(vecstring * src, size_t idx_from, size_t idx_to, vecstring * dst);

static int find_end_of_data_mark ( vecstring * src, int position, void * priv ) {
	size_t * marker_stop = (size_t *)priv;
	*marker_stop = position;
	return 0;
}

static int find_end_of_data ( vecstring * src, int position, void * priv ) {
	size_t * marker_stop = (size_t *)priv;
	*marker_stop = position;
	return 0;
}

vecstring * pcp_do_parse(vecstring * src, pcp_t * pcp, do_parse_callback dpc, void * dpc_priv) {
	viteration_t v = VITERATION_INIT;
	size_t idx = 0;
	size_t copy_from_idx = 0; // копировать данные с индекса по индекс
	int brlvl = 0; // уровень вложенности
	uint8_t fx[2] = {0,0};
	vecstring * dst = new_vecstring(0);
	
	for (;idx<src->length;idx++) {
		fx[0] = fx[1];
		fx[1] = vstring_at(src, idx, &v);
		
		if (dpc) {
			if (!dpc(src, dst, idx, fx[1], brlvl, dpc_priv)) {
				pcp_write_data( src, copy_from_idx, idx, dst);
				return dst;
			}
		}
		
		// здесь идет обработка тега $(data[marker] ... [marker])$
		if (brlvl==0 && vstrcmp2(src, "$(data[", idx)) {
			// все, что находится до этого тега должно быть сохранено!
			
			size_t marker_stop = -1;
			vfind_pchar(src, "]", idx+7, find_end_of_data_mark, &marker_stop);
			if (marker_stop !=-1) {
				vecstring * marker = subvstr(src, idx+7, marker_stop-idx-7);
				
				// now, look for the end of data
				size_t marker_end_stop = -1;
				char * marker_end = (char *) malloc(marker->length+5);
				vdump_to_ptr( marker, marker_end+1);
				marker_end[0] = '[';
				marker_end[marker->length+1] = ']';
				marker_end[marker->length+2] = ')';
				marker_end[marker->length+3] = '$';
				marker_end[marker->length+4] = 0;
				
				vfind_pchar(src, marker_end, marker_stop+1, find_end_of_data, &marker_end_stop);
				if ( marker_end_stop != -1 ) {
					// перед тем, как обработать этот тег необходимо сохранить все данные до него
					pcp_write_data(src, copy_from_idx, idx, dst);
					
					vecstring * data = subvstr(src, marker_stop+1, marker_end_stop - marker_stop-1);
					PCP_COMMAND cm = { .command = CMD_DATA, .data.length = data->length };
					vwrite(dst, &cm, sizeof(cm));
					vmove(dst, data);
					idx = marker_end_stop+marker->length+3;
					copy_from_idx = idx+1;
					fx[0] = 0;
					fx[1] = 0;
				}
				free(marker_end);
				delete_vecstring(marker);
				continue;
			}
		}
		
		if (brlvl>0 && vstrcmp2(src, "$(data[", idx)) {
			size_t marker_stop = -1;
			vfind_pchar(src, "]", idx+7, find_end_of_data_mark, &marker_stop);
			if (marker_stop !=-1) {
				vecstring * marker = subvstr(src, idx+7, marker_stop-idx-7);
				size_t marker_end_stop = -1;
				char * marker_end = (char *) malloc(marker->length+5);
				vdump_to_ptr( marker, marker_end+1);
				marker_end[0] = '[';
				marker_end[marker->length+1] = ']';
				marker_end[marker->length+2] = ')';
				marker_end[marker->length+3] = '$';
				marker_end[marker->length+4] = 0;
				
				vfind_pchar(src, marker_end, marker_stop+1, find_end_of_data, &marker_end_stop);
				if ( marker_end_stop != -1 ) {
					// нашли конец
					idx = marker_end_stop+marker->length+3;
					fx[0] = 0;
					fx[1] = 0;
				}
				
				free(marker_end);
				delete_vecstring(marker);
			}
		}
		
		
		if ( fx[0] == '$' && fx[1] == '(' ) {
			brlvl++;
			if (brlvl==1) {
				pcp_write_data(src, copy_from_idx, idx-1, dst);
				copy_from_idx = idx+1;
			}
			fx[0] = 0; fx[1] = 0;
			continue;
		}
		
		if ( fx[0] == ')' && fx[1] == '$' ) {
			brlvl--;
			if (brlvl<0) {
				char * pn = piece_name(pcp->piece_being_parsed);
				char * ns = piece_ns(pcp->piece_being_parsed);
				pcp_error(pcp, "pcp brackets mismatch: too many closing brackets in [%s:%s]\n", ns, pn);
				free(ns); free(pn);
				return NULL;
			}
			if (brlvl==0) {
				int parsed = 0;
				int jdx = pcp_max_handler_id(pcp);
				pcp_handler * hit = NULL;
				vecstring * inner = subvstr( src, copy_from_idx, idx-1-copy_from_idx );
				for (hit = pcp_get_handler(pcp,jdx);jdx>0;--jdx,hit = pcp_get_handler(pcp,jdx)) {
					if (hit) {
						
						if (!hit->cb) {
							pcp_error(pcp, "extension id %d doesn't have any parsing callback\n", hit->probe_command);
							return NULL;
						}
						vecstring * parse_result = hit->cb( inner, pcp, &parsed);
						
						if ( parsed ) {
							if (!parse_result) {
								char * pn = piece_name(pcp->piece_being_parsed);
								char * ns = piece_ns(pcp->piece_being_parsed);
								pcp_error(pcp, "failed to parse piece [%s:%s]\n", ns, pn);
								free(ns); free(pn);
								return NULL;
							}
							vmove(dst, parse_result);
							break;
						}
					}
				}
				delete_vecstring(inner);
				copy_from_idx = idx+1;
			}
			fx[0] = 0; fx[1] = 0;
			continue;
		}
	}
	// после всего просто добавляем данные в конце
	if ( src->length - copy_from_idx > 0) {
		pcp_write_data( src, copy_from_idx, src->length, dst);
	} else if (src->length == 0) {
		PCP_COMMAND cm = { .command = CMD_DATA, .data.length = 0 };
		vwrite(dst, &cm, sizeof(cm));
	}
	return dst;
}

static void pcp_write_data(vecstring * src, size_t idx_from, size_t idx_to, vecstring * dst) {
	PCP_COMMAND cm = { .command = CMD_DATA, .data.length = idx_to - idx_from };
	vwrite(dst, &cm, sizeof(cm));
	vmove(dst, subvstr(src, idx_from, idx_to-idx_from));
}
