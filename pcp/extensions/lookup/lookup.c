#include "pcp/pcp.h"

/* это расширение инициализируется всегда
*/

static vecstring * parse_lookup ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t lookup_xplain( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * lookup_exec ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_lookup = {
	.cb = parse_lookup,
	.cb_explain = lookup_xplain,
	.cb_exec = lookup_exec,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = CMD_LOOKUP
};

vecstring * lookup_varstorage ( vecstring * arg );
void setvar_varstorage( vecstring * arg, vecstring * val );

static lookup_exec_hook default_lookup_exec_hook = lookup_varstorage;
static set_exec_hook default_set_exec_hook = setvar_varstorage;

set_exec_hook get_servar_hook() {
	return default_set_exec_hook;
}

lookup_exec_hook get_lookupvar_hook() {
	return default_lookup_exec_hook;
}

void set_lookup_hook( lookup_exec_hook f ) {
	default_lookup_exec_hook = f;
}

void set_setvar_hook( set_exec_hook f ) {
	default_set_exec_hook = f;
}

static vecstring * parse_lookup ( vecstring * src, pcp_t * pcp, int * parsed ) {
	uint32_t cm = CMD_LOOKUP;
	vecstring * dst = new_vecstring(0);

	vwrite(dst, &cm, sizeof(uint32_t));
	vecstring * lookup_var_name = pcp_do_parse(src, pcp, NULL, NULL);
	if (!lookup_var_name) {
		delete_vecstring(dst);
		return NULL;
	}
	cm = lookup_var_name->length;
	vwrite(dst, &cm, sizeof(uint32_t));
	vmove(dst, lookup_var_name);
	*parsed = 1; // всегда говорим, что мы сделали это
	return dst;
}

static size_t lookup_xplain( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == CMD_LOOKUP ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_LOOKUP, ARG=\n");
		vecstring * cmdata = subvstr( parsed, 8, sz );
		size_t ret = 8 + pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		return ret;
	}
	return 0;
}

static vecstring * lookup_exec ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0 ) && cm == CMD_LOOKUP ) {
		// yep, it's lookup command
		vecstring * cmdata = subvstr( src, 8, sz );
		vecstring * arg = pcp_do_exec ( cmdata, pcp );
		if (!arg) return NULL;
		delete_vecstring(cmdata);

		cmdata = default_lookup_exec_hook(arg);
		if (!cmdata)
			pcp_error(pcp, "storage might be not initialized, are you using default lookup storage?\n");
		delete_vecstring(arg);

		*parsed = 1;
		return cmdata;
	}
	return NULL;
}
