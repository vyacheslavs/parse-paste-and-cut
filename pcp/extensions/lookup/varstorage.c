/* this is a default implementation of varstorage based on hash table */

#include "pcp/pcp.h"
#include <assert.h>
#include <malloc.h>
#include "mempool/mempool.h"
#include <string.h>

#ifndef VARSTORAGE_HTSIZE
#define VARSTORAGE_HTSIZE 256
#endif

typedef struct __varstorage_node {
	vecstring * val;
	vecstring * key;
	struct __varstorage_node * next;
} varstorage_node;

static varstorage_node ** ht = NULL;
static mempool ht_pool;
vecstring * find_vecval ( vecstring * key );

void setvar_varstorage( vecstring * arg, vecstring * val ) {
	if (!ht)
		return;
	// если найдена такая переменная, ее необходимо удалить
	uint64_t key = vecstring_keyval ( arg ) % VARSTORAGE_HTSIZE;
	
	varstorage_node ** it = &ht[key];
	for (;*it;it = &(*it)->next) {
		if (vstr_compare((*it)->key, arg)) {
			delete_vecstring((*it)->val);
			delete_vecstring((*it)->key);
			varstorage_node * tokill = *it;
			*it = (*it)->next;
			mp_free(&ht_pool, tokill);
			break;
		}
	}
	
	struct __varstorage_node * newnode = (struct __varstorage_node *) mp_alloc(&ht_pool);
	newnode->val = vshare(val);
	newnode->key = vshare(arg);
	newnode->next = ht[key];
	ht[key] = newnode;
}

vecstring * lookup_varstorage ( vecstring * arg ) {
	if (!ht)
		return NULL;
	vecstring * ret = find_vecval(arg);
	if (ret)
		return vshare(ret);
	ret = new_vecstring(0);
	return ret;
}

void init_varstorage() {
	ht = malloc ( sizeof(varstorage_node *) * VARSTORAGE_HTSIZE );
	assert(ht);
	memset(ht, 0, sizeof(varstorage_node *) * VARSTORAGE_HTSIZE );
	mp_init(&ht_pool, sizeof(varstorage_node));
}
void fini_varstorage() {
	varstorage_node * it, *n;
	int idx;
	for (idx=0;idx<VARSTORAGE_HTSIZE;idx++) {
		it = ht[idx];
		while(it) {
			n = it->next;
			delete_vecstring(it->val);
			delete_vecstring(it->key);
			mp_free(&ht_pool, it);
			it = n;
		}
	}
	mp_fini(&ht_pool);
	free(ht);
}

vecstring * find_vecval ( vecstring * key ) {
	if (!ht)
		return NULL;
	uint64_t keyval = vecstring_keyval(key) % VARSTORAGE_HTSIZE;
	varstorage_node * it = ht[keyval];
	while (it) {
		if ( vstr_compare( key, it->key) )
			return it->val;
		it = it->next;
	}
	return NULL;
}
