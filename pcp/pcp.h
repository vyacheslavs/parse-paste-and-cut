#ifndef __PCP_H
#define __PCP_H

#include <stdio.h>
#include "vecstring/vecstring.h"
#include "pcp_commands.h"

#ifndef PCP_HT_SIZE
#define PCP_HT_SIZE 256
#endif

#define CMD_END_MARKER 0x80000000
#define COMMAND_REGISTRATOR_SZ 256

#define PCP_LOG_CRITICAL 0
#define PCP_LOG_ERROR    1
#define PCP_LOG_INFO     2
#define PCP_LOG_WARNING  3
#define PCP_LOG_DEBUG    4

typedef struct __PCP_COMMAND {
	uint32_t command;
	union {
		uint32_t length;
	} __attribute__((packed)) data;
}__attribute__((packed)) PCP_COMMAND;

typedef struct __pcp_piece {
	struct __pcp_piece * next;
	vecstring * name;
	refstring * ns;
	vecstring * unparsed;
	vecstring * parsed;
	vecstring * compressed;
} pcp_piece;

struct __pcp_t;

/* handler должен возвращать NULL - в случае, если произошла ошибка
*/

typedef vecstring * (*pcp_tag_handler) ( vecstring * src, struct __pcp_t * pcp, int * parsed );
typedef size_t (*pcp_xplain_handler) ( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
typedef vecstring * (*pcp_exec_handler) ( struct __pcp_t * pcp, vecstring * src, int * parsed );
typedef vecstring * (*lookup_exec_hook) ( vecstring * arg );
typedef void (*set_exec_hook) ( vecstring * arg, vecstring * val );
typedef void (*pcp_handler_init_handler) (void *reserved);
typedef void (*pcp_handler_fini_handler) (void *reserved);

void init_refstring_printfs();
void init_vecstring_printfs();

void set_lookup_hook( lookup_exec_hook f );
void set_setvar_hook( set_exec_hook f );
set_exec_hook get_servar_hook();
lookup_exec_hook get_lookupvar_hook();
vecstring * find_vecval ( vecstring * key );

typedef struct __pcp_handler {
	pcp_tag_handler cb;
	pcp_xplain_handler cb_explain;
	pcp_exec_handler cb_exec;
	pcp_handler_init_handler cb_init;
	pcp_handler_fini_handler cb_fini;
	uint32_t probe_command;
	void * so_library;
	int * so_library_refcount;
} pcp_handler;

pcp_handler * pcp_first_handler();
void pcp_intend( int lvl, FILE * out );
int pcp_read_command ( uint32_t * cm, uint32_t * sz, vecstring * src, size_t oft );

void pcp_handler_registry_init( struct __pcp_t * );
void pcp_handler_registry_fini( struct __pcp_t * );
void pcp_register_handler ( struct __pcp_t * , pcp_handler * handler );
pcp_handler * pcp_get_handler ( struct __pcp_t * , uint32_t probe_command );
int pcp_max_handler_id( struct __pcp_t * );

#define DPC_CONTINUE 1
#define DPC_STOP     0

void pcp_set_log_level ( struct __pcp_t * pcp, unsigned int new_level );
unsigned int pcp_get_log_level (struct __pcp_t * pcp);
void pcp_log_code ( struct __pcp_t * pcp, unsigned int code );
void pcp_dont_log ( struct __pcp_t * pcp, unsigned int code );
void pcp_log_system (int doit);

void pcp_error(struct __pcp_t * pcp, const char * fmt, ... );
void pcp_log(struct __pcp_t * pcp, unsigned int log_level, unsigned int log_code, const char * fmt, ...);
int pcp_could_be_logged (struct __pcp_t * pcp, unsigned int log_level, unsigned int log_code );

typedef void (*error_callback) ( const char * what, uint32_t what_len );
typedef void (*log_callback) ( unsigned int level, unsigned int code, const char * what, uint32_t what_len );
typedef int (*do_parse_callback) ( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );

char stop_on_char();
int stop_on_eq( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );
int stop_on_comma( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );
int stop_on_closingbr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );
int stop_on_closingcirclebr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );
int stop_on_question( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );
int stop_on_openbr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );
int stop_on_comma_or_closingbr( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );
int stop_on_dot( vecstring * src, vecstring * dst, size_t idx, uint8_t c, int brlvl, void * priv );

void pcp_default_error_callback ( const char * what, uint32_t what_len );
void pcp_default_log_callback ( unsigned int level, unsigned int code, const char * what, uint32_t what_len );

typedef struct __pcp_t {
	vecstring * buf;
	pcp_piece ** pieces;
    pcp_piece * working_on;
	pcp_handler ** handlers;
	int max_handler_id;
	error_callback errcb;
    log_callback logcb;
    unsigned int log_level;
    uint8_t * log_codes;
    unsigned int log_codes_size;
    uint8_t * log_codes_except;
    unsigned int log_codes_except_size;
	pcp_piece * piece_being_parsed;
} pcp_t;

vecstring * pcp_do_parse(vecstring * src, pcp_t * pcp, do_parse_callback cb, void * priv);
vecstring * pcp_do_exec(vecstring * src, pcp_t * pcp);
size_t pcp_do_xplain ( pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );

void pcp_init(pcp_t * pcp);
void pcp_load(pcp_t * pcp, const char * file);
void pcp_fini(pcp_t * pcp);
void pcp_pars(pcp_t * pcp);
void pcp_save(pcp_t * pcp, const char * file);
void pcp_xplain(pcp_t * pcp, FILE * stream);
void pcp_z(pcp_piece * piece);
void pcp_uz(pcp_piece * piece);

vecstring * pcp_exec(pcp_t * pcp, const char * ns, const char * piecename);
pcp_piece * pcp_find_piece ( pcp_t * pcp, const char * ns, const char * piecename );

char * piece_name(pcp_piece * p);
char * piece_ns(pcp_piece * p);

#endif
