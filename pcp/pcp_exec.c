#include "pcp.h"
#include <assert.h>

vecstring * pcp_exec(pcp_t * pcp, const char * ns, const char * piecename) {
	pcp_piece * piece = pcp_find_piece(pcp,ns, piecename);
	if (!piece) {
		pcp_error(pcp, "Can not find piece while executing [%s:%s]\n", ns, piecename);
		return NULL;
	}
	if (!piece->parsed) {
		pcp_error(pcp, "can not execute piece [%s:%s], parse error\n", ns, piecename);
		return NULL;
	}
	assert(piece->parsed);
    pcp->working_on = piece;
	return pcp_do_exec ( piece->parsed, pcp );
}

vecstring * pcp_do_exec(vecstring * src, pcp_t * pcp) {
	size_t idx = 0;
	uint32_t cm, sz;
	vecstring * dst = new_vecstring(0);
	while ( idx < src->length ) {
		if ( pcp_read_command( &cm, &sz, src, idx ) && cm == CMD_DATA ) {
			// это просто данные
			vmove(dst, subvstr(src, idx+8, sz));
		} else {
			// пробуем объяснить через handlers
			vecstring * cmdata = subvstr(src, idx, sz+8);
			pcp_handler * hit = pcp_get_handler(pcp,cm);
			int parsed = 0;
			if (hit && hit->cb_exec) {
					vecstring * presult = hit->cb_exec ( pcp, cmdata, &parsed );
					if ( parsed ) {
						if (!presult)
							return NULL;
						vmove(dst, presult);
					}
			}
			delete_vecstring(cmdata);
			if (!parsed) {
				pcp_error(pcp, "Warning, unknown command id %d skipped\n", cm);
			}
		}
		idx+= 8 + sz;
	}
	return dst;
}
