#include "pcp/pcp.h"
#include "pcp/macros.h"
#include <math.h>
#include <stdlib.h>

/*
  - $(math[<op>,<val>]val2)$
       op - inc|dec|sup|div|abs|flr|cei|rnd
*/

static vecstring * parse_math ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_math( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_math ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_math = {
	.cb = parse_math,
	.cb_explain = xplain_math,
	.cb_exec = exec_math,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_MATH
};

static vecstring * parse_math ( vecstring * src, pcp_t * pcp, int * parsed ) {
	const char key[] = "math[";
	if ( vstrcmp(src, key) ) {
		PARSE_START(COMMAND_MATH);
		// берем op
		PARSE(op,stop_on_comma);
		// берем val
		PARSE(val,stop_on_closingbr);
		
		// берем вторую часть, которая находится за закрывающейся
		vecstring * argdata = subvstr(src, parsing_offset, SIZE_MAX);
		vecstring * val2 = pcp_do_parse(argdata, pcp, NULL, NULL);
		delete_vecstring(argdata);
		
		// теперь записываем полностью команду
		cm = op->length + val->length + 3*8 + val2->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = COMMAND_MATH_OP;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = op->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,op);
		cm = COMMAND_MATH_VAL;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = val->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,val);
		cm = COMMAND_MATH_VAL2;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = val2->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,val2);
		return dst;
	}
	return NULL;
}

static size_t xplain_math( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_MATH ) {
		XPLAIN_START
		if ( EXPECT_NEXT_COMMAND(COMMAND_MATH_OP) ) {
			XPLAIN("CMD_MATH, OP=");
			if ( EXPECT_NEXT_COMMAND(COMMAND_MATH_VAL) ) {
				XPLAIN("CMD_MATH, VAL=");
				if ( EXPECT_NEXT_COMMAND(COMMAND_MATH_VAL2) ) {
					XPLAIN("CMD_MATH, VAL2=");
					return ret;
				}
			}
		}
	}
}

static vecstring * exec_math ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_MATH ) {
		EXEC_START(COMMAND_MATH);
		EXEC_GET_ARG(COMMAND_MATH_OP,op);
		EXEC_GET_ARG(COMMAND_MATH_VAL,val);
		EXEC_GET_ARG(COMMAND_MATH_VAL2,val2);
		
		char * val_p = vtopchar(val);
		char * val2_p = vtopchar(val2);
		double val_d = atof(val_p);
		double val2_d = atof(val2_p);
		free(val_p);free(val2_p);
		
		
		int res = 0;
		int res_as_double = 1;
		if ( vstrcmp(op, "inc") ) {
			//increment
			val_d += val2_d;
		} else if ( vstrcmp(op, "dec") ) {
			val_d -= val2_d;
		} else if ( vstrcmp(op, "sup") ) {
			val_d *= val2_d;
		} else if ( vstrcmp(op, "div") ) {
			val_d /= val2_d;
		} else if ( vstrcmp(op, "abs") ) {
			val_d = abs(val_d);
		} else if ( vstrcmp(op, "flr") ) {
			res = floor(val_d);
			res_as_double = 0;
		} else if ( vstrcmp(op, "cei") ) {
			res = ceil(val_d);
			res_as_double = 0;
		} else if ( vstrcmp(op, "rnd") ) {
			res_as_double = 2;
		}
		
		// пакуем результат
		if ( res_as_double == 1) {
			char fmt[0xfff];
			snprintf(fmt, 0xfff-1, "%.10f", val_d);
			vecstring * out = vecstring_from_pchar(fmt);
			vmove(dst,out);
		} else if ( res_as_double == 0 ) {
			char fmt[0xfff];
			snprintf(fmt, 0xfff-1, "%d", res);
			vecstring * out = vecstring_from_pchar(fmt);
			vmove(dst,out);
		} else if ( res_as_double == 2 ) {
			char fmtfmt[0xff];
			snprintf(fmtfmt, 0xfff-1, "%%.%df", (int)val2_d);
			char fmt[0xfff];
			snprintf(fmt, 0xfff-1, fmtfmt, val_d);
			vecstring * out = vecstring_from_pchar(fmt);
			vmove(dst,out);
		}
		
		delete_vecstring(op);
		delete_vecstring(val);
		delete_vecstring(val2);
		return dst;
	}
}
