#include "pcp/pcp.h"

extern pcp_handler handler_inc;
extern pcp_handler handler_math;

pcp_handler * register_pcp_handlers[] = {
	&handler_inc,
	&handler_math,
	NULL
};
