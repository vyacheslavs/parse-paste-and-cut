#include "pcp/pcp.h"
#include <malloc.h>

static vecstring * parse_inc ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_inc( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_inc ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_inc = {
	.cb = parse_inc,
	.cb_explain = xplain_inc,
	.cb_exec = exec_inc,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_INC
};

static vecstring * parse_inc ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "inc ") ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_INC;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		vecstring * val = subvstr(src, 4, SIZE_MAX);
		vecstring * outval = pcp_do_parse( val, pcp, NULL, NULL );
		delete_vecstring(val);
		
		cm = outval->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, outval);
		return dst;
	}
	return NULL;
}

static size_t xplain_inc( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_INC ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_INC, DATA=\n");
		vecstring * cmdata = subvstr( parsed, 8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		return sz+8;
	}
	return 0;
}

static vecstring * exec_inc ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_INC ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		vecstring * cmdata = subvstr( src, 8, sz );
		vecstring * val = pcp_do_exec( cmdata, pcp );
		delete_vecstring(cmdata);
		// после того, как мы получили данные необходимо сделать 
		vecstring * numval = get_lookupvar_hook()(val);
		char * numv = vtopchar(numval);
		delete_vecstring(numval);
		int64_t numv_i = atoll(numv);
		numv_i++;
		char newnum_v[0xff];
		snprintf(newnum_v, 0xff-1,"%lld", numv_i);
		numval =  vecstring_from_pchar( newnum_v );
		get_servar_hook() ( val, numval );
		delete_vecstring(numval);
		delete_vecstring(val);
		return dst;
	}
	return NULL;
}
