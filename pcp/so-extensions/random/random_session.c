#include "pcp/pcp.h"
#include <malloc.h>
#include <stdlib.h>

static vecstring * parse_random_session ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_random_session( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_random_session ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_random_session = {
	.cb = parse_random_session,
	.cb_explain = xplain_random_session,
	.cb_exec = exec_random_session,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_RANDOM_SESSION
};

static vecstring * parse_random_session ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "random[session]") ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_RANDOM_SESSION;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = 0;
		vwrite(dst, &cm, sizeof(uint32_t));
		return dst;
	}
	return NULL;
}

static size_t xplain_random_session( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_RANDOM_SESSION ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_RANDOM_SESSION\n");
		return sz+8;
	}
	return 0;
}

static vecstring * exec_random_session ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_RANDOM_SESSION ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		// now generate 16 byte random session id
		int i = 0;
		srand(time(NULL));
		for (;i<32;i++) {
			int j = (int) (3 * (rand() / (RAND_MAX + 1.0)));
			char c;
			switch (j) {
				case 0: {
					c = '0' + (int) (10 * (rand() / (RAND_MAX + 1.0)));
					break;
				}
				case 1: {
					c = 'a' + (int) (26 * (rand() / (RAND_MAX + 1.0)));
					break;
				}
				default: {
					c = 'A' + (int) (26 * (rand() / (RAND_MAX + 1.0)));
					break;
				}
			}
			vwrite(dst, &c, 1);
		}
		return dst;
	}
	return NULL;
}
