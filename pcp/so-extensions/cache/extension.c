#include "pcp/pcp.h"

extern pcp_handler handler_cache;
extern pcp_handler handler_cache_sys;
extern pcp_handler handler_cachekill;

pcp_handler * register_pcp_handlers[] = {
	&handler_cache,
	&handler_cache_sys,
	&handler_cachekill,
	NULL
};
