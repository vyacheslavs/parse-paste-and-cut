Cache extension
---------------

# Syntax:

    $(cache[<cachehandle>,<cachetimesec>] ... <data> ...)$
    $(cache-sys-<on/off>)$
    $(cachekill[<cachehandle>])$

# Purpose:

    $(cache)$ caches <data> for <cachetimesec> seconds, and bind this cache to handle <cachehandle>.

    $(cache-sys-on)$ - turns on cache system, while $(cache-sys-off)$ turns off one.

    $(cachekill)$ - kills cache handle
