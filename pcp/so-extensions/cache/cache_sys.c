#include "pcp/pcp.h"
#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "cache.h"

/*  $(cache-sys-<on/off>)$ - включить/выключить систему кэширования */

static vecstring * parse_cache_sys ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_cache_sys( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_cache_sys ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_cache_sys = {
	.cb = parse_cache_sys,
	.cb_explain = xplain_cache_sys,
	.cb_exec = exec_cache_sys,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_CACHE_SYS
};

static vecstring * parse_cache_sys ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "cache-sys-on") ) {
		*parsed = 1; // говорим, что мы беремся за парсинг сразу
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_CACHE_SYS;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = 1;
		vwrite(dst, &cm, sizeof(uint32_t));
		uint8_t on = 1;
		vwrite(dst, &on, sizeof(uint8_t));
		return dst;
	}
	if ( vstrcmp(src, "cache-sys-off") ) {
		*parsed = 1; // говорим, что мы беремся за парсинг сразу
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_CACHE_SYS;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = 1;
		vwrite(dst, &cm, sizeof(uint32_t));
		uint8_t on = 0;
		vwrite(dst, &on, sizeof(uint8_t));
		return dst;
	}
}

static size_t xplain_cache_sys( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_CACHE_SYS ) {
		pcp_intend(lvl, stream);
		uint8_t sw = 0;
		vread(parsed, &sw, 8, 1);
		fprintf(stream, "CMD_CACHE_SYSTEM SWITCH %s\n", sw?"ON":"OFF");
		return 9;
	}
	return 0;
}

static vecstring * exec_cache_sys ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
		uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_CACHE_SYS ) {
		vecstring * dst = new_vecstring(0);
		*parsed = 1;
		uint8_t sw = 0;
		vread(src, &sw, 8, 1);
		set_cache_sys(sw);
		return dst;
	}
	return NULL;
}
