#include "pcp/pcp.h"
#include "pcp/macros.h"
#include <unistd.h>
#include <malloc.h>
#include <string.h>

/*
    cachekill extension:

    usage:
        $(cachekill[<cachehandle>])$
*/

static vecstring * parse_cachekill ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_cachekill( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_cachekill ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_cachekill = {
	.cb = parse_cachekill,
	.cb_explain = xplain_cachekill,
	.cb_exec = exec_cachekill,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_CACHE_KILL
};

static vecstring * parse_cachekill ( vecstring * src, pcp_t * pcp, int * parsed ) {
	const char key[] = "cachekill[";
	if ( vstrcmp(src, key) ) {
		PARSE_START(COMMAND_CACHE_KILL);
		// берем handle
		PARSE(handle,stop_on_closingbr);
		cm = handle->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,handle);
		return dst;
	}
}

static size_t xplain_cachekill( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_CACHE_KILL ) {
		XPLAIN_START
		ret = 0;
		XPLAIN("CMD_CACHE_KILL, HANDLE=");
		return ret;
	}
}

static vecstring * exec_cachekill ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_CACHE_KILL ) {
		EXEC_START(COMMAND_CACHE_KILL);
		vecstring * cmdata = subvstr( src, oft, sz );
		vecstring * handle = pcp_do_exec (cmdata, pcp);
		delete_vecstring(cmdata);
		if (!handle) {
			pcp_error(pcp, "error while evaluating command argument\n" );
			return NULL;
		}
		// теперь у меня есть handle
		char * handle_p = vtopchar(handle);
		char * handle_fp = (char *) malloc ( strlen(handle_p)+1+100 );
		snprintf(handle_fp, strlen(handle_p)+100, "/tmp/pcp-cache/%s", handle_p);
		unlink(handle_fp);
		free(handle_fp);
		free(handle_p);
		delete_vecstring(handle);
		return dst;
	}
	return NULL;
}
