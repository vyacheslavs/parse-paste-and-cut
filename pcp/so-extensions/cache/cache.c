#include "pcp/pcp.h"
#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

/* $(cache[<cachehandle>,<cachetimesec>] ... данные ...)$ - закэшировать данные на cachetimesec и назначить имя кэшу в cachehandle */

static vecstring * parse_cache ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_cache( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_cache ( struct __pcp_t * pcp, vecstring * src, int * parsed );
static uint8_t cache_sys_sw = 1;

pcp_handler handler_cache = {
	.cb = parse_cache,
	.cb_explain = xplain_cache,
	.cb_exec = exec_cache,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_CACHE
};

void set_cache_sys(uint8_t sw) {
	cache_sys_sw = sw;
}

static vecstring * parse_cache ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "cache[") ) {
		*parsed = 1; // говорим, что мы беремся за парсинг сразу
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_CACHE;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		size_t handle_offset = -1;
		vecstring * argdata = subvstr(src, 6, SIZE_MAX);
		vecstring * handle = pcp_do_parse( argdata, pcp, stop_on_comma, &handle_offset );
		delete_vecstring(argdata);
		
		if ( handle_offset == -1 ) {
			pcp_error(pcp, "syntax error while parsing <cache>: handle\n" );
			delete_vecstring(handle);
			delete_vecstring(dst);
			return NULL;
		}
		
		size_t time_offset = -1;
		argdata = subvstr(src, 6+handle_offset+1, SIZE_MAX);
		vecstring * tim = pcp_do_parse( argdata, pcp, stop_on_closingbr, &time_offset );
		delete_vecstring(argdata);
		
		if ( time_offset == -1 ) {
			pcp_error(pcp, "syntax error while parsing <cache>: time\n" );
			delete_vecstring(handle);
			delete_vecstring(tim);
			delete_vecstring(dst);
			return NULL;
		}
		
		// теперь остались только данные
		argdata = subvstr(src, 6+handle_offset+1+time_offset+1, SIZE_MAX);
		vecstring * data = pcp_do_parse( argdata, pcp, stop_on_closingbr, &time_offset );
		delete_vecstring(argdata);
		// записываем комманду
		cm = 3*8+handle->length + tim->length + data->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = COMMAND_CACHE_HANDLE;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = handle->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, handle);
		cm = COMMAND_CACHE_TIME;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = tim->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, tim);
		cm = COMMAND_CACHE_DATA;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = data->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,data);
		return dst;
	}
	return NULL;
}

static size_t xplain_cache( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_CACHE ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_CACHE\n");
		size_t ret = 8;
		if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_CACHE_HANDLE ) {
			pcp_intend(lvl, stream);
			ret+=8;
			vecstring * cmdata = subvstr( parsed, ret, sz );
			fprintf(stream, "CMD_CACHE, HANDLE=\n");
			pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
			delete_vecstring(cmdata);
			ret+=sz;
			if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_CACHE_TIME ) {
				pcp_intend(lvl, stream);
				ret+=8;
				vecstring * cmdata = subvstr( parsed, ret, sz );
				fprintf(stream, "CMD_CACHE, TIME=\n");
				pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
				delete_vecstring(cmdata);
				ret+=sz;
				if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_CACHE_DATA ) {
					pcp_intend(lvl, stream);
					ret+=8;
					vecstring * cmdata = subvstr( parsed, ret, sz );
					fprintf(stream, "CMD_CACHE, DATA=\n");
					pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
					delete_vecstring(cmdata);
					ret+=sz;
					return ret;
				}
			}
		}
	}
	return 0;
}

static vecstring * exec_cache ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_CACHE ) {
		vecstring * dst = new_vecstring(0);
		*parsed = 1;
		size_t ret = 8;
		if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_CACHE_HANDLE ) {
			ret+=8;
			vecstring * cmdata = subvstr( src, ret, sz );
			vecstring * handle = pcp_do_exec ( cmdata, pcp );
			delete_vecstring(cmdata);
			ret+=sz;
			if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_CACHE_TIME ) {
				ret+=8;
				vecstring * cmdata = subvstr( src, ret, sz );
				vecstring * tim = pcp_do_exec ( cmdata, pcp );
				delete_vecstring(cmdata);
				ret+=sz;
				if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_CACHE_DATA ) {
					ret+=8;
					vecstring * cmdata = subvstr( src, ret, sz );
					// vecstring * data = pcp_do_exec ( cmdata, pcp );
					ret+=sz;
					
					// сначала смотрим есть ли директория для кэша
					struct stat st;
					char * handle_pchar = vtopchar(handle);
					char * handle_fp = (char *) malloc ( strlen(handle_pchar)+1+100 );
					snprintf(handle_fp, strlen(handle_pchar)+100, "/tmp/pcp-cache/%s", handle_pchar);
					int exec_cache = 1; // будем ли исполнять код кэша или не?
					if (cache_sys_sw) {
						if (stat("/tmp/pcp-cache", &st) < 0) {
							// нет директории, создаем директорию
							mkdir("/tmp/pcp-cache", 0755);
						} else {
							// директория существует, пробуем открыть кэш
							int fd = open (handle_fp, O_RDONLY);
							if (fd>=0) {
								// кэш файл существует
								// устарел ли кэш?
								uint64_t expire, now = time(NULL);
								read(fd, &expire, 8);
								if (expire > now ) {
									// fprintf(stderr, "loading cache %s (expires in %lld)\n", handle_fp, (expire-now));
									// нет, еще не устарел
									// запрещаем исполнение кэша
									exec_cache = 0;
									// читаем данные из кэша
									int n = 1;
									while (n>0) {
										refstring * refd = new_refstring(1024);
										n = read(fd, refstring_ptr(refd), 1024);
										vmove(dst, vecstring_from_refstring(refd, 0, n));
									}
								} else {
									// fprintf(stderr, "cache expired for %s\n", handle_fp);
								}
								close(fd);
							}
						}
					}
					if ( exec_cache ) {
						vecstring * data = pcp_do_exec ( cmdata, pcp );
						if (cache_sys_sw) {
							// записываем данные в кэш
							int fd = open(handle_fp, O_RDWR|O_CREAT|O_TRUNC, 0644);
							if (fd>=0) {
								// записываем в кэш когда он устаревает
								char * ptime = vtopchar(tim);
								uint64_t now = time(NULL) + atoi(ptime);
								write(fd, &now, 8);
								// fprintf(stderr, "executing cache data, saving to cache: %s, %lld\n", handle_fp, now);
								
								vecstring_chain * it = data->head;
								int len = 0;
								while (it) {
									write(fd, refstring_ptr(it->refstr)+it->payload_offset, it->payload_size);
									it = it->next;
								}
								free(ptime);
								close(fd);
							}
						}
						vmove(dst,data);
					}
					free(handle_fp);
					free(handle_pchar);
					
					delete_vecstring(cmdata);
					delete_vecstring(handle);
					delete_vecstring(tim);
					return dst;
				}
			}
		}
	}
	return NULL;
}
