#include "pcp/pcp.h"

extern pcp_handler handler_system;

pcp_handler * register_pcp_handlers[] = {
	&handler_system,
	NULL
};
