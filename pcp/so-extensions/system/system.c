#include "pcp/pcp.h"
#include <malloc.h>

static vecstring * parse_system ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_system( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_system ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_system = {
	.cb = parse_system,
	.cb_explain = xplain_system,
	.cb_exec = exec_system,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_SYSTEM
};

static vecstring * parse_system ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "system ") ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_SYSTEM;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		vecstring * val = subvstr(src,7, SIZE_MAX);
		vecstring * outval = pcp_do_parse( val, pcp, NULL, NULL );
		delete_vecstring(val);
		
		cm = outval->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, outval);
		return dst;
	}
	return NULL;
}

static size_t xplain_system( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_SYSTEM ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_SYSTEM, DATA=\n");
		vecstring * cmdata = subvstr( parsed, 8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		return sz+8;
	}
	return 0;
}

static vecstring * exec_system ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_SYSTEM ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		vecstring * cmdata = subvstr( src, 8, sz );
		vecstring * val = pcp_do_exec( cmdata, pcp );
		delete_vecstring(cmdata);
		// после того, как мы получили данные необходимо сделать 
		char * sys = vtopchar(val);
		system(sys);
		free(sys);
		delete_vecstring(val);
		return dst;
	}
	return NULL;
}
