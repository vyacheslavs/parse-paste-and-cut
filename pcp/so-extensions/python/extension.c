#include "pcp/pcp.h"

extern pcp_handler handler_python;

pcp_handler * register_pcp_handlers[] = {
	&handler_python,
	NULL
};
