#include "pcp/pcp.h"
#include <malloc.h>
#include "pcp/macros.h"
#include <Python.h>

static struct python_exec_context_t {
	vecstring * dst;
	pcp_t * pcp;
	struct python_exec_context_t * next;
} * head = NULL;

static void push_exec_context ( vecstring * dst, pcp_t * pcp );
static void pop_exec_context();

static void init_python ( void * r );
static void fini_python ( void * r );
static vecstring * parse_python ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_python( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_python ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_python = {
	.cb = parse_python,
	.cb_explain = xplain_python,
	.cb_exec = exec_python,
	.cb_init = init_python,
	.cb_fini = fini_python,
	.probe_command = COMMAND_PYTHON
};

static vecstring * parse_python ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "python\n") || vstrcmp(src, "python ") ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_PYTHON;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		vecstring * val = subvstr(src,7, SIZE_MAX);
		vecstring * outval = pcp_do_parse( val, pcp, NULL, NULL );
		delete_vecstring(val);
		
		cm = outval->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, outval);
		return dst;
	}
	return NULL;
}

static size_t xplain_python( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_PYTHON ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_PYTHON, PARSE DATA=\n");
		vecstring * cmdata = subvstr( parsed, 8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		return sz+8;
	}
	return 0;
}

static void python_exec( const char * code, vecstring * dst, pcp_t * pcp );

static vecstring * exec_python ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_PYTHON ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		vecstring * cmdata = subvstr( src, 8, sz );
		vecstring * val = pcp_do_exec( cmdata, pcp );
		delete_vecstring(cmdata);
		// после того, как мы получили данные необходимо запарсить данные
		char * code = vtopchar(val);
		python_exec(code, dst, pcp);
		free(code);
		delete_vecstring(val);
		return dst;
	}
	return NULL;
}

static PyObject * cgi_write(PyObject *self, PyObject *args) {
	const char *what;
	if (!PyArg_ParseTuple(args, "s", &what))
		return NULL;
	vecstring * w = vecstring_from_pchar(what);
	vmove(head->dst, w);
	return Py_BuildValue("");
}

static PyObject * cgi_get(PyObject *self, PyObject *args) {
	const char *what;
	if (!PyArg_ParseTuple(args, "s", &what))
		return NULL;
	vecstring * arg = vecstring_from_pchar(what);
	vecstring * cmdata = get_lookupvar_hook()(arg);
	delete_vecstring(arg);
	char * val = vtopchar(cmdata);
	PyObject * ret = Py_BuildValue("s#", val, cmdata->length);
	
	free(val);
	delete_vecstring(cmdata);
	return ret;
}

static PyObject * cgi_set(PyObject *self, PyObject *args) {
	const char *var, *val;
	if (!PyArg_ParseTuple(args, "ss", &var, &val))
		return NULL;
	
	vecstring * vvar = vecstring_from_pchar(var);
	vecstring * vval = vecstring_from_pchar(val);
	get_servar_hook()(vvar, vval);
	delete_vecstring(vval);
	delete_vecstring(vvar);
	
	return Py_BuildValue("");
}

static PyObject * cgi_piece(PyObject *self, PyObject *args) {
	const char *fn, *pn;
	if (!PyArg_ParseTuple(args, "ss", &fn, &pn))
		return NULL;
	pcp_load(head->pcp, fn);
	pcp_piece * p = pcp_find_piece ( head->pcp, fn, pn );
	if (!p) {
		pcp_error(head->pcp, "python extension failure: piece not found: %s\n", pn);
		return NULL;
	} else if (!p->parsed) {
		pcp_error(head->pcp, "python extension failure: piece not parsed: %s\n", pn);
		return NULL;
	} else {
		vecstring * res = pcp_do_exec(p->parsed, head->pcp);
		if (!res)
			return NULL;
		char * respc = vtopchar(res);
		PyObject * ret = Py_BuildValue("s#", respc, res->length);
		free(respc);
		delete_vecstring(res);
		return ret;
	}
	return Py_BuildValue("");
}

static PyMethodDef a_methods[] = {
	{"write", cgi_write, METH_VARARGS, "Write something."},
	{"get", cgi_get, METH_VARARGS, "Get cgi variable"},
	{"set", cgi_set, METH_VARARGS, "Set cgi variable"},
	{"piece", cgi_piece, METH_VARARGS, "invoke piece"},
	{NULL, NULL, 0, NULL}
};

static void python_exec( const char * code, vecstring * dst, pcp_t * pcp ) {
	push_exec_context( dst, pcp );
	PyRun_SimpleString(code);
	pop_exec_context();
}

static void init_python ( void * r ) {
	Py_Initialize();
	PyObject * m = Py_InitModule("cgi", a_methods);
	if (m) {
		PySys_SetObject("stdout", m);
	}
}

static void fini_python ( void * r ) {
	Py_Finalize();
}

static void push_exec_context ( vecstring * dst, pcp_t * pcp ) {
	struct python_exec_context_t * new_context = malloc(sizeof(struct python_exec_context_t));
	new_context->next = head;
	new_context->pcp = pcp;
	new_context->dst = dst;
	head = new_context;
}

static void pop_exec_context() {
	struct python_exec_context_t * toremove = head;
	head = toremove->next;
	if (toremove)
		free(toremove);
}
