#include "pcp/pcp.h"
#include <malloc.h>
#include <libgen.h>
#include <string.h>
#include "pcp/macros.h"

/*
   piece extension:

   usage:
   $(piece[filename,piecename])$
*/


static vecstring * parse_piece ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_piece( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_piece ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_piece = {
	.cb = parse_piece,
	.cb_explain = xplain_piece,
	.cb_exec = exec_piece,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_PIECE
};

static vecstring * parse_piece ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "piece[";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_PIECE);
        PARSE_WITH_CUSTOM_FAIL_HANDLER_START(fn, stop_on_comma)
        {
            PARSE(apn, stop_on_closingbr);
            // если запятой нету, то считаем что fn = NULL, а остальная часть - это название куска
            vecstring * afn = new_vecstring(0);
            cm = afn->length + apn->length + 8*2;
            vwrite(dst, &cm, sizeof(uint32_t));

            SERIALIZE(COMMAND_PIECE, afn);
            SERIALIZE(COMMAND_PIECE, apn);
            return dst;
        }
        PARSE_WITH_CUSTOM_FAIL_HANDLER_END(fn)
        PARSE(name, stop_on_closingbr);

        cm = fn->length + name->length + 8*2;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_PIECE, fn);
        SERIALIZE(COMMAND_PIECE, name);
        return dst;
    }
    return NULL;
}

static size_t xplain_piece( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_PIECE ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_PIECE) ) {
            XPLAIN("CMD_PIECE, FILE=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_PIECE) ) {
                XPLAIN("CMD_PIECE, PIECE=");
                return ret;
            }
        }
    }
    return 0;
}

static vecstring * exec_piece ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_PIECE ) {
        EXEC_START(COMMAND_PIECE);
        EXEC_GET_ARG(COMMAND_PIECE, file);
        EXEC_GET_ARG(COMMAND_PIECE, piece);

        char * fn = vtopchar(file);
        char * pn = vtopchar(piece);

        pcp_log(pcp, PCP_LOG_DEBUG, COMMAND_PIECE, "piece file name to be included [%s]\n", fn);
        if (!fn[0]) {
            pcp_log(pcp, PCP_LOG_DEBUG, COMMAND_PIECE, "piece file name is empty, we consider it as local file, let's find piece file name");
            if ( pcp->working_on ) {
                free(fn);
                fn = rtopchar( pcp->working_on->ns );
            }
        } else {
            pcp_load(pcp, fn);
        }
        pcp_log(pcp, PCP_LOG_DEBUG, COMMAND_PIECE, "piece name to be included [%s]\n", pn);

        pcp_piece * p = pcp_find_piece ( pcp, fn, pn );
        if (!p) {
            pcp_log(pcp, PCP_LOG_ERROR, COMMAND_PIECE, "piece not found: %s:%s\n", fn, pn);
            return NULL;
        } else if (!p->parsed) {
            pcp_log(pcp, PCP_LOG_ERROR, COMMAND_PIECE, "piece not parsed: %s:%s\n", fn, pn);
            return NULL;
        } else {
            pcp->working_on = p;
            vecstring * res = pcp_do_exec(p->parsed, pcp);
            if (!res)
                return NULL;
            vmove(dst, res);
        }
        free(fn);
        free(pn);
        delete_vecstring(file);
        delete_vecstring(piece);
        return dst;
    }
    return NULL;
}
