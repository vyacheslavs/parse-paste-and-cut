#include "pcp/pcp.h"
#include "pcp/macros.h"

static vecstring * parse_log ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_log( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_log ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_log = {
	.cb = parse_log,
	.cb_explain = xplain_log,
	.cb_exec = exec_log,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_LOG
};

static vecstring * parse_log ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "pcplog ";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_LOG);
        GET_REST(log_text);

        cm = log_text->length + 8;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_LOG, log_text);
        return dst;
    }
    return NULL;
}

static size_t xplain_log( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_LOG ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_LOG) ) {
            XPLAIN("CMD_LOG, DATA=");
            return ret;
        }
    }
    return 0;
}

static vecstring * exec_log ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_LOG ) {
        EXEC_START(COMMAND_LOG);
        EXEC_GET_ARG(COMMAND_LOG, log_text);
        char * logv = vtopchar(log_text);
        pcp_log(pcp, PCP_LOG_INFO, COMMAND_LOG, "%s\n", logv);
        free(logv);
        delete_vecstring(log_text);
        return dst;
    }
}
