#include "pcp/pcp.h"
#include <malloc.h>
#include "pcp/macros.h"

/* фильтр webf

1. Защита от mysql injection работает только если данные записываются через "
2. Убирается \r\n
3. Фильтруется < и >

*/

static vecstring * parse_webf ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_webf( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_webf ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_webf = {
	.cb = parse_webf,
	.cb_explain = xplain_webf,
	.cb_exec = exec_webf,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_WEBF
};

static vecstring * parse_webf ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "wf ") ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_WEBF;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		vecstring * val = subvstr(src,3, SIZE_MAX);
		vecstring * outval = pcp_do_parse( val, pcp, NULL, NULL );
		delete_vecstring(val);
		
		cm = outval->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, outval);
		return dst;
	}
	return NULL;
}

static size_t xplain_webf( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_WEBF ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_WEBF, FILTER DATA=\n");
		vecstring * cmdata = subvstr( parsed, 8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		return sz+8;
	}
	return 0;
}

struct filter_ctx {
    int cp_start;
    int cp_len;
    vecstring * dst;
};

static void copy_aggregated(vecstring * src, struct filter_ctx * ctx) {
    if ( ctx->cp_len>0) {
        vmove(ctx->dst, subvstr(src, ctx->cp_start, ctx->cp_len));
        ctx->cp_start = -1;
        ctx->cp_len = 0;
    }
}

static int filter_cb( vecstring * src, int position, char c, void * priv ) {
    struct filter_ctx * ctx = (struct filter_ctx *) priv;
    if ( c == '\r' || c=='\n' ) {
        // необходимо скопировать в dst все, что мы накопили
        copy_aggregated( src, ctx);
        return 1; // пропускаем, если \r или \n
    }

    if ( c == '"' ) {
        copy_aggregated( src, ctx );
        vmove(ctx->dst, vecstring_from_pchar("\\\""));
        return 1;
    }

    if ( c == '<' ) {
        copy_aggregated( src, ctx );
        vmove(ctx->dst, vecstring_from_pchar("&lt;"));
        return 1;
    }
    if ( c == '>' ) {
        copy_aggregated( src, ctx );
        vmove(ctx->dst, vecstring_from_pchar("&gt;"));
        return 1;
    }

    if ( !ctx->cp_len )
        ctx->cp_start = position;
    ctx->cp_len++;
    return 1;
}

static vecstring * exec_webf ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_WEBF ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		vecstring * cmdata = subvstr( src, 8, sz );
		vecstring * val = pcp_do_exec( cmdata, pcp );
		delete_vecstring(cmdata);
        struct filter_ctx ctx = {
            .cp_start = -1,
            .cp_len = 0,
            .dst = dst
        };
        vforeach(val, filter_cb, &ctx);
        copy_aggregated( val, &ctx );
		delete_vecstring(val);
		return dst;
	}
	return NULL;
}
