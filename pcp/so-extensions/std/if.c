#include "pcp/pcp.h"
#include <malloc.h>
#include <libgen.h>
#include <string.h>

/*
   piece extension:

   usage:
   $(if[<arg1>=<arg2>]do1?do2)$
*/

static vecstring * parse_if ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_if( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_if ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_if = {
	.cb = parse_if,
	.cb_explain = xplain_if,
	.cb_exec = exec_if,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_IF
};

static vecstring * parse_if ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "if[") ) {
		*parsed = 1; // говорим, что мы беремся за парсинг сразу
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_IF;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		size_t stopped_offset = -1;
		size_t stopped_offset2 = -1;
		size_t stopped_offset3 = -1;
		size_t stopped_offset4 = -1;
		// парсим аргумент 1
		vecstring * argdata = subvstr(src, 3, SIZE_MAX);
		vecstring * arg1 = pcp_do_parse( argdata, pcp, stop_on_eq, &stopped_offset );
		if (!arg1) return NULL;
		delete_vecstring(argdata);
		
		if (stopped_offset==-1) {
			pcp_error(pcp, "syntax error while parsing <if>: arg1\n" );
			delete_vecstring(arg1);
			delete_vecstring(dst);
			return NULL;
		}
		// парсим аргумент 2
		argdata = subvstr( src, 4+stopped_offset, SIZE_MAX );
		vecstring * arg2 = pcp_do_parse( argdata, pcp, stop_on_closingbr, &stopped_offset2 );
		if (!arg2) {
			pcp_error(pcp, "can not parse arg2\n");
			return NULL;
		}
		delete_vecstring(argdata);
		
		if (!stopped_offset2==-1) {
			pcp_error(pcp, "syntax error while parsing <if>: arg2\n" );
			delete_vecstring(arg1);
			delete_vecstring(arg2);
			delete_vecstring(dst);
			return NULL;
		}
		// теперь у нас есть 2 аргумента, надо взять do1 и do2
		// парсим do1
		argdata = subvstr( src, 5+stopped_offset+stopped_offset2, SIZE_MAX );
		vecstring * do1 = pcp_do_parse( argdata, pcp, stop_on_question, &stopped_offset3 );
		if (!do1) return NULL;
		delete_vecstring(argdata);
		
		if (!stopped_offset3==-1) {
			pcp_error(pcp, "syntax error while parsing <if>: do1\n" );
			delete_vecstring(arg1);
			delete_vecstring(arg2);
			delete_vecstring(do1);
			delete_vecstring(dst);
			return NULL;
		}
		// парсим do2
		argdata = subvstr( src, 6+stopped_offset+stopped_offset2+stopped_offset3, SIZE_MAX );
		vecstring * do2 = pcp_do_parse(argdata, pcp, NULL, NULL);
		if (!do2) return NULL;
		delete_vecstring(argdata);
		
		// собираем бинарную комманду
		// длина всей комманды
		uint32_t command_len = arg1->length + arg2->length + 2*8 // длина двух аргументов
			+ do1->length + do2->length + 2*8; // длина двух действий
		vwrite(dst, &command_len, sizeof(uint32_t));
		
		cm = COMMAND_IF_ARG1;
		vwrite(dst, &cm, sizeof(uint32_t));
		command_len = arg1->length;
		vwrite(dst, &command_len, sizeof(uint32_t));
		vmove(dst, arg1);
		
		cm = COMMAND_IF_ARG2;
		vwrite(dst, &cm, sizeof(uint32_t));
		command_len = arg2->length;
		vwrite(dst, &command_len, sizeof(uint32_t));
		vmove(dst, arg2);
		
		cm = COMMAND_IF_DO1;
		vwrite(dst, &cm, sizeof(uint32_t));
		command_len = do1->length;
		vwrite(dst, &command_len, sizeof(uint32_t));
		vmove(dst, do1);
		
		cm = COMMAND_IF_DO2;
		vwrite(dst, &cm, sizeof(uint32_t));
		command_len = do2->length;
		vwrite(dst, &command_len, sizeof(uint32_t));
		vmove(dst, do2);
		return dst;
	}
	return NULL;
}

static size_t xplain_if( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_IF ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_IF, ARG1=\n");
		
		size_t ret = 8;
		pcp_read_command( &cm, &sz, parsed, ret);
		vecstring * cmdata = subvstr( parsed, ret+8, sz );
		ret += 8+sz;
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_IF, ARG2=\n");
		pcp_read_command( &cm, &sz, parsed, ret);
		cmdata = subvstr( parsed, ret+8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		ret += 8 + sz;
		
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_IF, DO1=\n");
		pcp_read_command( &cm, &sz, parsed, ret);
		cmdata = subvstr( parsed, ret+8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		ret += 8 + sz;
		
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_IF, DO2=\n");
		pcp_read_command( &cm, &sz, parsed, ret);
		
		cmdata = subvstr( parsed, ret+8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		ret += 8 + sz;
		
		return ret;
	}
	return 0;
}

static vecstring * exec_if ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	vecstring * res = NULL;
	uint32_t cm,sz;
	size_t oft = 0; // оффсет от начала src
	if ( pcp_read_command( &cm, &sz, src, oft) && cm == COMMAND_IF ) {
		*parsed = 1;
		oft+= 8;
		if ( pcp_read_command( &cm, &sz, src, oft) && cm == COMMAND_IF_ARG1 ) {
			oft+=8;
			vecstring * cmdata = subvstr( src, oft, sz );
			oft+=sz;
			vecstring * arg1 = pcp_do_exec ( cmdata, pcp );
			if (!arg1) return NULL;
			delete_vecstring(cmdata);
			if ( pcp_read_command( &cm, &sz, src, oft) && cm == COMMAND_IF_ARG2 ) {
				oft+=8;
				cmdata = subvstr( src, oft, sz );
				oft+=sz;
				vecstring * arg2 = pcp_do_exec ( cmdata, pcp );
				if (!arg2) return NULL;
				delete_vecstring(cmdata);
				
				// сравниваем arg1 и arg2
				int is_equal = vstr_compare(arg1,arg2);
				if (is_equal) {
					// парсим do1
					if ( pcp_read_command( &cm, &sz, src, oft) && cm == COMMAND_IF_DO1 ) {
						oft+=8;
						cmdata = subvstr( src, oft, sz );
						oft+=sz;
						vecstring * do1 = pcp_do_exec ( cmdata, pcp );
						if (!do1) return NULL;
						delete_vecstring(cmdata);
						
						res = do1;
					}
				} else {
					// пропускаем do1
					if ( pcp_read_command( &cm, &sz, src, oft) && cm == COMMAND_IF_DO1 ) {
						oft+=8 + sz;
					}
					// парсим do2
					if ( pcp_read_command( &cm, &sz, src, oft) && cm == COMMAND_IF_DO2 ) {
						oft+=8;
						
						cmdata = subvstr( src, oft, sz );
						oft+=sz;
						vecstring * do2 = pcp_do_exec ( cmdata, pcp );
						if (!do2) return NULL;
						delete_vecstring(cmdata);
						
						res = do2;
					}
				}
				delete_vecstring(arg2);
			}
			delete_vecstring(arg1);
		}
	}
	return res;
}
