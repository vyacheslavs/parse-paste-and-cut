#include "pcp/pcp.h"
#include "pcp/macros.h"

static vecstring * parse_set ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_set( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_set ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_set = {
	.cb = parse_set,
	.cb_explain = xplain_set,
	.cb_exec = exec_set,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_SET
};

static vecstring * parse_set ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "set[";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_SET);
        PARSE(arg1, stop_on_eq);
        PARSE(arg2, stop_on_closingbr);

        cm = arg1->length + arg2->length + 8*2;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_SET, arg1);
        SERIALIZE(COMMAND_SET, arg2);
        return dst;
    }
    return NULL;
}

static size_t xplain_set( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_SET ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_SET) ) {
            XPLAIN("CMD_SET, VARIABLE=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_SET) ) {
                XPLAIN("CMD_SET, DATA=");
                return ret;
            }
        }
    }
    return 0;
}

static vecstring * exec_set ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_SET ) {
        EXEC_START(COMMAND_SET);
        EXEC_GET_ARG(COMMAND_SET, arg1);
        EXEC_GET_ARG(COMMAND_SET, arg2);

        get_servar_hook() ( arg1, arg2 );

        delete_vecstring(arg1);
        delete_vecstring(arg2);
        return dst;
    }
    return NULL;
}
