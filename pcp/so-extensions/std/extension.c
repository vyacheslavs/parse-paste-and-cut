#include "pcp/pcp.h"

extern pcp_handler handler_if;
extern pcp_handler handler_isset;
extern pcp_handler handler_loop;
extern pcp_handler handler_piece;
extern pcp_handler handler_set;
extern pcp_handler handler_webf;
extern pcp_handler handler_html;
extern pcp_handler handler_log;

pcp_handler * register_pcp_handlers[] = {
	&handler_if,
	&handler_isset,
	&handler_loop,
	&handler_piece,
	&handler_set,
    &handler_webf,
    &handler_html,
    &handler_log,
	NULL
};
