#include "pcp/pcp.h"
#include <malloc.h>
/*
 * $(loop[<initialValue>,<endValue>,<indexName>] ... данные )$ - луп от начального значения до конечного с шагом 1
 */

static vecstring * parse_loop ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_loop( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_loop ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_loop = {
	.cb = parse_loop,
	.cb_explain = xplain_loop,
	.cb_exec = exec_loop,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_LOOP
};

static vecstring * parse_loop ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "loop[") ) {
		*parsed = 1; // говорим, что мы беремся за парсинг сразу
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_LOOP;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		// берем начальное значение
		size_t stopped_offset=-1;
		vecstring * argdata = subvstr(src, 5, SIZE_MAX);
		vecstring * inival = pcp_do_parse( argdata, pcp, stop_on_comma, &stopped_offset );
		delete_vecstring(argdata);
		
		if ( stopped_offset == -1 ) {
			pcp_error(pcp, "syntax error while parsing <loop>\n" );
			delete_vecstring(inival);
			delete_vecstring(dst);
			return NULL;
		}
		size_t stopped_offset2=-1;
		argdata = subvstr(src, 5+stopped_offset+1, SIZE_MAX);
		vecstring * endval = pcp_do_parse( argdata, pcp, stop_on_comma, &stopped_offset2 );
		delete_vecstring(argdata);
		
		if ( stopped_offset2 == -1 ) {
			pcp_error(pcp, "syntax error while parsing <loop>\n" );
			delete_vecstring(inival);
			delete_vecstring(endval);
			delete_vecstring(dst);
			return NULL;
		}
		
		size_t stopped_offset3 = -1;
		argdata = subvstr(src, 5+stopped_offset+1+stopped_offset2+1, SIZE_MAX);
		vecstring * index = pcp_do_parse( argdata, pcp, stop_on_closingbr, &stopped_offset3 );
		delete_vecstring(argdata);
		
		if ( stopped_offset3 == -1 ) {
			pcp_error(pcp, "syntax error while parsing <loop>\n" );
			delete_vecstring(inival);
			delete_vecstring(endval);
			delete_vecstring(index);
			delete_vecstring(dst);
			return NULL;
		}
		// все данные до конца - это данные, которые надо "вращать"
		argdata = subvstr(src, 5+stopped_offset+1+stopped_offset2+1+stopped_offset3+1, SIZE_MAX);
		vecstring * val = pcp_do_parse( argdata, pcp, NULL, NULL );
		delete_vecstring(argdata);
		
		cm = 4*8+inival->length + endval->length + val->length + index->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = COMMAND_LOOP_INIT;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = inival->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, inival);
		cm = COMMAND_LOOP_FINI;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = endval->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, endval);
		cm = COMMAND_LOOP_INDEX;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = index->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, index);
		cm = COMMAND_LOOP_DATA;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = val->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,val);
		return dst;
	}
	return NULL;
}

static size_t xplain_loop( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_LOOP ) {
		size_t ret = 8;
		if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_LOOP_INIT ) {
			pcp_intend(lvl, stream);
			fprintf(stream, "CMD_LOOP, INIT=\n");
			vecstring * cmdata = subvstr( parsed, ret+8, sz );
			pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
			ret+=8+sz;
			delete_vecstring(cmdata);
			if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_LOOP_FINI ) {
				pcp_intend(lvl, stream);
				fprintf(stream, "CMD_LOOP, FINI=\n");
				cmdata = subvstr( parsed, ret+8, sz );
				pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
				ret+=8+sz;
				delete_vecstring(cmdata);
				if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_LOOP_INDEX ) {
					pcp_intend(lvl, stream);
					fprintf(stream, "CMD_LOOP, INDEX=\n");
					cmdata = subvstr( parsed, ret+8, sz );
					pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
					ret+=8+sz;
					delete_vecstring(cmdata);
					if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_LOOP_DATA ) {
						pcp_intend(lvl, stream);
						fprintf(stream, "CMD_LOOP, DATA=\n");
						cmdata = subvstr( parsed, ret+8, sz );
						pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
						ret+=8+sz;
						delete_vecstring(cmdata);
						return ret;
					}
				}
			}
		}
	}
	return 0;
}

static vecstring * exec_loop ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_LOOP ) {
		vecstring * dst = new_vecstring(0);
		*parsed = 1;
		size_t ret = 8;
		if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_LOOP_INIT ) {
			ret+=8;
			vecstring * cmdata = subvstr( src, ret, sz );
			vecstring * init = pcp_do_exec ( cmdata, pcp );
			delete_vecstring(cmdata);
			ret+=sz;
			if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_LOOP_FINI ) {
				ret+=8;
				cmdata = subvstr( src, ret, sz );
				vecstring * fini = pcp_do_exec ( cmdata, pcp );
				delete_vecstring(cmdata);
				ret+=sz;
				if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_LOOP_INDEX ) {
					ret+=8;
					cmdata = subvstr( src, ret, sz );
					vecstring * vindex = pcp_do_exec ( cmdata, pcp );
					delete_vecstring(cmdata);
					ret+=sz;
					if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_LOOP_DATA ) {
						ret+=8;
						cmdata = subvstr( src, ret, sz );
						ret+=sz;
						
						char * init_str = vtopchar(init);
						char * fini_str = vtopchar(fini);
						
						int init_int = atoi(init_str);
						int fini_int = atoi(fini_str);
						char index_str[0xff];
						
						if ( init_int < fini_int ) { //normal loop
							int idx=init_int;
							for (;idx<fini_int;++idx) {
								snprintf(index_str,0xff-1, "%d", idx);
								vecstring * vidx = vecstring_from_pchar(index_str);
								set_exec_hook f = get_servar_hook();
								f(vindex, vidx);
								vecstring * val = pcp_do_exec ( cmdata, pcp );
								delete_vecstring(vidx);
								vmove(dst, val);
							}
						} else if ( init_int > fini_int ) {
							int idx = fini_int;
							for (;idx>init_int;--idx) {
								vecstring * val = pcp_do_exec ( cmdata, pcp );
								vmove(dst, val);
							}
						}
						free(init_str);
						free(fini_str);
						delete_vecstring(cmdata);
					}
					delete_vecstring(vindex);
				}
				delete_vecstring(fini);
			}
			delete_vecstring(init);
		}
		return dst;
	}
	return NULL;
}
