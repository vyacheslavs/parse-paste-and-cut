#include "pcp/pcp.h"

//$(isset[varname])$ - проверяет есть ли в хранилище переменная varname, возвращает true если переменная существует, false - иначе

static vecstring * parse_isset ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_isset( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_isset ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_isset = {
	.cb = parse_isset,
	.cb_explain = xplain_isset,
	.cb_exec = exec_isset,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_ISSET
};

static vecstring * parse_isset ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "isset[") ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_ISSET;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		size_t stopped_offset=-1;
		vecstring * argdata = subvstr(src, 6, SIZE_MAX);
		vecstring * arg1 = pcp_do_parse( argdata, pcp, stop_on_closingbr, &stopped_offset );
		if (!arg1) return NULL;
		delete_vecstring(argdata);
		
		if (stopped_offset==-1) {
			pcp_error(pcp, "syntax error while parsing <isset>" );
			delete_vecstring(arg1);
			delete_vecstring(dst);
			return NULL;
		}
		
		// мы нашли аргумент
		cm = arg1->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, arg1);
		return dst;
	}
	return NULL;
}

static size_t xplain_isset( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_ISSET ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_ISSET, VAR=\n");
		vecstring * cmdata = subvstr( parsed, 8, sz );
		size_t ret = 8 + sz;
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		return ret;
	}
	return 0;
}

static vecstring * exec_isset ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	vecstring * res = NULL;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_ISSET ) {
		*parsed = 1;
		vecstring * cmdata = subvstr( src, 8, sz );
		vecstring * arg1 = pcp_do_exec ( cmdata, pcp );
		if (!arg1) return NULL;
		delete_vecstring(cmdata);
		res = new_vecstring(0);
		
		// проверяем, существует ли arg1?
		if (find_vecval(arg1))
			vwrite(res, "true", 4);
		 else
			vwrite(res, "false", 5);
		
		delete_vecstring(arg1);
	}
	return res;
}
