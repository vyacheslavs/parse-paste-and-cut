Standart extensions for PCP
---------------------------

# Syntax

    $(piece[<filename>,<piece name>])$
    $(isset[<variable name>])$
    $(set[<variable name>=<value>])$
    $(pcplog <..data..>)$

# Purpose

## piece

$(piece)$ extension includes data from <piece name> to current piece template. For example, you have 2 files:

sample.pct:

    <!--{main}-- Hello, $(piece[sample2.pcp,piece])$ --{main}-->

sample2.pct:

    <!--{piece}--World--{piece}-->

Finally, `main` piece will get data from sample2 file and put it instead of $(piece ..)$ extension.

$(isset)$ checks if <variable name> exists and returns `true` or `false`.

$(set)$ sets the <variable name> the value of <value>.

## pcplog

$(pcplog)$ logs all data to defualt log device. When using pcpcli, you can all loged data in error_log.


