Curl extension for PCP Project.
-------------------------------

# Usage:

    $(curl[<url>] ...<data>)$

# Purpose:

    Makes http (POST or GET) query to <url> and fetches data. If no <data> specified then curl makes GET request,
    else - POST.
