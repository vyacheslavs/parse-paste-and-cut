#include "pcp/pcp.h"
#include <malloc.h>
#include "pcp/macros.h"
#include <curl/curl.h>

/*
    Синтаксис:
    $(curl[<url>]...<data>)$
    GET / POST

    Если нет <data>, то данные берутся через GET
    Если есть <data>, то данные берутся через POST
*/

static vecstring * parse_curl ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_curl( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_curl ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_curl = {
	.cb = parse_curl,
	.cb_explain = xplain_curl,
	.cb_exec = exec_curl,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_CURL
};

static vecstring * parse_curl ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "curl[";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_CURL);
        PARSE(url, stop_on_closingbr);

        vecstring * argdata = subvstr(src, parsing_offset, SIZE_MAX);
        vecstring * post_data = pcp_do_parse(argdata, pcp, NULL, NULL);
        delete_vecstring(argdata);

        cm = url->length + post_data->length + 8*2;
        vwrite(dst, &cm, sizeof(uint32_t));

        cm = COMMAND_CURL;
        vwrite(dst, &cm, sizeof(uint32_t));
        cm = url->length;
        vwrite(dst, &cm, sizeof(uint32_t));
        vmove(dst, url);

        cm = COMMAND_CURL;
        vwrite(dst, &cm, sizeof(uint32_t));
        cm = post_data->length;
        vwrite(dst, &cm, sizeof(uint32_t));
        vmove(dst, post_data);
        return dst;
    }
}

static size_t xplain_curl( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_CURL ) {
		XPLAIN_START
		if ( EXPECT_NEXT_COMMAND(COMMAND_CURL) ) {
			XPLAIN("CMD_CURL, URL=");
			if ( EXPECT_NEXT_COMMAND(COMMAND_CURL) ) {
				XPLAIN("CMD_CURL, DATA=");
				return ret;
			}
		}
	}
}

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
    if ( stream ) {
        vecstring * dst = (vecstring *) stream;
        vmove(dst, vecstring_from_bchar ( ptr, size * nmemb ) );
    }
    return size * nmemb;
}

static void make_request ( vecstring * dst, const char * url, uint8_t * data, size_t data_len, pcp_t * pcp ) {
    CURL * curl;
    curl = curl_easy_init();
    if (curl) {
        pcp_log(pcp, PCP_LOG_INFO, COMMAND_CURL, "perform curl request to %s\n", url);
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
        curl_easy_setopt(curl, CURLOPT_WRITEHEADER, 0);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, dst);

        if ( data_len > 0 ) {
            curl_easy_setopt(curl, CURLOPT_POST, 1);
            curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data);
        }

        CURLcode res = curl_easy_perform(curl);
        if(res != CURLE_OK)
        pcp_error(pcp, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));
        curl_easy_cleanup(curl);
    }
}

static vecstring * exec_curl ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_CURL ) {
        EXEC_START(COMMAND_CURL);
        EXEC_GET_ARG(COMMAND_CURL, url);
        EXEC_GET_ARG(COMMAND_CURL, data);

        char * urlp = vtopchar(url);
        char * datap = vtopchar(data);
        make_request ( dst, urlp, (uint8_t *)datap, data->length, pcp );
        free(urlp);
        free(datap);

        delete_vecstring(url);
        delete_vecstring(data);
        return dst;
    }
}
