#include "pcp/pcp.h"
#include <malloc.h>
#include "pcp/macros.h"
#include <libxml/parser.h>
#include <libxslt/xslt.h>
#include <libxslt/xsltInternals.h>
#include <libxslt/transform.h>
/*
	$(xslt[data,template])$
	data + template -> xslt -> profit!
*/

static vecstring * parse_xslt ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_xslt( pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_xslt ( pcp_t * pcp, vecstring * src, int * parsed );
static void fini_xslt ( void * r );

pcp_handler handler_xslt = {
	.cb = parse_xslt,
	.cb_explain = xplain_xslt,
	.cb_exec = exec_xslt,
	.cb_init = NULL,
	.cb_fini = fini_xslt,
	.probe_command = COMMAND_XSLT
};

static vecstring * parse_xslt ( vecstring * src, pcp_t * pcp, int * parsed ) {
	const char key[] = "xslt[";
	if ( vstrcmp(src, key) ) {
		PARSE_START(COMMAND_XSLT);
		// берем data
		PARSE(data,stop_on_comma);
		// берем template
		PARSE(template,stop_on_closingbr);
		cm = data->length + template->length + 2*8;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = COMMAND_XSLT;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = data->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, data);
		cm = COMMAND_XSLT;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = template->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, template);
		return dst;
	}
	return NULL;
}

static size_t xplain_xslt( pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_XSLT ) {
		XPLAIN_START
		if ( EXPECT_NEXT_COMMAND(COMMAND_XSLT) ) {
			XPLAIN("CMD_XSLT, DATA=");
			if ( EXPECT_NEXT_COMMAND(COMMAND_XSLT) ) {
				XPLAIN("CMD_MATH, TEMPLATE=");
				return ret;
			}
		}
	}
}

static vecstring * exec_xslt ( pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_XSLT ) {
		EXEC_START(COMMAND_XSLT);
		EXEC_GET_ARG(COMMAND_XSLT, data);
		EXEC_GET_ARG(COMMAND_XSLT, template);
		
		char * p_template = vtopchar(template);
		char * p_data = vtopchar(data);
		// сначала загружаем шаблон
		xmlDocPtr template_doc = xmlParseDoc((const xmlChar *)p_template);
		if (template_doc) {
			xsltStylesheetPtr xslt_template = xsltParseStylesheetDoc(template_doc);
			if (xslt_template) {
				xmlDocPtr data_doc = xmlParseDoc((const xmlChar *)p_data);
				if (data_doc) {
					xmlDocPtr res = xsltApplyStylesheet( xslt_template, data_doc, NULL );
					if (res) {
						char * result;
						int result_len;
						xsltSaveResultToString((xmlChar **)&result, &result_len, res, xslt_template);
						vmove(dst, vecstring_from_bchar(result, result_len));
						free(result);
						xmlFreeDoc(res);
					}
					xmlFreeDoc(data_doc);
				} else {
					// fprintf(stderr, "failed to parse xslt data:\n%s\n", p_data);
				}
				xsltFreeStylesheet(xslt_template);
			} else {
				fprintf(stderr, "failed to create stylesheet from xml provided\n");
			}
			// xmlFreeDoc(template_doc);
		} else {
			// fprintf(stderr, "failed to parse xslt template:\n%s\n", p_template);
		}
		
		free(p_data);
		free(p_template);
		delete_vecstring(data);
		delete_vecstring(template);
		return dst;
	}
}

static void fini_xslt ( void * r ) {
	xsltCleanupGlobals();
	xmlCleanupParser();
}
