#include "json-storage.h"
#include <mempool/mempool.h>
#include <string.h>

static DEFINE_HASHTABLE(ht, 8);
static mempool mp;

void json_storage_init(void * reserve) {
    hash_init(ht);
    mp_init( &mp, sizeof(struct json_item_t) );
}

void json_storage_fini(void * reserve) {
    mp_fini( &mp );
}

void allocate_json_item ( vecstring * key, json_value * value )
{
    struct json_item_t * it = get_json_item( key );
    if ( it ) {
        it->value = value;
        return;
    }
    uint8_t keyval = (uint8_t) vecstring_keyval(key);
    struct json_item_t * item = (struct json_item_t *) mp_alloc ( &mp );
    item->key = vshare(key);
    item->value = value;

    hash_add( ht, &item->hash, keyval );
}

void remove_json_item ( vecstring * key ) {
    struct json_item_t * it = get_json_item( key );
    if ( it ) {
        hash_del( &it->hash );
        mp_free(&mp, it);
        return;
    }
}

struct json_item_t * get_json_item ( vecstring * key ) {
    uint8_t keyval = (uint8_t) vecstring_keyval(key);
    struct json_item_t * it = NULL;
    hash_for_each_possible( ht, it, hash, keyval )
    {
        if ( vstr_compare( it->key, key ) ) {
            return it;
        }
    }
    return NULL;
}

json_value * get_json_value ( vecstring * key )
{
    struct json_item_t * it = get_json_item ( key );
    if (!it)
        return NULL;
    return it->value;
}

static const char * json_find_delim ( const char * from ) {
    while ( *from ) {
        if ( *from == '[' || *from == '.' ) {
            return from;
        }
        ++from;
    }
    return from;
}

static char * sdup( const char * s, size_t size ) {
    char * r = (char *) malloc(size+1);
    memcpy(r, s, size);
    r[size] = 0;
    return r;
}

json_value * json_path ( const char * path, struct __pcp_t * pcp )
{

    // в начале идет имя переменной
    const char * name_ends = json_find_delim ( path );
    if (!name_ends || name_ends == path)
    {
        pcp_error(pcp, "invalid json variable name lookup\n");
        return NULL;
    }
    // мы узнали название переменной, давайте узнаем есть ли эта переменная в хранилище
    vecstring * json_value_name = vecstring_from_bchar( path, name_ends - path );
    json_value * json_val = get_json_value(json_value_name);
    if ( !json_val )
    {
        delete_vecstring(json_value_name);
        pcp_error(pcp, "unknown json variable\n");
        return NULL;
    }
    delete_vecstring(json_value_name);

    const char * b = name_ends;
    const char * e = NULL;

    json_value * result = json_val;
    do {
        if ( *b == 0 )
            return result;

        if ( *b == '[' ) {
            // если мы нашли начало квадратных скобок, то значит мы находимся внутри массива
            // поэтому сначала проверяем тип
            if ( result->type != json_array ) {
                pcp_error(pcp, "type mismatch (1)\n");
                return NULL;
            }
            e = strchr(b, ']');
            if ( !e ) {
                pcp_error(pcp, "malformed json (0)\n");
                return NULL;
            }
            b++;
            char * index_s = sdup(b, e-b);
            unsigned int index = atoi(index_s);
            free(index_s);
            // теперь проверяем на границу индекса
            if ( index >= result->u.array.length ) {
                pcp_error(pcp, "index range overflow (2)\n");
                return NULL;
            }
            // теперь, когда все ок
            result = result->u.array.values[index];
            b = json_find_delim(e+1);
        } else if ( *b == '.' ) {
            // если мы нашли точку, значит мы находимся внутри объекта
            // проверим тип
            if ( result->type != json_object ) {
                pcp_error(pcp, "type mismatch (3)\n");
                return NULL;
            }
            b++;
            e = json_find_delim(b);
            if ( e == b ) {
                pcp_error(pcp, "malformed json (4)\n");
                return NULL;
            }
            // мы должны узнать как называется объект
            char * index_s = sdup(b, e-b);
            unsigned int idx;
            unsigned int found = 0;
            for ( idx = 0; idx < result->u.object.length; ++idx ) {
                if ( strcmp( result->u.object.values[idx].name, index_s ) == 0 ) {
                    result = result->u.object.values[idx].value;
                    found = 1;
                    break;
                }
            }
            free(index_s);
            if (!found) {
                pcp_error(pcp, "object not found (5)\n");
                return NULL;
            }
            b = e;
        }
    } while ( *b != 0 );

    // если *b == 0, то отдаем что есть
    // теперь парсим путь
    return result;
}
