#include "pcp/pcp.h"
#include <malloc.h>
#include "pcp/macros.h"
#include "json.h"
#include "json-storage.h"

/**
    Парсит json.
    Синтаксис:

    $(json-parse[<variable>] ...json text )$
*/

static vecstring * parse_json_parser ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_json_parser( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_json_parser ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_json_parser = {
	.cb = parse_json_parser,
	.cb_explain = xplain_json_parser,
	.cb_exec = exec_json_parser,
	.cb_init = json_storage_init,
	.cb_fini = json_storage_fini,
	.probe_command = COMMAND_JSON_PARSER
};

static vecstring * parse_json_parser ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "json-parser[";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_JSON_PARSER);
        PARSE(var, stop_on_closingbr);

        vecstring * argdata = subvstr(src, parsing_offset, SIZE_MAX);
        vecstring * jsontext = pcp_do_parse(argdata, pcp, NULL, NULL);
        delete_vecstring(argdata);

        cm = var->length + jsontext->length + 8*2;
        vwrite(dst, &cm, sizeof(uint32_t));

        cm = COMMAND_JSON_PARSER;
        vwrite(dst, &cm, sizeof(uint32_t));
        cm = var->length;
        vwrite(dst, &cm, sizeof(uint32_t));
        vmove(dst, var);

        cm = COMMAND_JSON_PARSER;
        vwrite(dst, &cm, sizeof(uint32_t));
        cm = jsontext->length;
        vwrite(dst, &cm, sizeof(uint32_t));
        vmove(dst, jsontext);
        return dst;
    }
}

static size_t xplain_json_parser( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_JSON_PARSER ) {
		XPLAIN_START
		if ( EXPECT_NEXT_COMMAND(COMMAND_JSON_PARSER) ) {
			XPLAIN("CMD_JSON_PARSER, VARIABLE=");
			if ( EXPECT_NEXT_COMMAND(COMMAND_JSON_PARSER) ) {
				XPLAIN("CMD_JSON_PARSER, JSON=");
				return ret;
			}
		}
	}
}

static vecstring * exec_json_parser ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_JSON_PARSER ) {
        EXEC_START(COMMAND_JSON_PARSER);
        EXEC_GET_ARG(COMMAND_JSON_PARSER, var);
        EXEC_GET_ARG(COMMAND_JSON_PARSER, jsontext);
        // начинаем парсить json text
        char * json_t = vtopchar(jsontext);
        char json_error[0xfff];

        json_settings settings = { 0 };
        json_value * jsonval = json_parse_ex ( &settings, json_t, jsontext->length, json_error);
        if (!jsonval)
            pcp_error(pcp, "failed to parse json: %s\n", json_error);
        else {
            allocate_json_item( var, jsonval );
        }

        free(json_t);
        delete_vecstring(var);
        delete_vecstring(jsontext);
        return dst;
    }
}
