#include "pcp/pcp.h"

extern pcp_handler handler_jsonprep;
extern pcp_handler handler_json_parser;
extern pcp_handler handler_json_lookup;
extern pcp_handler handler_json_foreach;

pcp_handler * register_pcp_handlers[] = {
    &handler_jsonprep,
    &handler_json_parser,
    &handler_json_lookup,
    &handler_json_foreach,
    NULL
};
