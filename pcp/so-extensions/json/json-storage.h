#ifndef __JSON_STORAGE
#define __JSON_STORAGE

#include <htlib.h>
#include "json.h"
#include <vecstring/vecstring.h>
#include "pcp/pcp.h"

struct json_item_t {
    struct hlist_node hash;
    vecstring * key;
    json_value * value;
};

void json_storage_init(void * reserve);
void json_storage_fini(void * reserve);

void allocate_json_item ( vecstring * key, json_value * value );
void remove_json_item ( vecstring * key );
struct json_item_t * get_json_item ( vecstring * key );
json_value * get_json_value ( vecstring * key );

json_value * json_path ( const char * path, struct __pcp_t * pcp );

#endif
