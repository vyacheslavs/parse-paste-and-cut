#include "pcp/pcp.h"
#include <malloc.h>
#include "pcp/macros.h"
#include "json-storage.h"
#include <string.h>

static vecstring * parse_json_foreach ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_json_foreach( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_json_foreach ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_json_foreach = {
	.cb = parse_json_foreach,
	.cb_explain = xplain_json_foreach,
	.cb_exec = exec_json_foreach,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_JSON_FOREACH
};

static vecstring * parse_json_foreach ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "json-foreach[";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_JSON_FOREACH);
        PARSE(key, stop_on_comma);
        PARSE(value, stop_on_closingbr);
        PARSE(trash, stop_on_openbr);
        PARSE(json_var, stop_on_closingbr);

        GET_REST(text);

        cm = key->length + value->length + json_var->length + text->length+8*4;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_JSON_FOREACH, key);
        SERIALIZE(COMMAND_JSON_FOREACH, value);
        SERIALIZE(COMMAND_JSON_FOREACH, json_var);
        SERIALIZE(COMMAND_JSON_FOREACH, text);

        delete_vecstring(trash);
        return dst;
    }
}

static size_t xplain_json_foreach( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_JSON_FOREACH ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_JSON_FOREACH) ) {
            XPLAIN("CMD_JSON_FOREACH, KEY=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_JSON_FOREACH) ) {
                XPLAIN("CMD_JSON_FOREACH, VALUE=");
                if ( EXPECT_NEXT_COMMAND(COMMAND_JSON_FOREACH) ) {
                    XPLAIN("CMD_JSON_FOREACH, JSON VARIABLE=");
                    if ( EXPECT_NEXT_COMMAND(COMMAND_JSON_FOREACH) ) {
                        XPLAIN("CMD_JSON_FOREACH, TEXT=");
                        return ret;
                    }
                }
            }
        }
    }
    return 0;
}

static vecstring * exec_json_foreach ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_JSON_FOREACH ) {
        EXEC_START(COMMAND_JSON_FOREACH);
        EXEC_GET_ARG(COMMAND_JSON_FOREACH, key);
        EXEC_GET_ARG(COMMAND_JSON_FOREACH, value);
        EXEC_GET_ARG(COMMAND_JSON_FOREACH, json_var);
        EXEC_GET_ARG_NOEXEC(COMMAND_JSON_FOREACH, text);

        char * json_varp = vtopchar(json_var);
        // для начала проверяем есть ли вообще json_var
        json_value * json_var_value = json_path(json_varp, pcp);
        if ( json_var_value ) {
            if ( json_var_value->type == json_array ) {
                // начинаем цикл внутри массива
                unsigned int idx = 0;
                json_value * key_value = (json_value *) malloc (sizeof(json_value));
                for (; idx < json_var_value->u.array.length; ++idx) {
                    // создаем key
                    key_value->u.integer = idx;
                    key_value->type = json_integer;
                    allocate_json_item ( key, key_value );
                    // теперь создаем value
                    allocate_json_item ( value, json_var_value->u.array.values[idx] );

                    vecstring * itdata = pcp_do_exec(text, pcp);
                    vmove(dst, itdata);
                }
                // после того, как мы использовали key/value должно быть удалено
                remove_json_item(key);
                remove_json_item(value);
                free(key_value);
            } else if ( json_var_value -> type = json_object ) {
                // начинаем цикл внутри хэша
                unsigned int idx = 0;
                json_value * key_value = (json_value *) malloc (sizeof(json_value));
                for (; idx < json_var_value->u.object.length;++idx ) {
                    // создаем key

                    key_value->u.string.ptr = json_var_value->u.object.values[idx].name;
                    key_value->u.string.length = strlen(key_value->u.string.ptr);
                    key_value->type = json_string;
                    allocate_json_item ( key, key_value );
                    // теперь создаем value
                    allocate_json_item ( value, json_var_value->u.object.values[idx].value );

                    vecstring * itdata = pcp_do_exec(text, pcp);
                    vmove(dst, itdata);
                }
            } else {
                pcp_error(pcp, "Can not iterate on non object/array json variable %s\n", json_varp);
            }
        } else {
            pcp_error(pcp, "No json variable found: %s\n", json_varp);
        }
        free(json_varp);

        delete_vecstring(key);
        delete_vecstring(value);
        delete_vecstring(json_var);
        delete_vecstring(text);
        return dst;
    }
}
