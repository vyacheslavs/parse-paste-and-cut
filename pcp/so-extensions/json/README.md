JSON Extensions
---------------

# Syntax

    $(json-parse[<variable>] ... <json text> ... )$
    $(JSON::<variable><path>)$
    $(jsonprep <json text>)$
    $(json-foreach[<key>,<value>] in [<variable><path>]  ...<data> ... )$

# Purpose

$(json-parse)$ parses <json text> and stores JSON object in <variable>. You can access values of parsed json by using $(JSON::). The <path> is to be formatted like in examples:

Suppose, you have json like this:

    {
        "some-data" : [
            "some-val", "some-val-0"
        ]
    }

First, you need to parse this json: $(json-parse[json-text]{"some-data":[....]})$. Then, you can access "some-val" like this: $(JSON::json-text.some-data[0])$

$(jsonprep)$ cuts all spaces from <json text> and prepares strict style json.

$(json-foreach)$ iterates over <variable> and stores each iteration into <key>,<value>, so anything in <data> can access it. Both <key> and <value> are json lookup variables.
