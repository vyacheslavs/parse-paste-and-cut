#include "pcp/pcp.h"
#include <malloc.h>
#include "pcp/macros.h"
#include "json.h"
#include "json-storage.h"

/*
    Делает lookup для json объекта следующим образом:
    JSON::<имя переменной><путь>

    пример:

    JSON::val[0].hello
*/

static vecstring * parse_json_lookup ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_json_lookup( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_json_lookup ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_json_lookup = {
	.cb = parse_json_lookup,
	.cb_explain = xplain_json_lookup,
	.cb_exec = exec_json_lookup,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_JSON_LOOKUP
};

static vecstring * parse_json_lookup ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "JSON::";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_JSON_LOOKUP);
        GET_REST(json_var);

        cm = json_var->length + 8;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_JSON_LOOKUP, json_var);
        return dst;
    }
}

static size_t xplain_json_lookup( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_JSON_LOOKUP ) {
		XPLAIN_START
		if ( EXPECT_NEXT_COMMAND(COMMAND_JSON_LOOKUP) ) {
			XPLAIN("CMD_JSON_LOOKUP, VARIABLE=");
			return ret;
		}
	}
}

static vecstring * exec_json_lookup ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_JSON_LOOKUP ) {
        EXEC_START(COMMAND_JSON_LOOKUP);
        EXEC_GET_ARG(COMMAND_JSON_LOOKUP, var);
        char * pvar = vtopchar(var);

        json_value * val = json_path ( pvar, pcp );
        if ( val ) {
            switch (val->type) {
                case json_double: {
                    char tmp[0xfff];
                    snprintf ( tmp, 0xfff-1, "%.2f", val->u.dbl );
                    vmove ( dst, vecstring_from_pchar ( tmp ) );
                    break;
                }
                case json_integer: {
                    char tmp[0xfff];
                    snprintf ( tmp, 0xfff-1, "%lld", val->u.integer );
                    vmove ( dst, vecstring_from_pchar ( tmp ) );
                    break;
                }
                case json_string: {
                    vmove ( dst, vecstring_from_bchar( val->u.string.ptr, val->u.string.length ) );
                    break;
                }
                case json_boolean: {
                    vmove ( dst, vecstring_from_pchar( val->u.boolean ? "true" : "false" ) );
                    break;
                }
                case json_null: {
                    vmove ( dst, vecstring_from_pchar( "null" ) );
                    break;
                }
                case json_object: {
                    vmove ( dst, vecstring_from_pchar( "json#object" ) );
                    break;
                }
                case json_array: {
                    vmove ( dst, vecstring_from_pchar( "json#array" ) );
                    break;
                }
            }
        }

        free(pvar);
        delete_vecstring(var);
        return dst;
    }
}
