#include "pcp/pcp.h"

extern pcp_handler handler_db;
extern pcp_handler handler_fetch_sql;
extern pcp_handler handler_qs;
extern pcp_handler handler_row;
extern pcp_handler handler_sql;

pcp_handler * register_pcp_handlers[] = {
	&handler_db,
	&handler_fetch_sql,
	&handler_qs,
	&handler_row,
	&handler_sql,
	NULL
};
