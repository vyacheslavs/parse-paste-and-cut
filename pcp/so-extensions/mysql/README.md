Mysql extension for PCP
-----------------------

# Syntax

    $(db <database handle>[=<dsn>])$ : dsn = <host>;<db>;<user>;<password>[;port]
    $(sql <handle>=<sql statement>)$
    $(fetch_sql[<handle>] ...<data> ...)$
    $(row <handle>[<index>])$

# Purpose

## $(db)$

$(db) extension connects to mysql database using <dsn> and attaches connection to <database handle>.
When using two or more connections to different databases you can switch among them by using
$(db) without specifying <dsn>.

For example,

    $(db mydb1=localhost;mydb;root;root)$ - connect to database at localhost and set mydb1 as active.
    $(db mydb2=192.168.0.3;mydb;root;root)$ - also connect to database at 192.168.0.3 and set mydb2 as active.
    .. work with mydb2 ..
    $(db mydb1)$ - switch to database mydb1

## $(sql)$

$(sql)$ queries active database for <sql statement> and assigns the result to <handle>

For example,

    $(sql h=SELECT * FROM tbl ORDER BY id)$

## $(fetch_sql)$ fetches table data. Each row can be accessed by calling $(row)$ pragma. Each row's <data> is to be concatinated to previuos row's data.

For example,

    $(fetch_sql[h] this is row number $(row h[::index])$)$. For 3 rows it will be: this is row number 0 this is row number 1 this is row number 2

## $(row)$

    Fetches data from row. <Index> could be a column name, or integer index, or special ::index (meaning current index)

