#include "pcp/pcp.h"
#include "sqlhandles.h"
#include "fieldresolv.h"
#include <assert.h>

static vecstring * parse_row ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_row( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_row ( struct __pcp_t * pcp, vecstring * src, int * parsed );
static void getrow(vecstring * dst, vecstring * handle, vecstring * index );

pcp_handler handler_row = {
	.cb = parse_row,
	.cb_explain = xplain_row,
	.cb_exec = exec_row,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_MYSQL_ROW
};

static vecstring * parse_row ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "row ") ) {
		*parsed = 1; // говорим, что мы беремся за парсинг сразу
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_MYSQL_ROW;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		// теперь выдергиваем handle
		size_t stopped_offset=-1;
		vecstring * argdata = subvstr(src, 4, SIZE_MAX);
		vecstring * handle = pcp_do_parse( argdata, pcp, stop_on_openbr, &stopped_offset );
		delete_vecstring(argdata);
		
		if ( stopped_offset == -1 ) {
			pcp_error(pcp, "syntax error while parsing <row>\n" );
			return NULL;
		}
		
		// теперь выдергиваем колонку
		size_t stopped_offset2=-1;
		argdata = subvstr(src, 4 + stopped_offset+1, SIZE_MAX);
		vecstring * columname = pcp_do_parse( argdata, pcp, stop_on_closingbr, &stopped_offset2 );
		delete_vecstring(argdata);
		
		if ( stopped_offset2 == -1 ) {
			pcp_error(pcp, "syntax error while parsing <row>\n" );
			return NULL;
		}
		
		// теперь записываем полностью команду
		cm = 2*8 + handle->length + columname->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		// пишем handle
		cm = COMMAND_MYSQL_HANDLE;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = handle->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, handle);
		cm = COMMAND_MYSQL_STTMENT;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = columname->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, columname);
		return dst;
	}
	return NULL;
}

static size_t xplain_row( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_MYSQL_ROW ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_ROW, HANDLE=\n");
		size_t ret = 8;
		if (pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_MYSQL_HANDLE ) {
			ret+=8;
			vecstring * cmdata = subvstr( parsed, ret, sz );
			ret += sz;
			pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
			delete_vecstring(cmdata);
		
			pcp_intend(lvl, stream);
			pcp_read_command( &cm, &sz, parsed, ret);
			if (sz) {
				fprintf(stream, "CMD_ROW_FIELD, FIELD=\n");
				ret+=8;
				cmdata = subvstr( parsed, ret, sz );
				pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
				delete_vecstring(cmdata);
			}
		} else {
			
			return 0;
		}
		return ret;
	}
	return 0;
}

static vecstring * exec_row ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_MYSQL_ROW ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		size_t oft = 8;
		if ( pcp_read_command (&cm, &sz, src, oft ) && cm == COMMAND_MYSQL_HANDLE ) {
			oft+=8;
			vecstring * cmdata = subvstr( src, oft, sz );
			oft += sz;
			vecstring * handle = pcp_do_exec (cmdata, pcp);
			if (!handle)
				return NULL;
			delete_vecstring(cmdata);
			if ( pcp_read_command (&cm, &sz, src, oft ) && cm == COMMAND_MYSQL_STTMENT ) {
				oft+=8;
				cmdata = subvstr( src, oft, sz );
				oft+=sz;
				vecstring * index = pcp_do_exec(cmdata, pcp);
				getrow(dst, handle, index);
				delete_vecstring(index);
				delete_vecstring(cmdata);
			}
			delete_vecstring(handle);
			return dst;
		} else {
			return NULL;
		}
	}
	return NULL;
}

static void getrow(vecstring * dst, vecstring * handle, vecstring * index ) {
	sqlst_node * node = find_sqlstatement ( handle );
	if (!node || !node->result)
		return;
	assert(node->frctx);
	
	// а вдруг это индекс? ::index
	if ( index->length == 7 && vstrcmp(index, "::index")) {
		char tmp[0xff];
		snprintf(tmp, 0xff-1, "%lld", node->index);
		vecstring * vindex = vecstring_from_pchar(tmp);
		vmove(dst, vindex);
		return;
	}
	
	int idx = field_resolve ( node->frctx, index );
	if (idx<0)
		return;
	if ( !node->current_row || !node->current_row[idx] )
		return;
	vecstring * data = vecstring_from_pchar(node->current_row[idx]);
	vmove(dst, data);
}
