#include "pcp/pcp.h"
#include "sqlhandles.h"
#include <pcp/macros.h>

static vecstring * parse_sql ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_sql( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_sql ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_sql = {
	.cb = parse_sql,
	.cb_explain = xplain_sql,
	.cb_exec = exec_sql,
	.cb_init = sqlst_init,
	.cb_fini = sqlst_fini,
	.probe_command = COMMAND_MYSQL_SQL
};

static vecstring * parse_sql ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "sql ";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_MYSQL_SQL);
        PARSE(handle, stop_on_eq);
        GET_REST(sql_statement);

        cm = handle->length + sql_statement->length + 8*2;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_MYSQL_SQL, handle);
        SERIALIZE(COMMAND_MYSQL_SQL, sql_statement);
        return dst;
    }
    return NULL;
}

static size_t xplain_sql( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_MYSQL_SQL ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_MYSQL_SQL) ) {
            XPLAIN("CMD_MYSQL_SQL, HANDLE=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_MYSQL_SQL) ) {
                XPLAIN("CMD_MYSQL_SQL, STATEMENT=");
                return ret;
            }
        }
    }
    return 0;
}

static vecstring * exec_sql ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_MYSQL_SQL ) {
        EXEC_START(COMMAND_MYSQL_SQL);
        EXEC_GET_ARG(COMMAND_MYSQL_SQL, handle);
        EXEC_GET_ARG(COMMAND_MYSQL_SQL, sql_statement);

        allocate_sqlstatement(handle, sql_statement);

        delete_vecstring(sql_statement);
        delete_vecstring(handle);
        return dst;
    }
    return NULL;
}

