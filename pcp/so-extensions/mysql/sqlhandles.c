#include "sqlhandles.h"
#include "mempool/mempool.h"
#include <assert.h>
#include <malloc.h>
#include <string.h>
#include "dbhandles.h"

static sqlst_node ** ht = NULL;
static mempool ht_pool;

void allocate_sqlstatement( vecstring * name, vecstring * st ) {
	if (!get_active_handle()) {
		fprintf(stderr, "no active mysql connection\n");
		return;
	}
	int tree_place = 0;
	sqlst_node * wnode = find_sqlstatement(name);
	if (!wnode) {
		wnode = (sqlst_node *)mp_alloc(&ht_pool);
		wnode->name = vshare (name);
		wnode->result = NULL;
		wnode->frctx = NULL;
		wnode->index = 0;
		tree_place = 1;
	} else if (wnode->result) {
		delete_vecstring(wnode->statement);
		mysql_free_result(wnode->result);
		wnode->result = NULL;
		wnode->index = 0;
		fr_fini(wnode->frctx);
		wnode->frctx = NULL;
	}
	wnode->statement = vshare(st);
	char * statement = vtopchar(wnode->statement);
	if (mysql_real_query(get_active_handle(), statement, st->length)) {
		fprintf(stderr, "error executing query: %s in statement: [%s]\n", mysql_error(get_active_handle()), statement);
		if (tree_place) {
			delete_vecstring(wnode->name);
			delete_vecstring(wnode->statement);
			mp_free(&ht_pool, wnode);
		}
		free(statement);
		return;
	}
	free(statement);
	wnode->result = mysql_store_result(get_active_handle());
	if (mysql_errno(get_active_handle())!=0) {
		fprintf(stderr, "error while storing sql result: %s (%d)\n", mysql_error(get_active_handle()), mysql_errno(get_active_handle()));
		if (tree_place) {
			delete_vecstring(wnode->name);
			delete_vecstring(wnode->statement);
			mp_free(&ht_pool, wnode);
		}
		return;
	}
	
	if (get_servar_hook()) {
		char * affected = vtopchar(name);
		char * vaff = (char *) malloc ( name->length+100 );
		char naff[0xff];
		snprintf(vaff, name->length+99, "%s::insert-id", affected);
		uint64_t num = mysql_insert_id(get_active_handle());
		snprintf(naff, 0xff-1, "%lld", num);
		vecstring * vaff_v = vecstring_from_pchar(vaff);
		vecstring * naff_v = vecstring_from_pchar(naff);
		
		get_servar_hook()(vaff_v, naff_v );
		
		delete_vecstring(naff_v);
		delete_vecstring(vaff_v);
		
		free(vaff);
		free(affected);
	}
	
	if (!wnode->result) {
		if (tree_place) {
			delete_vecstring(wnode->name);
			delete_vecstring(wnode->statement);
			mp_free(&ht_pool, wnode);
		}
		return;
	}
	if (get_servar_hook()) {
		char * affected = vtopchar(name);
		char * vaff = (char *) malloc ( name->length+100 );
		char naff[0xff];
		snprintf(vaff, name->length+99, "%s::aff-rows-number", affected);
		uint64_t num = mysql_affected_rows(get_active_handle());
		snprintf(naff, 0xff-1, "%lld", num);
		vecstring * vaff_v = vecstring_from_pchar(vaff);
		vecstring * naff_v = vecstring_from_pchar(naff);
		
		get_servar_hook()(vaff_v, naff_v );
		
		delete_vecstring(naff_v);
		delete_vecstring(vaff_v);
		
		free(vaff);
		free(affected);
	}
	
	// теперь заполняем field resolver
	wnode->frctx = fr_init(wnode->result);
	if (tree_place) {
		uint64_t key = vecstring_keyval ( name ) % SQLST_HTSIZE;
		wnode->next = ht[key];
		ht[key] = wnode;
	}
}

sqlst_node * find_sqlstatement( vecstring * name ) {
	uint64_t key = vecstring_keyval ( name ) % SQLST_HTSIZE;
	sqlst_node * it = ht[key];
	while (it) {
		if ( vstr_compare(it->name, name))
			return it;
		it = it->next;
	}
	return NULL;
}

void sqlst_init(void * r) {
	resolver_init(r);
	ht = (sqlst_node **) malloc (sizeof(sqlst_node *) * SQLST_HTSIZE);
	assert(ht);
	memset(ht, 0, sizeof(sqlst_node *) * SQLST_HTSIZE);
	mp_init(&ht_pool, sizeof(sqlst_node));
}

void sqlst_fini(void * r) {
	int idx=0;
	for (;idx<SQLST_HTSIZE;++idx) {
		sqlst_node * it = ht[idx];
		while (it) {
			ht[idx] = it->next;
			delete_vecstring(it->name);
			delete_vecstring(it->statement);
			if (it->result) {
				mysql_free_result(it->result);
				fr_fini(it->frctx);
				it->result = NULL;
			}
			mp_free(&ht_pool, it);
			it = ht[idx];
		}
	}
	free(ht);
	mp_fini(&ht_pool);
	resolver_fini(r);
}

