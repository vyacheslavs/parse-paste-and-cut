#include "pcp/pcp.h"
#include "dbhandles.h"
#include <malloc.h>

static vecstring * parse_qs ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_qs( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_qs ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_qs = {
	.cb = parse_qs,
	.cb_explain = xplain_qs,
	.cb_exec = exec_qs,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_MYSQL_QS
};

static vecstring * parse_qs ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "qs ") ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_MYSQL_QS;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		vecstring * val = subvstr(src,3, SIZE_MAX);
		vecstring * outval = pcp_do_parse( val, pcp, NULL, NULL );
		delete_vecstring(val);
		
		cm = outval->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, outval);
		return dst;
	}
	return NULL;
}

static size_t xplain_qs( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_MYSQL_QS ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_MYSQL_QS, DATA=\n");
		vecstring * cmdata = subvstr( parsed, 8, sz );
		pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
		delete_vecstring(cmdata);
		return sz+8;
	}
	return 0;
}

static vecstring * exec_qs ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_MYSQL_QS ) {
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		vecstring * cmdata = subvstr( src, 8, sz );
		vecstring * val = pcp_do_exec( cmdata, pcp );
		delete_vecstring(cmdata);
		// после того, как мы получили данные необходимо сделать 
		size_t datalen = val->length;
		char * data = vtopchar(val);
		delete_vecstring(val);
		
		char * to = (char *) malloc (datalen * 2 + 1);
		char * pto = to, *pfrom = data;
		int i=0;
		while (i<datalen) {
			if (*pfrom == '"') {
				*pto = '\\';
				pto++;
			}
			if (*pfrom == '\n' || *pfrom == '\r' ) {
				i++;
				pfrom++;
				continue;
			}
			*pto = *pfrom;
			pto++;
			pfrom++;
			i++;
		}
		*pto = 0;
		vmove(dst, vecstring_from_pchar(to));
		free(to);
		free(data);
		return dst;
	}
	return NULL;
}
