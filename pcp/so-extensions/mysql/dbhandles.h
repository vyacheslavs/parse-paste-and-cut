#ifndef __DB_HANDLES_H
#define __DB_HANDLES_H

/**
 * dbhandles - это простая хэш таблица
 */

#ifndef DBHANDLES_HT_SIZE
#define DBHANDLES_HT_SIZE 256
#endif

#include "pcp/pcp.h"
#include <mysql.h>

typedef struct __dbhandles_node {
	vecstring * name;
	vecstring * dsn;
	MYSQL * handle;
	struct __dbhandles_node * next;
} dbhandle_node;

void allocate_dbhandle ( pcp_t * pcp, vecstring * name, vecstring * dsn );
dbhandle_node * find_dbhandle ( vecstring * name );
void dbhandlers_init(void * reserve);
void dbhandlers_fini(void * reserve);
MYSQL * get_active_handle();

#endif
