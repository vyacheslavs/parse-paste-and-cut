#include "fieldresolv.h"
#include "mempool/mempool.h"
#include <assert.h>
#include <malloc.h>
#include <string.h>

static mempool ht_pool;

field_resolv_ctx fr_init(MYSQL_RES * result) {
	field_resolv_ctx ctx = (field_resolv_ctx) malloc( sizeof(field_resolv_node *) * __FIELD_RESOLV_HTSIZE );
	memset(ctx, 0, sizeof(field_resolv_node *) * __FIELD_RESOLV_HTSIZE );
	
	char tmp[0xfff];
	MYSQL_FIELD *field;
	int idx=0;
	while ( (field = mysql_fetch_field(result) ) ) {
		// добавляем новое поле в resolver
		
		field_resolv_node * newnode = (field_resolv_node *)mp_alloc(&ht_pool);
		newnode->fieldname = vecstring_from_pchar (field->name);
		newnode->index = idx;
		
		uint64_t key = vecstring_keyval ( newnode->fieldname ) % __FIELD_RESOLV_HTSIZE;
		newnode->next = ctx[key];
		ctx[key] = newnode;
		
		// добавляем алиас
		newnode = (field_resolv_node *)mp_alloc(&ht_pool);
		snprintf(tmp, 0xfff-1, "%d", idx);
		newnode->fieldname = vecstring_from_pchar (tmp);
		newnode->index = idx;
		
		key = vecstring_keyval ( newnode->fieldname ) % __FIELD_RESOLV_HTSIZE;
		newnode->next = ctx[key];
		ctx[key] = newnode;
		
		++idx;
	}
	
	return ctx;
}

void fr_fini(field_resolv_ctx ctx) {
	int idx=0;
	while (idx < __FIELD_RESOLV_HTSIZE) {
		field_resolv_node * it = ctx[idx];
		while (it) {
			ctx[idx] = it->next;
			delete_vecstring(it->fieldname);
			mp_free(&ht_pool, it);
			it = ctx[idx];
		}
		++idx;
	}
	free(ctx);
}

void resolver_init(void * r) {
	mp_init (&ht_pool, sizeof(field_resolv_node));
}

void resolver_fini(void * r) {
	mp_fini(&ht_pool);
}

int field_resolve ( field_resolv_ctx ctx, vecstring * field ) {
	uint64_t key = vecstring_keyval ( field ) % __FIELD_RESOLV_HTSIZE;
	field_resolv_node * it = ctx[key];
	while (it) {
		if ( vstr_compare(it->fieldname, field) )
			return it->index;
		it = it->next;
	}
	return -1;
}
