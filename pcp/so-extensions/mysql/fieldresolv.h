#ifndef __FIELD_RESOLVING
#define __FIELD_RESOLVING

#include "pcp/pcp.h"
#include <mysql.h>

#ifndef __FIELD_RESOLV_HTSIZE
#define __FIELD_RESOLV_HTSIZE 16
#endif

typedef struct __field_hasht {
	vecstring * fieldname;
	int index;
	struct __field_hasht * next;
} field_resolv_node;

typedef field_resolv_node ** field_resolv_ctx;

field_resolv_ctx fr_init(MYSQL_RES * result);
void fr_fini(field_resolv_ctx ctx);

void resolver_init(void * r);
void resolver_fini(void * r);

int field_resolve ( field_resolv_ctx ctx, vecstring * field );

#endif
