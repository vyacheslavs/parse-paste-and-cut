#ifndef __SQL_HANDLES
#define __SQL_HANDLES

#include "pcp/pcp.h"
#include <mysql.h>
#include "fieldresolv.h"

#ifndef SQLST_HTSIZE
#define SQLST_HTSIZE 256
#endif

typedef struct __sqlst_node {
	vecstring * name;
	vecstring * statement;
	MYSQL_RES * result;
	MYSQL_ROW current_row;
	field_resolv_ctx frctx;
	uint64_t index;
	struct __sqlst_node * next;
} sqlst_node;

void allocate_sqlstatement( vecstring * name, vecstring * st );
sqlst_node * find_sqlstatement( vecstring * name );

void sqlst_init(void * r);
void sqlst_fini(void * r);

#endif
