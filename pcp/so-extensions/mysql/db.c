#include "pcp/pcp.h"
#include "dbhandles.h"
#include "pcp/macros.h"
/*
    db extension:

    usage:
        $(db mydb[=<dsn>])$
        dsn = <host>;<db>;<user>;<password>[;port]
*/

static vecstring * parse_db ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_db( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_db ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_db = {
	.cb = parse_db,
	.cb_explain = xplain_db,
	.cb_exec = exec_db,
	.cb_init = dbhandlers_init,
	.cb_fini = dbhandlers_fini,
	.probe_command = COMMAND_MYSQL_DB
};

static vecstring * parse_db ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "db ";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_MYSQL_DB);
        PARSE_WITH_CUSTOM_FAIL_HANDLER_START(handle, stop_on_eq)
        {
            // здесь описываем что делать, если `равно` не нашлось
            vecstring * dsn = new_vecstring(0);
            cm = handle->length+8*2;
            vwrite(dst, &cm, sizeof(uint32_t));
            SERIALIZE(COMMAND_MYSQL_DB,handle);
            SERIALIZE(COMMAND_MYSQL_DB,dsn);
            return dst;
        }
        PARSE_WITH_CUSTOM_FAIL_HANDLER_END(handle)
        GET_REST(dsn);
        cm = handle->length+dsn->length+8*2;
        vwrite(dst, &cm, sizeof(uint32_t));
        SERIALIZE(COMMAND_MYSQL_DB,handle);
        SERIALIZE(COMMAND_MYSQL_DB,dsn);
        return dst;
    }
    return NULL;
}

static size_t xplain_db( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_MYSQL_DB ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_MYSQL_DB) ) {
            XPLAIN("CMD_MYSQL_DB, handle=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_MYSQL_DB) ) {
                XPLAIN("CMD_MYSQL_DB, dsn=");
                return ret;
            }
        }
    }
    return 0;
}

static vecstring * exec_db ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	size_t oft = 0;
	if ( pcp_read_command( &cm, &sz, src, oft) && cm == COMMAND_MYSQL_DB ) {
		oft+=8;
		*parsed = 1;
		vecstring * dst = new_vecstring(0);
		pcp_read_command( &cm, &sz, src, oft);
		oft+=8;
		vecstring * cmdata = subvstr( src, oft, sz );
		oft += sz;
		vecstring * handle = pcp_do_exec (cmdata, pcp);
		if (!handle)
			return NULL;
		delete_vecstring(cmdata);
		pcp_read_command( &cm, &sz, src, oft);
		oft+=8;
		if (sz) {
			cmdata = subvstr( src, oft, sz );
			oft+=sz;
			vecstring * dsn = pcp_do_exec(cmdata,pcp);
			delete_vecstring(cmdata);
			allocate_dbhandle ( pcp, handle, dsn );
			delete_vecstring(dsn);
		} else {
			allocate_dbhandle ( pcp, handle, NULL );
		}
		delete_vecstring(handle);
		return dst;
	}
	return NULL;
}
