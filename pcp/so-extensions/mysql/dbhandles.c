#include "dbhandles.h"
#include "mempool/mempool.h"
#include <assert.h>
#include <malloc.h>
#include <string.h>

static dbhandle_node ** ht = NULL;
static mempool ht_pool;
static MYSQL * db_connect ( pcp_t * pcp, vecstring * dsn );
static MYSQL * active_connection = NULL;

MYSQL * get_active_handle() {
	return active_connection;
}

void allocate_dbhandle ( pcp_t * pcp, vecstring * name, vecstring * dsn ) {
	dbhandle_node * node = find_dbhandle(name);
	if ( node ) {
		active_connection = node->handle;
		return;
	}
	if (!dsn)
		return;
	MYSQL * h = db_connect(pcp, dsn);
	if (!h)
		return;
	active_connection = h;
	uint64_t key = vecstring_keyval ( name ) % DBHANDLES_HT_SIZE;
	dbhandle_node * newnode = (dbhandle_node *) mp_alloc(&ht_pool);
	newnode->name = vshare(name);
	newnode->dsn = vshare(dsn);
	newnode->handle = h;
	newnode->next = ht[key];
	ht[key] = newnode;
}

dbhandle_node * find_dbhandle ( vecstring * name ) {
	uint64_t key = vecstring_keyval ( name ) % DBHANDLES_HT_SIZE;
	dbhandle_node * it = ht[key];
	while (it) {
		if ( vstr_compare(it->name, name))
			return it;
		it = it->next;
	}
	return NULL;
}

void dbhandlers_init(void * reserve) {
	ht = (dbhandle_node **) malloc ( sizeof(dbhandle_node *) * DBHANDLES_HT_SIZE );
	assert(ht);
	memset(ht, 0, sizeof(dbhandle_node *) * DBHANDLES_HT_SIZE);
	mp_init(&ht_pool, sizeof(dbhandle_node));
}

void dbhandlers_fini(void * reserve) {
	// надо пробежаться по хэшу и убить всех неверующих
	int idx=0;
	for (;idx<DBHANDLES_HT_SIZE;++idx) {
		dbhandle_node * it = ht[idx];
		while (it) {
			ht[idx] = it->next;
			delete_vecstring(it->name);
			delete_vecstring(it->dsn);
			mysql_close(it->handle);
			mp_free(&ht_pool, it);
			it = ht[idx];
		}
	}
	free(ht);
	mp_fini( &ht_pool );
	mysql_library_end();
}

static MYSQL * db_connect ( pcp_t * pcp, vecstring * dsn ) {
	char * dsnchar = vtopchar ( dsn );
    char * odsn = strdup(dsnchar);
	MYSQL * r = NULL;
	assert(dsnchar);
	
	int i = 0;
	char * it = strtok(dsnchar, ",;: ");
	char * host = NULL;
	char * db = NULL;
	char * user = NULL;
	char * pwd = NULL;
	char * port = NULL;
	while (it) {
		switch(i) {
			case 0: host = it; break;
			case 1: db = it; break;
			case 2: user = it; break;
			case 3: pwd = it; break;
			case 4: port = it; break;
		}
		++i;
		it = strtok(NULL, ",;: ");
	}
	
	if (!host) {
		pcp_log(pcp, PCP_LOG_ERROR, COMMAND_MYSQL_DB, "no host specified in db_connect using dsn %s\n", odsn);
		return NULL;
	}
	if (!user) {
		pcp_log(pcp, PCP_LOG_ERROR, COMMAND_MYSQL_DB, "no user specified in db_connect using dsn %s\n", odsn);
		return NULL;
	}
	if (!db) {
		pcp_log(pcp, PCP_LOG_ERROR, COMMAND_MYSQL_DB, "no db specified in db_connect using dsn %s\n", odsn);
		return NULL;
	}
	// now real connect
	r = mysql_init(NULL);
    if (r) {
        if (!mysql_real_connect(r, host, user, pwd?pwd:"", db, port?atoi(port):0, NULL, 0)) {
            pcp_log(pcp, PCP_LOG_ERROR, COMMAND_MYSQL_DB, "unable to connect to mysql using dsn %s: %s\n", odsn, mysql_error(r));
            r = NULL;
        } else {
            pcp_log(pcp, PCP_LOG_DEBUG, COMMAND_MYSQL_DB, "connected to %s\n", odsn);
        }
    } else
        pcp_log(pcp, PCP_LOG_ERROR, COMMAND_MYSQL_DB, "failed to initialize mysql\n");
	free(dsnchar);
    free(odsn);
	return r;
}
