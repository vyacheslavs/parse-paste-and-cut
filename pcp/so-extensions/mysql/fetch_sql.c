#include "pcp/pcp.h"
#include "sqlhandles.h"
#include "pcp/macros.h"

static vecstring * parse_fetch_sql ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_fetch_sql( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_fetch_sql ( struct __pcp_t * pcp, vecstring * src, int * parsed );
static void fetch_sql_data ( vecstring * dst, vecstring * handle, vecstring * data, struct __pcp_t * pcp );

pcp_handler handler_fetch_sql = {
	.cb = parse_fetch_sql,
	.cb_explain = xplain_fetch_sql,
	.cb_exec = exec_fetch_sql,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_MYSQL_FETCHSQL
};

static vecstring * parse_fetch_sql ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "fetch_sql[";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_MYSQL_FETCHSQL);
        PARSE(handle, stop_on_closingbr);
        GET_REST(data);

        cm = handle->length + data->length + 8*2;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_MYSQL_FETCHSQL, handle);
        SERIALIZE(COMMAND_MYSQL_FETCHSQL, data);
        return dst;
    }
    return NULL;
}

static size_t xplain_fetch_sql( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_MYSQL_FETCHSQL ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_MYSQL_FETCHSQL) ) {
            XPLAIN("CMD_MYSQL_FETCHSQL, HANDLE=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_MYSQL_FETCHSQL) ) {
                XPLAIN("CMD_MYSQL_FETCHSQL, STATEMENT=");
                return ret;
            }
        }
    }
    return 0;
}

static vecstring * exec_fetch_sql ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_MYSQL_FETCHSQL ) {
        EXEC_START(COMMAND_MYSQL_FETCHSQL);
        EXEC_GET_ARG(COMMAND_MYSQL_FETCHSQL, handle);
        EXEC_GET_ARG_NOEXEC(COMMAND_MYSQL_FETCHSQL, data);
        fetch_sql_data(dst, handle, data, pcp);
        delete_vecstring(data);
        delete_vecstring(handle);
        return dst;
    }
    return NULL;
}

static void fetch_sql_data ( vecstring * dst, vecstring * handle, vecstring * data, struct __pcp_t * pcp ) {
	// ищем handle
	sqlst_node * node = find_sqlstatement ( handle );
	if (!node || !node->result)
		return;
	
	mysql_data_seek(node->result, 0);
	node->index = 0;
	// нашли handle
	while ((node->current_row = mysql_fetch_row ( node->result ))) {
		node->index++;
		vecstring * rowdata = pcp_do_exec(data, pcp);
		if (rowdata) vmove(dst, rowdata);
	}
}
