#include "pcp/pcp.h"

extern pcp_handler handler_dump;
extern pcp_handler handler_load;

pcp_handler * register_pcp_handlers[] = {
	&handler_dump,
	&handler_load,
	NULL
};
