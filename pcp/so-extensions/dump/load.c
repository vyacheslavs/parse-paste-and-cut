#include "pcp/pcp.h"
#include "pcp/macros.h"
#include <fcntl.h>
#include <malloc.h>
/*
     $(load[ <filename>[,<varname>] ])$ - загружает в перменную variablename данные из файла. Если filename не указан, то данные будут загружены в место вызова
 */

static vecstring * parse_load ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_load( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_load ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_load = {
	.cb = parse_load,
	.cb_explain = xplain_load,
	.cb_exec = exec_load,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_LOAD
};

static vecstring * parse_load ( vecstring * src, pcp_t * pcp, int * parsed ) {
	const char key[] = "load[";
	if ( vstrcmp(src, key) ) {
		PARSE_START(COMMAND_LOAD);
		// берем op
		PARSE(filename,stop_on_comma);
		// берем val
		PARSE(varname,stop_on_closingbr);
		// записываем комманду
		cm = varname->length + filename->length + 2*8;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = COMMAND_LOAD_FNAME;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = filename->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,filename);
		cm = COMMAND_LOAD_VNAME;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = varname->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst,varname);
		return dst;
	}
}

static size_t xplain_load( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_LOAD ) {
		XPLAIN_START
		if ( EXPECT_NEXT_COMMAND(COMMAND_LOAD_FNAME) ) {
			XPLAIN("CMD_LOAD, FNAME=");
			if ( EXPECT_NEXT_COMMAND(COMMAND_LOAD_VNAME) ) {
				XPLAIN("CMD_LOAD, VNAME=");
				return ret;
			}
		}
	}
}

static vecstring * exec_load ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_LOAD ) {
		EXEC_START(COMMAND_LOAD);
		EXEC_GET_ARG(COMMAND_LOAD_FNAME,fname);
		EXEC_GET_ARG(COMMAND_LOAD_VNAME,vname);
		// берем содержимое файла
		char * filename = vtopchar(fname);
		int fd = open(filename, O_RDONLY);
		if (fd>=0) {
			vecstring * contents = new_vecstring(0);
			unsigned char chunk[0xfff];
			int nr = 0;
			while ( (nr = read(fd, chunk, 0xfff))>0 ) {
				vwrite(contents, chunk, nr);
			}
			close(fd);
			if ( vname->length > 0 ) {
				get_servar_hook() ( vname, contents );
				delete_vecstring(contents);
			} else {
				vmove(dst, contents);
			}
		} else {
			fprintf(stderr, "unable to load [%s]\n", filename);
		}
		free(filename);
		delete_vecstring(fname);
		delete_vecstring(vname);
		return dst;
	}
}
