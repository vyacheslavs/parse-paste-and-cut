#include "pcp/pcp.h"
#include <malloc.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

/*
    $(dump[<variablename>,<filename>])$
    дампит переменную variablename
    если указана вторая переменная, то результат сохраняется в filename
 */

static vecstring * parse_dump ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_dump( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_dump ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_dump = {
	.cb = parse_dump,
	.cb_explain = xplain_dump,
	.cb_exec = exec_dump,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_DUMP
};

static vecstring * parse_dump ( vecstring * src, pcp_t * pcp, int * parsed ) {
	if ( vstrcmp(src, "dump[") ) {
		*parsed = 1; // говорим, что мы беремся за парсинг сразу
		vecstring * dst = new_vecstring(0);
		uint32_t cm = COMMAND_DUMP;
		vwrite(dst, &cm, sizeof(uint32_t));
		
		size_t vname_offset = -1;
		vecstring * argdata = subvstr(src, 5, SIZE_MAX);
		vecstring * vname = pcp_do_parse( argdata, pcp, stop_on_comma, &vname_offset );
		delete_vecstring(argdata);
		
		if ( vname_offset == -1 ) {
			pcp_error(pcp, "syntax error while parsing <dump>: vname\n" );
			delete_vecstring(vname);
			delete_vecstring(dst);
			return NULL;
		}
		
		size_t fname_offset = -1;
		argdata = subvstr(src, 5+vname_offset+1, SIZE_MAX);
		vecstring * fname = pcp_do_parse( argdata, pcp, stop_on_closingbr, &fname_offset );
		delete_vecstring(argdata);
		
		if ( fname_offset == -1 ) {
			pcp_error(pcp, "syntax error while parsing <dump>: fname\n" );
			delete_vecstring(vname);
			delete_vecstring(fname);
			delete_vecstring(dst);
			return NULL;
		}
		
		// записываем комманду
		cm = 2*8 + vname->length + fname->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = COMMAND_DUMP_VNAME;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = vname->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, vname);
		cm = COMMAND_DUMP_FNAME;
		vwrite(dst, &cm, sizeof(uint32_t));
		cm = fname->length;
		vwrite(dst, &cm, sizeof(uint32_t));
		vmove(dst, fname);
		return dst;
	}
}

static size_t xplain_dump( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_DUMP ) {
		pcp_intend(lvl, stream);
		fprintf(stream, "CMD_DUMP\n");
		size_t ret = 8;
		if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_DUMP_VNAME ) {
			pcp_intend(lvl, stream);
			ret+=8;
			vecstring * cmdata = subvstr( parsed, ret, sz );
			fprintf(stream, "CMD_DUMP, VNAME=\n");
			pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
			delete_vecstring(cmdata);
			ret+=sz;
			if ( pcp_read_command( &cm, &sz, parsed, ret) && cm == COMMAND_DUMP_FNAME ) {
				pcp_intend(lvl, stream);
				ret+=8;
				cmdata = subvstr( parsed, ret, sz );
				fprintf(stream, "CMD_DUMP, FNAME=\n");
				pcp_do_xplain( pcp, stream, cmdata, lvl+1 );
				delete_vecstring(cmdata);
				ret+=sz;
				return ret;
			}
		}
	}
	return 0;
}

static vecstring * exec_dump ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
	uint32_t cm, sz;
	if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_DUMP ) {
		vecstring * dst = new_vecstring(0);
		*parsed = 1;
		size_t ret = 8;
		if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_DUMP_VNAME ) {
			ret+=8;
			vecstring * cmdata = subvstr( src, ret, sz );
			vecstring * vname = pcp_do_exec ( cmdata, pcp );
			delete_vecstring(cmdata);
			ret+=sz;
			if ( pcp_read_command( &cm, &sz, src, ret) && cm == COMMAND_DUMP_FNAME ) {
				ret+=8;
				vecstring * cmdata = subvstr( src, ret, sz );
				vecstring * fname = pcp_do_exec ( cmdata, pcp );
				
				char * filename = vtopchar(fname);
				vecstring * val = get_lookupvar_hook() ( vname );
				int fd = open ( filename, O_CREAT|O_RDWR|O_TRUNC, 0644 );
				if (fd>0) {
					
					vecstring_chain * it = val->head;
					while (it) {
						write(fd, refstring_ptr(it->refstr)+it->payload_offset, it->payload_size);
						it = it->next;
					}
					close(fd);
				}
				free(filename);
				delete_vecstring(val);
				delete_vecstring(cmdata);
				delete_vecstring(vname);
				delete_vecstring(fname);
				return dst;
			}
		}
	}
	return NULL;
}
