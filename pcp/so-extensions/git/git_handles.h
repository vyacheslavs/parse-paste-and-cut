#ifndef __GIT_HANDLES_H
#define __GIT_HANDLES_H

#include <htlib.h>
#include <vecstring/vecstring.h>
#include <git2.h>

struct git_context {
    struct hlist_node hash;
    vecstring * ghandle;
    git_repository *repo;
    git_oid oid;
    git_revwalk *walk;
    git_commit * commit;
};

void git_handles_init(void *r);
void git_handles_fini(void *r);

struct git_context * allocate_git_context(const vecstring * handle);
struct git_context * get_git_context(const vecstring * handle);

#endif
