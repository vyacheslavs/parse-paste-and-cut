#include "pcp/pcp.h"
#include "pcp/macros.h"
#include "git_handles.h"


/*
    Usage:

    $(git_walk[<handle>=<git link>]
        
    )$
*/

static vecstring * parse_git_walk ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_git_walk( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_git_walk ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_git_walk = {
	.cb = parse_git_walk,
	.cb_explain = xplain_git_walk,
	.cb_exec = exec_git_walk,
	.cb_init = git_handles_init,
	.cb_fini = git_handles_fini,
	.probe_command = COMMAND_GIT_WALK
};

static vecstring * parse_git_walk ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "git_walk[";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_GIT_WALK);
        PARSE(ghandle, stop_on_eq);
        PARSE(link, stop_on_closingbr);
        GET_REST(data);

        cm = ghandle->length + link->length + data->length + 8*3;
        vwrite(dst, &cm, sizeof(uint32_t));

        SERIALIZE(COMMAND_GIT_WALK, ghandle);
        SERIALIZE(COMMAND_GIT_WALK, link);
        SERIALIZE(COMMAND_GIT_WALK, data);
        return dst;
    }
    return NULL;
}

static size_t xplain_git_walk( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_GIT_WALK ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_GIT_WALK) ) {
            XPLAIN("CMD_GIT_WALK, HANDLE=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_GIT_WALK) ) {
                XPLAIN("CMD_GIT_WALK, LINK=");
                if ( EXPECT_NEXT_COMMAND(COMMAND_GIT_WALK) ) {
                    XPLAIN("CMD_GIT_WALK, DATA=");
                    return ret;
                }
            }
        }
    }
    return 0;
}

static void git_walk(vecstring * dst, vecstring * ghandle, pcp_t * pcp, vecstring * link, vecstring * data);

static vecstring * exec_git_walk ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_GIT_WALK ) {
        EXEC_START(COMMAND_GIT_WALK);
        EXEC_GET_ARG(COMMAND_GIT_WALK, ghandle);
        EXEC_GET_ARG(COMMAND_GIT_WALK, link);
        EXEC_GET_ARG_NOEXEC(COMMAND_GIT_WALK, data);

        git_walk(dst, ghandle, pcp, link, data);

        delete_vecstring(ghandle);
        delete_vecstring(link);
        delete_vecstring(data);
        return dst;
    }
    return NULL;
}

static int refcb(git_reference * ref, void * p) {
    pcp_t * pcp = (pcp_t *) ((void **)p)[1];
    struct git_context * gitx = (struct git_context *) ((void **)p)[0];
    if ( git_reference_is_branch(ref) ) {
        pcp_log(pcp, PCP_LOG_DEBUG, COMMAND_GIT_WALK, "found branch %s\n", git_reference_name(ref));
        git_revwalk_push_ref(gitx->walk,git_reference_name(ref));
    }
    return 0;
}

static void git_walk(vecstring * dst, vecstring * ghandle, pcp_t * pcp, vecstring * link, vecstring * data) {
    char * lnk = vtopchar(link);

    git_repository * repo;
    int rc = git_repository_open_ext(&repo, lnk, 0, NULL);
    if (rc==0) {
        pcp_log(pcp, PCP_LOG_DEBUG, COMMAND_GIT_WALK, "opened %s repository successfully\n", lnk);
        struct git_context * gitx = allocate_git_context(ghandle);
        gitx->repo = repo;
        rc = git_revwalk_new(&gitx->walk, repo);
        //2 void pointers
        void * private[2] = {gitx, pcp};
        git_reference_foreach(gitx->repo, refcb, &private);
        // git_revwalk_push_head(gitx->walk);

        git_revwalk_sorting(gitx->walk, GIT_SORT_TOPOLOGICAL);
        if (rc == 0) {
            while (git_revwalk_next(&gitx->oid, gitx->walk)==0) {
                if (git_commit_lookup(&gitx->commit, gitx->repo, &gitx->oid)==0) {
                    vecstring * todst = pcp_do_exec(data,pcp);
                    if (!todst) {
                        pcp_log(pcp, PCP_LOG_ERROR, COMMAND_GIT_WALK, "failed to execute piece during git walk\n");
                        return;
                    }
                    vmove(dst, todst);
                    git_commit_free(gitx->commit);
                } else {
                    pcp_log(pcp, PCP_LOG_ERROR, COMMAND_GIT_WALK, "commit lookup error\n");
                    break;
                }
            }
        } else {
            pcp_log(pcp, PCP_LOG_ERROR, COMMAND_GIT_WALK, "Unable to initialize rev walk for %s: error %d\n", lnk, rc);
        }
        git_repository_free(repo);
    } else {
        pcp_log(pcp, PCP_LOG_ERROR, COMMAND_GIT_WALK, "Couldn't open reporitory %s: error %d\n", lnk, rc);
    }

    free(lnk);
}
