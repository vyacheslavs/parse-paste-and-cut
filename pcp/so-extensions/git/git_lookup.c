#include "pcp/pcp.h"
#include "pcp/macros.h"
#include "git_handles.h"
#include <string.h>

/*
    Usage:

    $(GIT::<handle>.<modifier>])$

    Example:
        $(GIT::<handle>.commit)$ <-- current commit sha1
*/

static vecstring * parse_git_lookup ( vecstring * src, pcp_t * pcp, int * parsed );
static size_t xplain_git_lookup( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
static vecstring * exec_git_lookup ( struct __pcp_t * pcp, vecstring * src, int * parsed );

pcp_handler handler_git_lookup = {
	.cb = parse_git_lookup,
	.cb_explain = xplain_git_lookup,
	.cb_exec = exec_git_lookup,
	.cb_init = NULL,
	.cb_fini = NULL,
	.probe_command = COMMAND_GIT_LOOKUP
};

static vecstring * parse_git_lookup ( vecstring * src, pcp_t * pcp, int * parsed ) {
    const char key[] = "GIT::";
    if ( vstrcmp(src, key) ) {
        PARSE_START(COMMAND_GIT_LOOKUP);
        PARSE(ghandle, stop_on_dot);
        GET_REST(modifier);

        cm = ghandle->length + modifier->length + 8*2;
        vwrite(dst, &cm, sizeof(uint32_t));
        SERIALIZE(COMMAND_GIT_LOOKUP, ghandle);
        SERIALIZE(COMMAND_GIT_LOOKUP, modifier);
        return dst;
    }
    return NULL;
}

static size_t xplain_git_lookup( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, parsed, 0) && cm == COMMAND_GIT_LOOKUP ) {
        XPLAIN_START
        if ( EXPECT_NEXT_COMMAND(COMMAND_GIT_LOOKUP) ) {
            XPLAIN("CMD_GIT_LOOKUP, HANDLE=");
            if ( EXPECT_NEXT_COMMAND(COMMAND_GIT_LOOKUP) ) {
                XPLAIN("CMD_GIT_LOOKUP, MODIFIER=");
                return ret;
            }
        }
    }
    return 0;
}

static vecstring * exec_git_lookup ( struct __pcp_t * pcp, vecstring * src, int * parsed ) {
    uint32_t cm, sz;
    if ( pcp_read_command( &cm, &sz, src, 0) && cm == COMMAND_GIT_LOOKUP ) {
        EXEC_START(COMMAND_GIT_LOOKUP);
        EXEC_GET_ARG(COMMAND_GIT_LOOKUP, ghandle);
        EXEC_GET_ARG(COMMAND_GIT_LOOKUP, modifier);

        struct git_context * ctx = get_git_context(ghandle);
        if (ctx) {
            // мы нашли контекст, выведем, что от нас требуется
            char * modp = vtopchar(modifier);
            if (strcasecmp(modp, "commit")==0) {
                // выводим название текущего коммита
                char commitN[41];
                commitN[40] = 0;
                git_oid_fmt(commitN, &ctx->oid);
                vmove(dst, vecstring_from_pchar(commitN));
            } else if (strcasecmp(modp, "message")==0) {
                vmove(dst, vecstring_from_pchar(git_commit_message(ctx->commit)));
            } else if (strcasecmp(modp, "parent-count")==0) {
                char pC[0xfff];
                snprintf(pC, 0xfff-1, "%d", git_commit_parentcount(ctx->commit));
                vmove(dst, vecstring_from_pchar(pC));
            } else if (strstr(modp, "parent[")) {
                // если присутствует parent-, значит находим
                char * index_s = modp+7;
                unsigned int index = atoi(index_s);
                if (index<git_commit_parentcount(ctx->commit)) {
                    const git_oid * poid = git_commit_parent_id(ctx->commit, index);
                    if (poid) {
                        char commitN[41];
                        commitN[40] = 0;
                        git_oid_fmt(commitN, poid);
                        vmove(dst, vecstring_from_pchar(commitN));
                    } else {
                        pcp_log(pcp, PCP_LOG_ERROR, COMMAND_GIT_LOOKUP, "unable to get parent commit with index: %s\n", modp);
                    }
                }
            } else if (strcasecmp(modp, "timestamp")==0) {
                char pC[0xfff];
                snprintf(pC, 0xfff-1, "%ld", git_commit_time(ctx->commit));
                vmove(dst, vecstring_from_pchar(pC));
            }
            free(modp);
        } else {
            char * ghandle_p = vtopchar(ghandle);
            pcp_log(pcp, PCP_LOG_ERROR, COMMAND_GIT_LOOKUP, "No such git context: %s\n", ghandle_p);
            free(ghandle_p);
        }
        delete_vecstring(ghandle);
        delete_vecstring(modifier);
        return dst;
    }
    return NULL;
}
