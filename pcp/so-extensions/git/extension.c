#include "pcp/pcp.h"

extern pcp_handler handler_git_walk;
extern pcp_handler handler_git_lookup;

pcp_handler * register_pcp_handlers[] = {
    &handler_git_walk,
    &handler_git_lookup,
    NULL
};
