#include "git_handles.h"
#include <mempool/mempool.h>

static DEFINE_HASHTABLE(ht, 8);
static mempool mp;

void git_handles_init(void *r) {
    hash_init(ht);
    mp_init( &mp, sizeof(struct git_context) );
}

void git_handles_fini(void *r) {
    mp_fini( &mp );
}

struct git_context * allocate_git_context(const vecstring * handle) {
    struct git_context * ret = get_git_context(handle);
    if (ret) {
        return ret;
    }
    uint8_t keyval = (uint8_t) vecstring_keyval(handle);
    struct git_context * newone = (struct git_context *) mp_alloc(&mp);
    newone->ghandle = vshare(handle);

    hash_add( ht, &newone->hash, keyval );
    return newone;
}

struct git_context * get_git_context(const vecstring * handle) {
    uint8_t keyval = (uint8_t) vecstring_keyval(handle);
    struct git_context * it = NULL;
    hash_for_each_possible( ht, it, hash, keyval )
    {
        if ( vstr_compare( it->ghandle, (vecstring *)handle ) ) {
            return it;
        }
    }
    return NULL;
}
