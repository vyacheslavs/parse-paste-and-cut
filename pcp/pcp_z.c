#include "pcp.h"
#include <zlib.h>
#include <malloc.h>
#include <assert.h>

void pcp_z(pcp_piece * piece) {
	if ( piece->parsed && !piece->compressed ) {
		// выпрямляем буфер
		uLong len = piece->parsed->length;
		uLong destLen = len+len/2+12;
		char * buf = vtopchar(piece->parsed);
		char * compressed = (char *) malloc ( destLen+4 );
		uint32_t * compressed_sz = (uint32_t *)compressed;
		*compressed_sz = len;
		compressed += 4;
		int res = compress2(compressed, &destLen, buf, len, Z_BEST_SPEED);
		if (res == Z_OK) {
			piece->compressed = vecstring_from_bchar( compressed-4, destLen+4 );
		} else {
			fprintf(stderr, "couldn't compress data: error=%d\n", res);
			assert(0);
		}
		compressed -= 4;
		free(compressed);
		free(buf);
	}
}

void pcp_uz(pcp_piece * piece) {
	if ( piece->compressed && !piece->parsed ) {
		// выпрямляем буфер
		uLong len = piece->compressed->length-4;
		char * buf = vtopchar(piece->compressed);
		uint32_t * uncompressed_sz = (uint32_t *) buf;
		uLong destLen = *uncompressed_sz;
		buf+=4;
		char * uncompressed = (char *) malloc ( destLen );
		if (uncompress(uncompressed, &destLen, buf, len) == Z_OK) {
			piece->parsed = vecstring_from_bchar( uncompressed, destLen );
		}
		buf-=4;
		free(uncompressed);
		free(buf);
	}
}
