#include "pcp.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <libgen.h>
#include <dirent.h>
#include <dlfcn.h>

#ifndef CHUNKSZ
#define CHUNKSZ 1024
#endif

#define DATA_FOUND 1

struct slice_context {
	size_t remove;
	size_t bracket_opened;
	size_t data_start;
	vecstring * name;
	vecstring * data;
};

extern pcp_handler handler_lookup;

static void load_so(pcp_t * pcp);

void pcp_init(pcp_t * pcp) {
	init_refstring_printfs();
	init_vecstring_printfs();
	
	pcp->buf = NULL;
	pcp->errcb = pcp_default_error_callback;
    pcp->logcb = pcp_default_log_callback;
    pcp->working_on = NULL;
    pcp->log_level = 4;
    pcp->log_codes = NULL;
    pcp->log_codes_except = NULL;
	pcp->pieces = malloc (sizeof(pcp_piece *) * PCP_HT_SIZE );
	assert(pcp->pieces);
	memset(pcp->pieces, 0, sizeof(pcp_piece *) * PCP_HT_SIZE);
	
	pcp->handlers = (pcp_handler **) malloc ( COMMAND_REGISTRATOR_SZ * sizeof(pcp_handler *));
	assert(pcp->handlers);
	pcp->max_handler_id = 0;
	pcp_handler_registry_init(pcp);
	
	handler_lookup.so_library = NULL;
	pcp_register_handler(pcp, &handler_lookup);
	// теперь грузим shared objects
	load_so(pcp);
}

void pcp_fini(pcp_t * pcp) {
	int idx=0;
	for (;idx<PCP_HT_SIZE;idx++) {
		pcp_piece * it = pcp->pieces[idx], * next;
		while(it) {
			next = it->next;
			if (it->ns) delete_refstring(it->ns);
			if (it->name) delete_vecstring(it->name);
			if (it->parsed) delete_vecstring(it->parsed);
			if (it->unparsed) delete_vecstring(it->unparsed);
			free(it);
			it = next;
		}
	}
	free(pcp->pieces);
	pcp_handler_registry_fini(pcp);
	free(pcp->handlers);
}

static int pcp_hash ( vecstring * src, refstring * ns ) {
	size_t idx=0;
	viteration_t v = VITERATION_INIT;
	int ret = 0;
	while (idx<refstring_size(ns)) {
		ret = idx * 10 + *refstring_ptr(ns);
		idx++;
	}
	idx = 0;
	while (idx<src->length) {
		uint8_t c = vstring_at(src,idx, &v);
		ret = idx * 10 + c;
		idx++;
	}
	return ret;
}

static void pcp_parse( pcp_t * pcp, refstring * ns );
static void pcp_parse_binary( pcp_t * pcp, refstring * ns );
static int probe_pcp_if_its_binary(pcp_t * pcp);

void pcp_load(pcp_t * pcp, const char * file) {
	int fd = open(file, O_RDONLY);
	if (fd<0) {
		pcp_error(pcp, "couldn't find file to load: %s\n", file);
		return;
	}
	int rd=1;
	int is_binary_formatted = 2; // является ли файл уже заранее скомпилированным?
								 // 0 - нет, не является
								 // 1 - да, является
								 // 2 - надо узнать
	
	refstring * ns = new_refstring_from_pchar(file);
	while (rd) {
		refstring * newchunk = new_refstring(CHUNKSZ);
		rd = read(fd, refstring_ptr(newchunk), CHUNKSZ );
		if ( !pcp->buf )
			pcp->buf = vecstring_from_refstring( newchunk, 0, rd );
		else {
			vappend_refstring(pcp->buf, newchunk, 0, rd);
		}
		delete_refstring(newchunk);
		
		if (is_binary_formatted == 2) { 
			if (probe_pcp_if_its_binary(pcp))
				is_binary_formatted = 1;
			else 
				is_binary_formatted = 0;
		}
		if (is_binary_formatted==0)
			pcp_parse(pcp, ns);
		else if (is_binary_formatted==1)
			pcp_parse_binary(pcp,ns);
	}
	delete_vecstring(pcp->buf);
	delete_refstring(ns);
	close(fd);
	pcp->buf = NULL;
}

static int probe_pcp_if_its_binary(pcp_t * pcp) {
	if ( pcp->buf->length < 8 )
		return 0;
	int ret = 0;
	vecstring * pcp_sign = subvstr( pcp->buf, 4, 4 );
	vecstring * pcp_idol = vecstring_from_pchar("PCP_");
	if ( vstr_compare ( pcp_sign, pcp_idol)) {
		ret = 1;
		// если мы точно знаем, что это бинарник - отрезаем заголовок
		uint32_t head_size = 0;
		vread(pcp->buf, &head_size, 0, 4); // считали размер заголовка
		verase(pcp->buf, 0, head_size+4); // обрезали заголовок
	}
	delete_vecstring(pcp_idol);
	delete_vecstring(pcp_sign);
	return ret; // по умолчанию говорим, что нет
}

static int open_bracket_cb ( vecstring * src, int position, void * ct );
static int open_bracket_name_cb ( vecstring * src, int position, void * ct );
static int close_bracket_cb ( vecstring * src, int position, void * ct );
static pcp_piece * __pcp_find_piece ( pcp_t * pcp, refstring * ns, vecstring * name );

static void pcp_parse( pcp_t * pcp, refstring * ns ) {
	struct slice_context ct;
	do {
		ct.remove = 0;
		vfind_pchar ( pcp->buf, "<!--{", 0, open_bracket_cb, &ct );
		if ( ct.remove ) {
			if ( !__pcp_find_piece(pcp, ns, ct.name)) {
				pcp_piece * new_piece = malloc(sizeof(pcp_piece));
				new_piece->name = ct.name;
				new_piece->ns = share_refstring(ns);
				new_piece->unparsed = ct.data;
				new_piece->parsed = NULL;
				new_piece->compressed = NULL;
				int oft = pcp_hash(new_piece->name, ns) % PCP_HT_SIZE;
				new_piece->next = pcp->pieces[oft];
				pcp->pieces[oft] = new_piece;
			}
			vecstring * newbuf = vsplit(pcp->buf, ct.remove);
			delete_vecstring(pcp->buf);
			pcp->buf = newbuf;
		}
	} while (ct.remove);
}

static int open_bracket_cb ( vecstring * src, int position, void * _ct ) {
	struct slice_context * ct = _ct;
	ct->bracket_opened = position;
	vfind_pchar ( src, "}--", position+5, open_bracket_name_cb, _ct);
	return ct->remove == 0;
}

static pcp_piece * __pcp_find_piece ( pcp_t * pcp, refstring * ns, vecstring * name ) {
	int idx = pcp_hash( name, ns ) % PCP_HT_SIZE;
	pcp_piece * it = pcp->pieces[idx];
	while (it) {
		// fprintf(stderr, "lookup [%R::%V], looking for [%R::%V]\n", it->ns, it->name, ns, name);
		if ( refstring_cmp(ns, it->ns) && vstr_compare(name, it->name) )
			break;
		it = it->next;
	}
	return it;
}

pcp_piece * pcp_find_piece ( pcp_t * pcp, const char * _ns, const char * piecename ) {
	refstring * ns = new_refstring_from_pchar(_ns);
	vecstring * name = new_vecstring(strlen(piecename));
	pchar_to_vecstring( name, piecename );
	pcp_piece * ret = __pcp_find_piece( pcp, ns, name );
	delete_vecstring(name);
	delete_refstring(ns);
	return ret;
}

static int open_bracket_name_cb ( vecstring * src, int position, void * _ct ) {
	struct slice_context * ct = _ct;
	 ct->name = subvstr(src, ct->bracket_opened+5, position-ct->bracket_opened-5);
	
	char * slicename = malloc( ct->name->length+8 ), *pslice = slicename;
	assert(slicename);
	memcpy(pslice, "--{", 3); pslice+=3;
	vdump_to_ptr(ct->name, pslice);
	pslice+=ct->name->length;
	memcpy(pslice, "}-->", 4); pslice+=4;
	*pslice = 0;
	ct->data_start = position+3;
	vfind_pchar( src, slicename, position+3, close_bracket_cb, _ct);
	free(slicename);
	if ( ct->remove ==0 )
		delete_vecstring(ct->name);
	return ct->remove == 0;
}

static int close_bracket_cb ( vecstring * src, int position, void * _ct ) {
	struct slice_context * ct = _ct;
	ct->data = subvstr(src, ct->data_start, position-ct->data_start);
	ct->remove = position+7+ct->name->length-1;
	return 0;
}

static void pcp_parse_binary( pcp_t * pcp, refstring * ns ) {
	for (;;) {
		if (pcp->buf->length <= 5)
			return;
		// загружаем флаг
		uint8_t flag = 0;
		vread(pcp->buf, &flag, 0, 1);
		// загружаем названия куска
		uint32_t piece_name_sz = 0;
		vread(pcp->buf, &piece_name_sz, 1, 4);
		if (pcp->buf->length <= 5+ piece_name_sz + 4)
			return;
		char * piece_name = (char *) malloc(piece_name_sz+1);
		vread(pcp->buf, piece_name, 5, piece_name_sz);
		piece_name[piece_name_sz]=0;

		// загружаем размер куска
		uint32_t piece_sz = 0;
		vread(pcp->buf, &piece_sz, 5+piece_name_sz, 4);

		if ( pcp->buf->length < 5+ piece_name_sz + 4 + piece_sz ) {
			free(piece_name);
			return;
		}

		vecstring * piece_name_vec = vecstring_from_pchar(piece_name);
		if ( !__pcp_find_piece(pcp, ns, piece_name_vec)) {
			// загружаем кусок
			pcp_piece * new_piece = malloc(sizeof(pcp_piece));
			new_piece->name = vshare(piece_name_vec);
			new_piece->ns = share_refstring(ns);
			new_piece->parsed = NULL;
			new_piece->compressed = NULL;
			new_piece->unparsed = NULL;
			
			if ( (flag & 2) > 0 ) {
				new_piece->compressed = subvstr(pcp->buf, 5+piece_name_sz+4, piece_sz);
				pcp_uz(new_piece);
			} else {
				new_piece->parsed = subvstr(pcp->buf, 5+piece_name_sz+4, piece_sz);
			}
			
			int oft = pcp_hash(new_piece->name, ns) % PCP_HT_SIZE;
			new_piece->next = pcp->pieces[oft];
			pcp->pieces[oft] = new_piece;
		}
		delete_vecstring(piece_name_vec);
		free(piece_name);
		verase(pcp->buf, 0, 9+piece_name_sz+piece_sz);
	}
}

static void load_so(pcp_t * pcp) {
    pcp_log( pcp, PCP_LOG_DEBUG, 0, "Loading extensions from %s\n", EXTPATH);
	DIR * dir = opendir(EXTPATH);
	if (dir) {
		struct dirent *entry;
		while ((entry = readdir(dir)) != NULL) {
			if (entry->d_type == DT_REG || entry->d_type==DT_LNK) {
				//TODO по хорошему dopen надо освобождать
				char fp[0xff];
				snprintf(fp, 0xff-1, "%s/%s", EXTPATH, entry->d_name);
                pcp_log(pcp, PCP_LOG_DEBUG, 0, " %s...\n", entry->d_name);
				void * h = dlopen(fp, RTLD_LAZY);
				if (h) {
					// удалось загрузить библиотеку
					pcp_handler ** handler = (pcp_handler **) dlsym(h, "register_pcp_handlers");
					if (handler) {
						int * hrefs = (int *) malloc(sizeof(int));
						*hrefs = 0;
						while ( *handler ) {
							(*handler)->so_library = h;
							(*handler)->so_library_refcount = hrefs;
							(*hrefs)++;
							pcp_register_handler(pcp, *handler);
							*handler++;
						}
					} else {
                        pcp_error(pcp, "couldn't find register_pcp_handlers symbol\n");
						dlclose(h);
					}
				} else {
					fprintf(stderr, "couldn't load %s\n", fp);
				}
			}
		}
		closedir(dir);
	} else {
		fprintf(stderr, "can not load: %s\n", EXTPATH);
	}
}
