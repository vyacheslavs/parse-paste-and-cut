#include "pcp.h"
#include <malloc.h>
#include <assert.h>

vecstring * parse_lookup ( vecstring * src, pcp_t * pcp, int * parsed );
size_t lookup_xplain( struct __pcp_t * pcp, FILE * stream, vecstring * parsed, int lvl );
vecstring * lookup_exec ( struct __pcp_t * pcp, vecstring * src, int * parsed );

void pcp_handler_registry_init(pcp_t * pcp) {
	int idx=0;
	for (;idx<COMMAND_REGISTRATOR_SZ;idx++)
		pcp->handlers[idx] = NULL;
}

void pcp_handler_registry_fini(pcp_t * pcp) {
	int idx=0;
	for (;idx<pcp_max_handler_id(pcp);idx++) {
		if (pcp->handlers[idx] && pcp->handlers[idx]->cb_fini)
			pcp->handlers[idx]->cb_fini(pcp);
		if (pcp->handlers[idx] && pcp->handlers[idx]->so_library) {
			int * refs = pcp->handlers[idx]->so_library_refcount;
			assert(refs);
			(*refs)--;
			if (*refs == 0) {
				dlclose(pcp->handlers[idx]->so_library);
				free(refs);
			}
		}
		pcp->handlers[idx] = NULL;
	}
}

void pcp_register_handler ( pcp_t * pcp, pcp_handler * handler ) {
	pcp->handlers[handler->probe_command] = handler;
	if (pcp->max_handler_id<handler->probe_command)
		pcp->max_handler_id=handler->probe_command+1;
	// обработчик зарегистрирован, вызываем инициализацию
	if (handler->cb_init)
		handler->cb_init(pcp);
}

pcp_handler * pcp_get_handler (pcp_t * pcp, uint32_t probe_command ) {
	return pcp->handlers[probe_command];
}

int pcp_max_handler_id(pcp_t * pcp) {
	return pcp->max_handler_id;
}
