#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include "pcp/pcp.h"
#include <unistd.h>
#include <string.h>

#define MAX_POST 10000000

#define PCP_CODE_CLIENT 0

static void parse_cookie(char * qs);
static int init_libpcp();
static int run();
static void fini_libpcp();
static pcp_t t;
static char * pinfo = NULL;
static int collect_query_string_params();
static void parse_form(uint64_t len, char * rm);
static void parse_data(uint64_t len, char * rm, char * ct);
static void parse_qs(char * qs, char * rm);
static char * unescape(char * src);
static char main_piece[0xff];
static int debug_mode = 0;

int main(int argc, char ** argv) {
	int idx=0;
    int log_sys = 0;
	for (;idx<argc;++idx) {
		if ( strcmp(argv[idx], "-d")==0 ) {
			debug_mode++;
		}
		if ( strcmp(argv[idx], "-D")==0 ) {
            log_sys = 1;
        }
    }
    pcp_log_system(log_sys);
	pcp_init(&t);
    pcp_set_log_level(&t, debug_mode);
	for (idx=0;idx<argc;++idx) {
        if ( strcmp(argv[idx], "-x")==0 && idx+1<argc ) {
            ++idx;
            pcp_dont_log(&t, atoi(argv[idx]));
            continue;
        }
	}
    pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "Starting pcp cgi client under debug mode %d\n", debug_mode);
	if (!init_libpcp())
		return 1;
	if (!collect_query_string_params())
		return 1;
	run();
	fini_libpcp();
	return 0;
}

static int init_libpcp() {
	init_varstorage();
	if (!getenv("DOCUMENT_ROOT")) {
		fprintf(stderr, "no web context, aborting...\n");
		return 0;
	}
	// переходим в doc_root
	chdir ( getenv("DOCUMENT_ROOT") );
	// теперь загружаем PATH_INFO
	pinfo = getenv("PATH_INFO");
	if (!pinfo) {
		fprintf(stderr, "no PATH_INFO, aborting...\n");
		return 0;
	}
	while (*pinfo && *pinfo=='/') ++pinfo;
	pcp_load(&t, pinfo);
	strncpy((char *)main_piece, "main", 0xff-1); // по умолчанию будет кусок main
	return 1;
}

static int run() {
	vecstring * result = pcp_exec(&t, pinfo, (const char *)main_piece);
	if (result) {
        char * resultp = vtopchar(result);
        printf("%s", resultp);
        pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "pcp exec result:\n---------------------------------------\n%s\n---------------------------------------\n", resultp);
        free(resultp);
		delete_vecstring(result);
	} else {
		fprintf(stderr, "no output from %s\n", main_piece);
	}
	return 1;
}

static void fini_libpcp() {
	pcp_fini(&t);
}

static int collect_query_string_params() {
	char * rm = getenv("REQUEST_METHOD");
	if (!rm) {
        pcp_log ( &t, PCP_LOG_CRITICAL, PCP_CODE_CLIENT, "no REQUEST_METHOD found\n");
		return 0;
	}
	char * e = getenv("QUERY_STRING");
	uint64_t contentlen = 0;
	char * cl = getenv("CONTENT_LENGTH");
	if (cl) contentlen = atoll(cl);
	char * cookie = getenv("HTTP_COOKIE");
	if (cookie) {
		parse_cookie(cookie);
	}
    pcp_log ( &t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "REQUEST_METHOD=[%s]\n",rm);
	vecstring * reqm = vecstring_from_pchar("ENV::REQUEST_METHOD");
	vecstring * reqm_val = vecstring_from_pchar(rm);
	get_servar_hook()(reqm, reqm_val);
	delete_vecstring(reqm_val);
	delete_vecstring(reqm);
	
	reqm = vecstring_from_pchar("ENV::QUERY_STRING");
	reqm_val = vecstring_from_pchar(e);
	get_servar_hook()(reqm, reqm_val);
	delete_vecstring(reqm_val);
	delete_vecstring(reqm);
	
	char * ra = getenv("REMOTE_ADDR");
	if (ra) {
		reqm = vecstring_from_pchar("ENV::REMOTE_ADDR");
		reqm_val = vecstring_from_pchar(ra);
		get_servar_hook()(reqm, reqm_val);
		delete_vecstring(reqm_val);
		delete_vecstring(reqm);
	}
	
	if (e) {
		char * qs = strdup(e);
		parse_qs(qs, rm);
		free(qs);
	}
	if ( contentlen > 0 && rm ) {
		char * ct = getenv("CONTENT_TYPE");
		if (ct) {
			if (strstr(ct, "application/x-www-form-urlencoded"))
				parse_form(contentlen, rm);
			if (strstr(ct, "multipart/form-data"))
				parse_data(contentlen, rm, ct);
		}
	}
	return 1;
}

static void parse_form(uint64_t len, char * rm) {
	if (len > MAX_POST)
		return;
	// fprintf(stderr, "parsing application/x-www-form-urlencoded...\n");
	char * qs = (char *) malloc(len+1);
	int r = 0;
	int nr = 0;
	while(1) {
		nr = read(0, qs+r, len-r);
		if (nr>0) {
			r+=nr;
		} else break;
	}
	qs[r] = 0;
	// fprintf(stderr, "urlencoded=[%s]\n", qs);
	parse_qs(qs, rm);
	free(qs);
}

static void parse_qs(char * qs, char * rm) {
	char * qsit = strtok(qs, "&");
	do {
		if (qsit) {
			char * lvalue = qsit;
			char * rvalue = strchr(qsit, '=');
			if (rvalue) {
				*rvalue = 0; ++rvalue;
			}
			char * lv = (char *) malloc (strlen(lvalue)+8);
			snprintf(lv,strlen(lvalue)+7, "%s::%s", rm, lvalue);
			vecstring * lvalue_v = vecstring_from_pchar(lv);
			free(lv);
			
			char * rvalueus = NULL;
			if (rvalue) {
				rvalueus = unescape(rvalue);
                pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "%s::%s=[%s] / [%s]\n",rm, lvalue, rvalue, rvalueus);
			} else {
                pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "lvalue without rvalue: [%s]\n", lvalue);
				if (strlen(lvalue)>1 && lvalue[0]=='@') {
					char * nmp = lvalue+1;
                    pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "changing main piece to: [%s]\n", nmp);
					strncpy(main_piece, nmp, 0xff-1);
				}
			}
			vecstring * rvalue_v = rvalue ? vecstring_from_pchar(rvalueus) : new_vecstring(0);
			get_servar_hook()(lvalue_v, rvalue_v);
			delete_vecstring( lvalue_v );
			delete_vecstring( rvalue_v );
			if (rvalueus)
				free(rvalueus);
		}
	} while (qsit = strtok(NULL,"&"));
}

static void parse_cookie(char * qs) {
	char * qsit = strtok(qs, ";");
	do {
		if (qsit) {
			char * lvalue = qsit;
			while (*lvalue && *(lvalue+1) && *lvalue == ' ') lvalue++;
			char * rvalue = strchr(qsit, '=');
			if (rvalue) {
				*rvalue = 0; ++rvalue;
			}
			char * lv = (char *) malloc (strlen(lvalue)+10);
			snprintf(lv,strlen(lvalue)+9, "COOKIE::%s", lvalue);
			vecstring * lvalue_v = vecstring_from_pchar(lv);
			free(lv);
			
			char * rvalueus = NULL;
			if (rvalue)
				rvalueus = unescape(rvalue);
            pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "COOKIE::%s=[%s]\n", lvalue, rvalueus ? rvalueus : "");
			vecstring * rvalue_v = rvalue ? vecstring_from_pchar(rvalueus) : new_vecstring(0);
			get_servar_hook()(lvalue_v, rvalue_v);
			delete_vecstring( lvalue_v );
			delete_vecstring( rvalue_v );
			if (rvalueus)
				free(rvalueus);
		}
	} while (qsit = strtok(NULL,";"));
}

static int unhex( char c ) {
	return( c >= '0' && c <= '9' ? c - '0' : c >= 'A' && c <= 'F' ? c - 'A' + 10 : c - 'a' + 10 );
}

int special( unsigned char * p ) {
	if ( *(p+1) && *(p+1)=='2' && *(p+2) && *(p+2)=='1' && *(p+3) && *(p+3)=='1' && *(p+4) && *(p+4)=='6' ) {
		return 1;
	}
	return 0;
}


static char * unescape(char * src) {
	
	char * out = (char *) malloc ( strlen(src)*2+1);
	if (!out) return NULL;
	memset(out, 0, strlen(src)*2+1);
	
	char * p = src;
	uint8_t * o = (uint8_t *)out;
	uint16_t tmpval;
	uint8_t * t;
	while (*p) {
		if (*p == '+' ) {
			*o = ' ';
			p++;
			o++;
			continue;
		}
		if (*p == '%' && *(p+1) ) {
			if ( *(p+1) == 'u' && special((unsigned char *)(p+1)) ) {
				// %u2116
				p+=6;
				strcpy((char*)o, "&#8470;");
				o+=7;
				continue;
			} else
			if ( *++p == 'u' && *(p+1) && *(p+2) && *(p+3) && *(p+4) ) {
				t = (uint8_t *) &tmpval;
				t++;
				*t = unhex(*++p) << 4;
				*t+= unhex(*++p);
				t--;
				*t = unhex(*++p) << 4;
				*t+= unhex(*++p);

				if ( tmpval < 0x0080 ) {
                    *o++ = (uint8_t)(tmpval & 0xff00);
					*o++ = tmpval & 0x00ff;
				} else if (tmpval < 0x0800) {
					*o++ = ( ( tmpval & 0x07c0 ) >> 6 ) | 0xc0;
					*o++ = ( tmpval & 0x3f ) | 0x80;
				} else {
					*o++ = (( tmpval & 0xf000 ) >> 12 ) | 0xe0;
					*o++ = ((tmpval & 0x0fc0) >> 6) | 0x80;
					*o++ = (tmpval & 0x3f) | 0x80;
				}
				p++;
				continue;
			} else if (*p && *(p+1)) {
				*o=unhex(*p++) << 4;
				*o+=unhex(*p++);
				o++;
				continue;
			}
		}
		*o=*p;
		p++;
		o++;
	}
	return out;
}

struct tokctx {
	char * delimiters;
	char * source;
	char * iterator;
};

static void tokenize( struct tokctx * ctx, const char * src, const char * delimiters ) {
	ctx->source = strdup(src);
	ctx->delimiters = strdup(delimiters);
	ctx->iterator = ctx->source;
}

static void detokenize(struct tokctx * ctx) {
	free(ctx->source);
	free(ctx->delimiters);
}

static char * gettoken( struct tokctx * ctx ) {
	int has_it = 0;
	do {
		if (!*ctx->iterator) return NULL;
		char * del = ctx->delimiters;
		has_it = 0;
		for (;del<ctx->delimiters+strlen(ctx->delimiters);++del) {
			if ( *del == *ctx->iterator ) {
				has_it = 1; break;
			}
		}
		if (!has_it) break;
		++ctx->iterator;
	} while ( has_it );
	char * save_iterator = ctx->iterator;
	do {
		if (!*ctx->iterator) return save_iterator;
		char * del = ctx->delimiters;
		has_it = 0;
		for (;del<ctx->delimiters+strlen(ctx->delimiters);++del) {
			if ( *del == *ctx->iterator ) {
				has_it = 1; break;
			}
		}
		if (has_it) break;
		++ctx->iterator;
	} while ( !has_it );
	if (!*ctx->iterator) return save_iterator;
	*ctx->iterator = 0;
	ctx->iterator++;
	return save_iterator;
}

static char * strnstr(const char * src, const char * find, int slen ) {
	
	int findlen = strlen(find), findfrom=0, idx;
	while (1) {
		if ( slen-findfrom < findlen )
			return NULL;
		
		for ( idx=findfrom; idx<findfrom+findlen; ++idx ) {
			if ( *(src+idx) != *(find+idx-findfrom) ) break;
		}
		if ( idx == findfrom+findlen )
			return (char *)(src+findfrom);
		findfrom++;
	}
	return NULL;
}

/*
В итоге мы должны получить следующие переменные:

POST::imagelink -> должно смотреть на данные
POST::imagelink{filename} -> название имени файла
POST::imagelink{content-type} -> медиа тип данных
*/

static void strip_strval ( char ** v ) {
	char * vv = *v;
	while (*vv && *vv=='"') ++vv;
	*v = vv;
	vv += strlen(vv)-1;
	while (vv>*v && *vv=='"') --vv;
	*(vv+1) = 0;
}

static void parse_params_pair ( char * ppair, char ** name ) {
	struct tokctx t;
	tokenize(&t, ppair, "=");
	char * tok = gettoken(&t);
	if (tok) {
		char * tokv = gettoken(&t);
		if (tokv) {
			if ( strcasecmp(tok, "name")==0) {
				strip_strval(&tokv);
				*name = strdup(tokv);
			}
			if ( strcasecmp(tok, "filename")==0 && *name ) {
				strip_strval(&tokv);
				char vn[0xfff];
				snprintf(vn, 0xfff-1, "POST::%s{filename}", *name );
				vecstring * vecn = vecstring_from_pchar(vn);
				vecstring * vecn_val = vecstring_from_pchar(tokv);
				get_servar_hook()(vecn, vecn_val);
				delete_vecstring(vecn_val);
				delete_vecstring(vecn);
				fprintf(stderr, "POST::%s{filename}=%s\n", *name, tokv);
			}
		}
	}
	detokenize(&t);
}

static void parse_header_line_params ( char * params, char ** name ) {
	struct tokctx paramspair;
	tokenize(&paramspair, params, ";");
	char * tok = gettoken(&paramspair);
	while (tok) {
		while (*tok && *tok ==' ')++tok;
		parse_params_pair(tok, name);
		tok = gettoken(&paramspair);
	}
	detokenize(&paramspair);
}

static void parse_header_line ( char * bheadline, char ** name ) {
	
	struct tokctx toke;
	tokenize(&toke, bheadline, " ;:");
	char * tokh = gettoken(&toke);
	if (!tokh) return;
	char * tok = gettoken(&toke);
	if (!tok) return;
	if (( strcasecmp(tokh, "Content-Disposition")==0 && strcasecmp(tok, "form-data")==0 )) {
		parse_header_line_params(toke.iterator, name);
	}
	if (( strcasecmp(tokh, "Content-Type")==0) && *name) {
		char vn[0xfff];
		snprintf(vn, 0xfff-1, "POST::%s{content-type}", *name );
		vecstring * vecn = vecstring_from_pchar(vn);
		vecstring * vecn_val = vecstring_from_pchar(tok);
		get_servar_hook()(vecn, vecn_val);
		delete_vecstring(vecn_val);
		delete_vecstring(vecn);
        pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "POST::%s{content-type}=%s\n", *name, tok);

	}
	detokenize(&toke);
}

static void parse_boundary_headers( char * bhead, char ** name ) {
	char * it = bhead, *o = bhead;
	while ( (it=strstr(it, "\r\n")) ) {
		char * headline = strndup(o, it-o);
		parse_header_line(headline, name);
		free(headline);
		it+=2;
		o = it;
	}
}

static void parse_boundary( const char * body, const char * boundary, unsigned int len ) {
	int blen = strlen(boundary);
	if ( len < blen+2 ) return;
	body += blen+2;
	len -= blen+2;
	char * bhead_end = strnstr(body, "\r\n\r\n", len);
	if (!bhead_end) {
		fprintf(stderr, "no end of header\n");
		return;
	}
	char * bhead = strndup(body, bhead_end-body+2), *vname = NULL;
	parse_boundary_headers(bhead, &vname);
	free(bhead);
	if (vname) {
		len -= (bhead_end+4-body+2);
		body = bhead_end+4;
		char vn [0xfff];
		snprintf(vn, 0xfff-1, "POST::%s", vname);
		vecstring * vecn = vecstring_from_pchar(vn);
		vecstring * vecn_val = vecstring_from_bchar(body,len);
		get_servar_hook()(vecn, vecn_val);
		delete_vecstring(vecn_val);
		delete_vecstring(vecn);
        pcp_log(&t, PCP_LOG_DEBUG, PCP_CODE_CLIENT, "POST::%s=.. %d size\n", vname, len);
		free(vname);
	}
}

static void parse_data(uint64_t len, char * rm, char * ct) {
	if (len > MAX_POST) {
		fprintf(stderr, "POST data is too big, skipping...");
		return;
	}
	char * qs = (char *) malloc(len), *qso=qs;
	char * rptr = qs, * orptr = qs;
	int r;
	while ( (r = read(0, rptr, len - (rptr-orptr)  )) ) {
		rptr+=r;
	}
	r = rptr-orptr;
	
	// сначала узнаем что за баундери у vecqs
	char * boun = strstr(ct, "boundary=");
	if (!boun)
		goto parse_end;
	boun += 9;
	char * bounend = boun;
	while ( *bounend && !(*bounend=='\n' || *bounend=='\r' || *bounend==';')) bounend++;
	
	char * boundary = (char *) malloc ( bounend - boun + 3+2 );
	strncpy(boundary+2, boun, bounend - boun);
	boundary[0] = '-';
	boundary[1] = '-';
	boundary[bounend - boun+2] = 0;
	// теперь начинаем искать boundary
	
	char * it = NULL, * prev_it = NULL;
	int blen = strlen(boundary);
	while ( (it = strnstr(qs, boundary, r)) ) {
		
		if (prev_it) {
			parse_boundary(prev_it, boundary, it - prev_it);
		}
		r = r - (it+blen-qs);
		qs = it+blen;
		if ( it + blen > qs+r ) break;
		prev_it = it;
	}
	
	free(boundary);
parse_end:
	free(qso);
}
